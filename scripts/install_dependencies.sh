#!/bin/bash
sudo yum install -y          \
    httpd24               \
    libxml2             \
    nodejs              \
    openldap-clients    \
    openssl             \
    openssl-devel       \
    php56                 \
    php56-cli             \
    php56-devel           \
    php56-gd              \
    php56-ldap            \
    php56-mbstring        \
    php56-mcrypt          \
    php56-mssql           \
    php56-mysqlnd         \
    php56-pdo             \
    php56-bcmath          \
    php56-pear            \
    php56-pear-HTTP       \
    php56-pear-HTTP-Client  \
    php56-pear-HTTP-Request \
    php56-pear-HTTP-Request2 \
    php56-pear-Net-Socket \
    php56-pear-Net-URL    \
    php56-pear-Net-URL2   \
    php56-pear-XML-Parser \
    php56-pear-XML-Serializer \
    php56-process         \
    php56-soap            \
    php56-xml             \
    php56-xmlrpc          \
    ant
sudo yum install -y python-setuptools
sudo easy_install pip
sudo pip install supervisor