#!/bin/bash
JIRA_RELEASE={RELEASE_NUMBER}
COMMIT_ID={COMMIT_ID}
# TODO replace by variable APP_HOME and define it on buildspec.yml
SYMLINK=/var/www/html/hubdashboard
cd /var/www/html/
# move symlink
if [ -f "$SYMLINK" ]; then
    rm -f $SYMLINK
fi
sudo ln -s /var/www/html/hubdashboard-${JIRA_RELEASE} $SYMLINK

# echo Version into Index
# sudo echo "<h1>$(hostname)</h1><h2>hubdashboard hubdashboard-${JIRA_RELEASE}</h2><p>Commit ID: {COMMIT_ID}</p>" > $SYMLINK/index.html