
aws deploy push --application-name <MyAppName> \
      --s3-location s3://<MyBucketName>/<MyNewAppBundleName> \
      --source <PathToMyBundle>
