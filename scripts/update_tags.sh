#!/bin/bash
echo "VERSION:$CODEBUILD_SOURCE_VERSION"
echo "REPO:$CODEBUILD_SOURCE_REPO_URL"
echo "RESOLVED:$CODEBUILD_RESOLVED_SOURCE_VERSION"
echo "BUILDID:$CODEBUILD_BUILD_ID"
for file in scripts/*.sh; do
  perl -pi -e "s/{RELEASE_NUMBER}/${RELEASE_NUMBER}/g" < $file > ${file}.tmp && mv $file{.tmp,} ;
  perl -pi -e "s/{COMMIT_ID}/${CODEBUILD_RESOLVED_SOURCE_VERSION}/g" < $file > ${file}.tmp && mv $file{.tmp,} ;
done