#!/bin/bash
if [ "$COMMIT_ID" == "" ]; then
    declare -x COMMIT_ID=`echo $DEPLOYMENT_ID | sed 's/d-//g'`
    if [ "$DEPLOYMENT_GROUP_NAME" == "HubDashboardFleetDev" ]; then
        declare -x RELEASE_NUMBER=$COMMIT_ID
        for file in `find . -name config.*`; do
            mv -f $file $file.tmp;  
            perl -pi -e "s/prod-webediaurora.datatrans.internal/dev-webediaurora.datatrans.internal/g" < $file.tmp > $file; 
            rm $file.tmp;
        done
    else
        LAST_BUILD=`ls  /var/www/html/  | grep GSS-[0-9] | sort | tail -1 | sed 's/hubdashboard-//g'`
        let NEXT_BUILD=$LAST_BUILD+1
        declare -x RELEASE_NUMBER=$NEXT_BUILD
    fi
    for file in `pwd`/deployment-root/$DEPLOYMENT_GROUP_ID/$DEPLOYMENT_ID/deployment-archive/scripts/*.sh; do
      perl -pi -e "s/{RELEASE_NUMBER}/${RELEASE_NUMBER}/g" < $file > ${file}.tmp && mv $file{.tmp,} ;
      perl -pi -e "s/{COMMIT_ID}/${COMMIT_ID}/g" < $file > ${file}.tmp && mv $file{.tmp,} ;
    done
    cd /tmp/BUILD
    for file in scripts/*.sh; do
      perl -pi -e "s/{RELEASE_NUMBER}/${RELEASE_NUMBER}/g" < $file > ${file}.tmp && mv $file{.tmp,} ;
      perl -pi -e "s/{COMMIT_ID}/${COMMIT_ID}/g" < $file > ${file}.tmp && mv $file{.tmp,} ;
    done
    sudo find . -print | zip -@ /tmp/hubdashboard_tmp.zip
    sudo mv /tmp/hubdashboard_tmp.zip /var/www/html/
    rm -rf /tmp/BUILD
fi


