#!/bin/bash
cd /var/www/html/
JIRA_RELEASE={RELEASE_NUMBER}
COMMIT_ID={COMMIT_ID}
EXPECTED_SIGNATURE=$(wget -q -O - https://composer.github.io/installer.sig)
sudo php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    >&2 echo 'ERROR: Invalid installer signature'
    sudo rm composer-setup.php
    exit 1
fi

sudo php composer-setup.php --quiet
RESULT=$?
sudo rm composer-setup.php
cd hubdashboard-${JIRA_RELEASE}
sudo ../composer.phar install --no-plugins --no-scripts --no-dev --optimize-autoloader
exit $RESULT

