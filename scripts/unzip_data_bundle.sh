#!/bin/bash
JIRA_RELEASE={RELEASE_NUMBER}
COMMIT_ID={COMMIT_ID}
cd /var/www/html/
if [ -d "hubdashboard-${JIRA_RELEASE}" ]; then
    echo "Deployment Version Already Exists. Update Version number in Buildspec.yml"
    exit -1
fi
sudo unzip -o hubdashboard*.zip -d "hubdashboard-${JIRA_RELEASE}"
sudo rm -f hubdashboard*.zip

cd hubdashboard-${JIRA_RELEASE}
if [ "$DEPLOYMENT_GROUP_NAME" == "HubDashboardFleetDev" ]; then    
    for file in `find . -name paramaters*.yml.dist`; do
        mv -f $file{,.tmp}; 
        perl -pi -e "s/prod-webediaurora.datatrans.internal/dev-webediaurora.datatrans.internal/g" < $file.tmp > $file; 
        rm $file.tmp;
    done
fi
#for file in {inc/config.php,webedi32/inc/config.php}; do
#    mv $file.{dist,tmp};
#    perl -pi -e "s/{JIRA_RELEASE}/${JIRA_RELEASE}/g" < $file.tmp > $file && mv $file{,.tmp} ;
#    perl -pi -e "s/{SERVER_DOMAIN}/www.datatranswebedi.com/g" < $file.tmp > $file && mv $file{,.tmp}; 
#    perl -pi -e "s/{SERVER_WILDDOMAIN}/datatranswebedi.com/g" < $file.tmp > $file && mv $file{,.tmp} ;
#    perl -pi -e "s/{WORKSPACE}/\/var\/www\/html\/hubdashboard-${JIRA_RELEASE}\//g" < $file.tmp > $file;
#done;
