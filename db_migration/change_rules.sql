ALTER TABLE `webedi30_demo`.`discrepancy_exception_rules` 
ADD COLUMN `stat_code` VARCHAR(45) NULL AFTER `priority`;
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('855', 'BAK09', '>', 'DTM002', 'Ack Past Due', '0', '0', '1', 'po-ack-pastdue');
UPDATE `webedi30_demo`.`discrepancy_exception_rules` SET `stat_code`='po-ack-missing' WHERE `id`='10';
UPDATE `webedi30_demo`.`discrepancy_exception_rules` SET `stat_code`='po-ack-discr-qty' WHERE `id`='1';
UPDATE `webedi30_demo`.`discrepancy_exception_rules` SET `stat_code`='po-ack-discr-qtyunit' WHERE `id`='2';
UPDATE `webedi30_demo`.`discrepancy_exception_rules` SET `stat_code`='po-ack-discr-price' WHERE `id`='3';
UPDATE `webedi30_demo`.`discrepancy_exception_rules` SET `stat_code`='po-inv-missing' WHERE `id`='11';
UPDATE `webedi30_demo`.`discrepancy_exception_rules` SET `stat_code`='po-inv-discr-qty' WHERE `id`='5';
UPDATE `webedi30_demo`.`discrepancy_exception_rules` SET `stat_code`='po-inv-discr-qtyunit' WHERE `id`='6';
UPDATE `webedi30_demo`.`discrepancy_exception_rules` SET `stat_code`='po-inv-discr-price' WHERE `id`='7';
UPDATE `webedi30_demo`.`discrepancy_exception_rules` SET `stat_code`='po-ack-pastdue' WHERE `id`='13';
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('855', 'BAK03', 'duplicate', 'BEG03', 'Ack Duplicate', '0', '0', '1', 'po-ack-duplicate');
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('810', 'BIG02', 'duplicate', 'BEG03', 'Invoice Duplicate', '0', '0', '1', 'po-inv-duplicate');
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('856', 'BSN02', 'duplicate', 'BEG03', 'ASN Duplicate', '0', '0', '1', 'po-asn-duplicate');
UPDATE `webedi30_demo`.`discrepancy_exception_rules` SET `stat_code`='po-ack-missing' WHERE `id`='4';
UPDATE `webedi30_demo`.`discrepancy_exception_rules` SET `stat_code`='po-asn-discr-shipnot' WHERE `id`='12';
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('856', 'BSN03', '>', 'DTM001', 'Shipped after Cancel date', '0', '0', '1', 'po-asn-cancel');
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('856', 'BSN03', '>', 'DTM061', 'Shipped after Cancel date', '0', '0', '2', 'po-asn-cancel');
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('856', 'BSN03', '>', 'DTM175', 'Shipped after Cancel date', '0', '0', '3', 'po-asn-cancel');
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('856', 'BSN03', '>', 'DTM177', 'Shipped after Cancel date', '0', '0', '4', 'po-asn-cancel');
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('856', 'BSN03', '>', 'DTM010', 'Shipped Late', '0', '0', '1', 'po-asn-discr-shiplate');
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('856', 'BSN03', '>', 'DTM038', 'Shipped Late', '0', '0', '2', 'po-asn-discr-shiplate');
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('856', 'BSN03', '>', 'DTM081', 'Shipped Late', '0', '0', '3', 'po-asn-discr-shiplate');
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('856', 'BSN03', '>', 'DTM084', 'Shipped Late', '0', '0', '4', 'po-asn-discr-shiplate');
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('856', 'BSN03', '>', 'DTM087', 'Shipped Late', '0', '0', '5', 'po-asn-discr-shiplate');
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('855', 'BAK09', '>', 'DTM010', 'Ack Past Due', '0', '0', '1', 'po-ack-pastdue');
UPDATE `webedi30_demo`.`discrepancy_exception_rules` SET `desc`='Ack Past Due' WHERE `id`='13';
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('855', 'BAK09', '>', 'DTM996', 'Ack Past Due', '0', '0', '2', 'po-ack-pastdue');
INSERT INTO `webedi30_demo`.`discrepancy_exception_rules` (`doc_type`, `src`, `operator`, `tgt`, `desc`, `customer`, `tp`, `priority`, `stat_code`) VALUES ('855', 'BAK09', '>', 'DTM106', 'Ack Past Due', '0', '0', '3', 'po-ack-pastdue');



USE `webedi30_demo`;
DROP procedure IF EXISTS `recalculate_partner_stats`;
