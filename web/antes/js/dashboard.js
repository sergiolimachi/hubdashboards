/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Load report: chart and data table.
 * url: path to load the report
 * onClick: click event handler for chart series/table rows.
 * originSID: origin serie ID (back button)
 * onClickBack: click event handler to return to the previous chart/table.
 * 
 */
function loadReport(url, onClick, originSID, onClickBack){
    $.ajax({
        url: url,
        beforeSend: function(){
            $('#chartBack').hide();
            $('#progressBar').fadeIn(300);
            $('#mainChart, #tableContainer').fadeOut(250);
            $('#noDataAlert').alert('close');
        },
        success: function(data){
            if(data.series){
                if(data.series[0].data.length > 0){
                    var series = data.series;
                    for(var s=0; s<series.length; s++){
                        var serie = series[s];
                        for(var d=0; d<serie.data.length; d++){
                            var sdata = serie.data[d];
                            if(sdata.y > 0) {
                                if(sdata.events !== undefined){
                                    sdata.events.click = onClick;
                                }else{
                                    data.plotOptions.series.point.events.click = onClick;
                                }
                            }
                        }
                    }
                    chart = new Highcharts.Chart(data);
                    fillTable('mainTable', data.series, onClick);
                    $('#mainChart, #tableContainer').fadeIn(500);
                }else{
                    var alert = '<div id="noDataAlert" class="alert alert-dismissable alert-info col-md-6 col-md-offset-3">';
                    alert += '<button type="button" class="close" data-dismiss="alert">×</button>';
                    alert += '<i class="glyphicon glyphicon-info-sign"></i> There is no data for the filter input.</div>';
                    $('.panel-body').append(alert);
                }
            }
        },
        error: function(x, e, msg){
            $('#mainChart, #tableContainer').html('');
            //$('#loadResultMessage').html('<i class="glyphicon glyphicon-remove-sign"></i> <b>'+e+':</b> '+msg).fadeIn(300).delay(5000).fadeOut(500);
            $.growl.error({ title: e, message: msg });
        },
        complete: function(){
            if(onClickBack){
                $('#chartBack').unbind('click')
                        .bind('click', {'serieID': $('#chartBack').data('backid')}, onClickBack)
                        .data('backid', originSID)
                        .show();
            }
            $('#progressBar').fadeOut(300);
        }
    });
}


function fillTable(table, series, onClick){
    $('#tableContainer').html('<table id="mainTable" class="table table-striped table-hover"><thead></thead><tbody></tbody></table>');
    for(var s=0; s<series.length; s++){
        var serie = series[s];
        if(series.length===1){
            var header = '<tr>';
            header += '<th class="col-sm-1"></th>';
            header += '<th class="col-sm-9">' + serie.name + '</th>';
            header += '<th class="col-sm-2 text-right">Qty</th>';
            header += '</tr>';
            $('#mainTable > thead').append(header);
        }else if(s===0){
            var header = '<tr>';
            header += '<th></th>';
            header += '<th>Baseline Item Data</th>';
            header += '<th class="text-center">Qty</th>';
            header += '</tr>';
            $('#mainTable > thead').append(header);
        }
        for(var d=0; d<serie.data.length; d++){
            var data = serie.data[d];
            var row = '<tr>';
            if(data.id !== undefined && data.name !== undefined){
                row += '<td class="text-right">' + (d+1) + '</td>';
                row += '<td>' + data.name + '</td>';
                row += '<td class="text-right">' + data.y + '</td>';
                //row += '<td>' + data.id + '</td>';
            }else{
                row += '<td class="text-right">' + (s+1) + '</td>';
                row += '<td>' + serie.name + '</td>';
                row += '<td class="text-right">' + data + '</td>';
                //row += '<td>' + serie.id + '</td>';
            }
            row += '</tr>';
            $('#mainTable > tbody').append(row);
            if(onClick !== null && (data > 0 || data.y > 0)){
                $('#'+table+' > tbody > tr:last')
                        .bind('click', {'serieID': (data.id ? data.id : serie.name)}, onClick)
                        .css('cursor', 'pointer');
            }
        }
    }
    /*
    $('#mainTable').dataTable({
        dom: 't',
        scrollY: "50vh",
        scrollCollapse: true,
        paging: false,
    });
    */
}

    function getStatistics(url) {
        $.ajax({
            url: url,            
            success: function(data) {
                var tr = '';
                tr += '         <tr>';
                tr += '             <th class="col-sm-4">';
                tr += '                 Documents';
                tr += '             </th>';
                tr += '             <td class="col-sm-2 text-right text-info">' + data[0].total + '</td>';
                tr += '             <td class="col-sm-2 text-right text-success">' + data[0].success + '</td>';
                tr += '             <td class="col-sm-2 text-right text-warning">' + data[0].warnings+ '</td>';
                tr += '             <td class="col-sm-2 text-right text-danger">' + data[0].errors + '</td>';
                tr += '         </tr>';
                tr += '         <tr>';
                tr += '             <th class="col-sm-4">';
                tr += '                 Processes';
                tr += '             </th>';
                tr += '             <td class="col-sm-2 text-right text-info">';
                tr +=                   data[1].total;
                tr += '             </td>';
                tr += '             <td class="col-sm-2 text-right text-success">';
                tr +=                   data[1].success;
                tr += '             </td>';
                tr += '             <td class="col-sm-2 text-right text-warning">';
                tr +=                   data[1].warnings;
                tr += '             </td>';
                tr += '             <td class="col-sm-2 text-right text-danger">';
                tr +=                   data[1].errors;
                tr += '             </td>';
                tr += '         </tr>';

                $('#tableStatitics > tbody').html(tr);
            }
        });
        
    };

    function suppliers(url,callbackData,t) {
        $.ajax({
            url: url,        
            success: function(data) {
                callbackData(t, data);
            }
        });
    }
    
    function suppliers_success(t, list) {   
        var vendors = [];
        var scores = [];
        var tpids = [];
        for (var i = 0; i < list['data'].length - 1; i++) {
            vendors.push(list['data'][i][0]);
            scores.push(list['data'][i][1]);
            tpids.push(list['data'][i][2]);
        }        
        for (var i = 0; i < vendors.length - 1; i++) {
            
            var grade = '';
            var symbol = '%';          

            if (scores[i] >= 90) {
                grade = '<span class="label label-success"><b>&nbsp;&nbsp;A&nbsp;&nbsp;</b></span>';
            } else if (scores[i] >= 80) {
                grade = '<span class="label label-primary"><b>&nbsp;&nbsp;B&nbsp;&nbsp;</b></span>';
            } else if (scores[i] >= 70) {
                grade = '<span class="label label-info"><b>&nbsp;&nbsp;C&nbsp;&nbsp;</b></span>';
            } else if (scores[i] >= 60) { 
                grade = '<span class="label label-warning"><b>&nbsp;&nbsp;D&nbsp;&nbsp;</b></span>';
            } else if (scores[i] >= 0 && (scores[i] < 60)){
                grade = '<span class="label label-danger"><b>&nbsp;&nbsp;F&nbsp;&nbsp;</b></span>';
            } else if(scores[i] === 'N/A'){
                grade = '<span class="label label-default" style="color:white">&nbsp;X&nbsp;</span>';
                symbol = '';
            }            
            
            var rowHtml = $('#row').html()           
            .replace('{scoreo}', scores[i])
            .replace('{namev}', vendors[i])
            .replace('{score}', scores[i])
            .replace('{symbol}',symbol)
            .replace('{grade}', grade)
            .replace('{tpids}', tpids[i]);
            //Add new row dinamically            
            t.row.add($(rowHtml)).draw();            
        }
    }
    
    function getSuppliersScore(url) {
        $.ajax({
            url: url,           
            success: function(data) {   
                var score = data['data'][0][1];
                $('#score').html(score);
                if (score >= 90) {
                    $('#grade').html('&nbsp;&nbsp;A&nbsp;&nbsp;').addClass('label-success');
                    $('#stars').html('<i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i>');
                } else if (score >= 80) {
                    $('#grade').html('&nbsp;&nbsp;B&nbsp;&nbsp;').addClass('label-primary');
                    $('#stars').html('<i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i>');
                } else if (score >= 70) {
                    $('#grade').html('&nbsp;&nbsp;C&nbsp;&nbsp;').addClass('label-info');
                    $('#stars').html('<i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i>');
                } else if (score >= 60) {
                    $('#grade').html('&nbsp;&nbsp;D&nbsp;&nbsp;').addClass('label-warning');
                    $('#stars').html('<i class="glyphicon glyphicon-star"></i>');
                } else if (score >= 0 && score < 60){
                    $('#grade').html('&nbsp;&nbsp;F&nbsp;&nbsp;').addClass('label-danger');
                    $('#stars').html('');
                } else if(score === 'N/A'){
                    $('#grade').html('&nbsp;&nbsp;N/A&nbsp;&nbsp;').addClass('label-default');
                    $('#stars').html('');
                }                                     
            }
        });
        
    };
    
    function getProcessByResults(url) {
        $.ajax({
            url: url,            
            success: function(data) {
                $('#activityByResultsChart').highcharts({
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                        min: 0,
                        plotLines: [{
                                value: 0,
                                width: 1,
                                color: '#808080'
                            }]
                    },
                    tooltip: {
                        valueSuffix: ''
                    },
                    plotOptions: {
                        series: {
                            point: {
                                events: {
                                    click: function () {
                                        $('#activityByResultsChart').fadeOut(200);
                                        loadProcessResultChart(data[this.category],this.series.name);
                                        $('#activityByResultsChart, #btnDrillup').fadeIn(200);
                                    }
                                }
                            }
                        }
                    },
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom',
                        x: 0,
                        y: 0,
                        floating: false,
                        borderWidth: 0,
                        //backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                        shadow: false,
                        itemStyle: {
                            //color: 'white',
                            //fontWeight: 'bold',
                            fontSize: '9px'
                        }
                    },
                    series: [{
                            name: 'Shipping Late',                            
                            data: data['shipLate']
                        }, {
                            name: 'Ack past due',                            
                            data: data['ackPasDue']
                        }, {
                            name: 'Duplicate documents',                            
                            data: data['duplicateSend']
                        }, {
                            name: 'Order Shipped after Cancel',                            
                            data: data['shipAfterCancel']
                        }, {
                            name: 'Missing req Response Doc',                            
                            data: data['respMissing']
                        }],
                    lang: {
                            noData: "No data available in chart"
                            },
                    noData: {
                        style: {
                            fontWeight: 'bold',
                            fontSize: '15px',
                            color: '#303030'
                        }
                    }                    
                });
            }
        });
        
    };
    
    function getProcessCompliance(url) {
        $.ajax({
            url: url,
            
            success: function(data) {
                $('#activityComplianceChart').highcharts({
                    chart: {
                        type: 'bar'
                    },
                    colors: ['#42763D', '#8A6D41', '#A9444D'],
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        //categories: ['Processes']
                        type: 'category'
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Business Processes'
                        }
                    },
                    legend: {
                        enabled: false,
                    },
                    plotOptions: {
                        series: {
                            //stacking: 'percent'
                        }
                    },

                    series: [{
                        name: 'Processes',
                        colorByPoint: true,
                        data: data['series']
                        }],
                    drilldown: {
                        drillUpButton: {
                            relativeTo: 'spacingBox',
                            position: {
                                y: -10,
                                x: 10
                            },
                        },
                        
                        series: [{
                                name: 'Success',
                                id: 'success',
                                data: [['850 vs 855',data['drilldown'][0]['successAck']],['850 vs 856',data['drilldown'][0]['successAsn']],['850 vs 810',data['drilldown'][0]['successInv']]]
                            },{
                                name: 'Warning',
                                id: 'warning',
                                data: [['850 vs 855',data['drilldown'][1]['warningAck']],['850 vs 856',data['drilldown'][1]['warningAsn']],['850 vs 810',data['drilldown'][1]['warningInv']]]
                            },{
                                name: 'Error',
                                id: 'error',
                                data: [['850 vs 855',data['drilldown'][2]['errorAck']],['850 vs 856',data['drilldown'][2]['errorAsn']],['850 vs 810',data['drilldown'][2]['errorInv']]]
                            }]                        
                    }
                });                
            }
        });
        
    };    
    
    
    function getSyntaxCompliance(url) {
        $.ajax({
            url: url,
            
            success: function(data) {
                //if(data[0].length || data[1].length || data[2].length){
                if($('#syntaxComplianceChart').highcharts()) {
                    var chart=window.Highcharts.charts[$('#syntaxComplianceChart').data('highchartsChart')];
                    chart.series[0].setData(data['series']);
                }
                    $('#syntaxComplianceChart').highcharts({
                        chart: {
                            type: 'bar'
                        },
                        colors: ['#42763D', '#8A6D41', '#A9444D'],
                        credits: {
                            enabled: false
                        },
                        exporting: {
                            enabled: false
                        },
                        title: {
                            text: ''
                        },
                        xAxis: {
                            //categories: ['Documents']
                            type: 'category'
                        },
                        yAxis: {

                            min: 0,
                            title: {
                                text: 'Documents'
                            }
                        },
                        legend: {
                            enabled: false,
                        },
                        plotOptions: {
                            series: {
                                //stacking: 'percent'
                            }
                        },
                        series: [{
                            name: 'Documents',
                            colorByPoint: true,
                            data: data['series']
                            }],

                        drilldown: {
                            drillUpButton: {
                                relativeTo: 'spacingBox',
                                position: {
                                    y: -10,
                                    x: 10
                                },
                            },
                            series: [{
                                    name: 'Success',
                                    id: 'success',
                                    data: [['855',data['drilldown'][0]['successAck']],['856',data['drilldown'][0]['successAsn']],['810',data['drilldown'][0]['successInv']]]
                                },{
                                    name: 'Warning',
                                    id: 'warning',
                                    data: [['855',data['drilldown'][1]['warningAck']],['856',data['drilldown'][1]['warningAsn']],['810',data['drilldown'][1]['warningInv']]]
                                },{
                                    name: 'Error',
                                    id: 'error',
                                    data: [['855',data['drilldown'][2]['errorAck']],['856',data['drilldown'][2]['errorAsn']],['810',data['drilldown'][2]['errorInv']]]
                                }]
                            
                        }                    
                    });
//                }else{
//                    
//                    $('#syntaxComplianceChart').html('<table class="table table-striped table-hover table-condensed dataTable no-footer" role="grid" aria-describedby="tableSuppliers_info"><tbody><tr class="odd"><td valign="top" colspan="3" class="dataTables_empty">No data available in chart</td></tr></tbody></table>');
//                }
            }
        });
        
    };    

function htmlDecode(value) {
    return value.replace(/&quot;/g, "\"").replace(/&lt;/g, "<").replace(/&gt;/g, ">");
}


//function clickChart(e) {
//    //alert('Details activityByResultsChart:' + e.y + ' ' + e.x + ' ' + this.category);
//    $('#activityByResultsChart').fadeOut(200);
//    //loadProcessResultChart(this.category);
//    loadProcessResultChart( [ e.x/2, e.y*3, parseInt((e.x+e.y)/2), parseInt(e.x/2+e.y/2) ] );
//    $('#activityByResultsChart, #btnDrillup').fadeIn(200);
//}

    function loadProcessResultChart(values,sname) {
        switch (sname) {
            case 'Shipping Late':
                dd = "ddshipLate";
                break;
            case 'Ack past due':
                dd = "ddackPasDue";
                break;
            case 'Duplicate documents':
                dd = "ddduplicateSend";
                break;
            case 'Order Shipped after Cancel':
                dd = "ddshipAfterCancel";
                break;
            case 'Missing req Response Doc':
                dd = "ddrespMissing";
                break;
        }
        $('#activityByResultsChart').highcharts({
            chart: {
                type: 'bar'
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: ['']
            },
            yAxis: {
                min: 0,
                title: {
                    text: sname
                }
            },
            tooltip: {
                formatter: function () {
                   return this.series.name + ': ' +  this.y.toFixed(1);
                }                
            },    
            legend: {
                reversed: true
            },
            plotOptions: {
                series: {
                    stacking: 'percent'
                }
            },
            series: [{
                name: Object.keys(values[dd])[0],
                data: [values[dd][Object.keys(values[dd])[0]]]
            }, {
                name: Object.keys(values[dd])[1],
                data: [values[dd][Object.keys(values[dd])[1]]]
            }, {
                name: Object.keys(values[dd])[2],
                data: [values[dd][Object.keys(values[dd])[2]]]
            }]
        });
    }
    
    function onDrillup() {
        $('#activityByResultsChart, #btnDrillup').fadeOut(200);
        loadProcessByResults();
        $('#activityByResultsChart').fadeIn(200);
    }    
	var print_r = function(obj,t){

	    // define tab spacing
	    var tab = t || '';
	
	    // check if it's array
	    var isArr = Object.prototype.toString.call(obj) === '[object Array]' ? true : false;
	
	    // use {} for object, [] for array
	    var str = isArr ? ('Array\n' + tab + '[\n') : ('Object\n' + tab + '{\n');

	    // walk through it's properties
	    for(var prop in obj){
		if (obj.hasOwnProperty(prop)) {
		    var val1 = obj[prop];
		    var val2 = '';
		    var type = Object.prototype.toString.call(val1);
		    switch(type){
			
		        // recursive if object/array
		        case '[object Array]':
		        case '[object Object]':
		            val2 = print_r(val1, (tab + '\t'));
		            break;
					
		        case '[object String]':
		            val2 = '\'' + val1 + '\'';
		            break;
					
		        default:
		            val2 = val1;
		    }
		    str += tab + '\t' + prop + ' => ' + val2 + ',\n';
		}
	    }
	
	    // remove extra comma for last property
	    str = str.substring(0, str.length-2) + '\n' + tab;
	
	    return isArr ? (str + ']') : (str + '}');
	}; 
