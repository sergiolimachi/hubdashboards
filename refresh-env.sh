#!/bin/bash

if [ "$1" != "" ]; then
    sudo rm -rf app/cache/$1/*
    sudo app/console cache:clear --env=$1
    sudo app/console assets:install --env=$1
    sudo app/console assetic:dump --env=$1
else
    sudo rm -rf app/cache/dev/*
    sudo app/console cache:clear
    sudo app/console assets:install
    sudo app/console assetic:dump
fi

sudo chmod 777 -R app/cache/ app/logs/

if [ "$2" != "" ] && [ "$3" != "" ]; then
    sudo chown $2:$3 -R app/cache/ app/logs/
fi

if [ "$2" != "" ]; then
    sudo chown $2:$2 -R app/cache/ app/logs/
fi

