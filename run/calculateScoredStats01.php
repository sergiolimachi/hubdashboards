<?php
class HubScoreStats {
    //Log file
    var $_flogs;
    var $status;
    var $mode;
    var $totalAsn;
    var $totalAck;
    var $totalInv;
    var $cancel;
    var $duplicate;
    var $missing;
    var $slate;
    var $pastdue;
    var $discrep;
    var $totalPo;

    function __construct($mode = null){
        $this->_flogs = '/var/www/hubdashboard/app/logs/score.log';
        $this->status = "Success";
        $this->mode = isset($mode) ? $mode : "run";
        $this->totalAsn = 0;
        $this->totalAck = 0;
        $this->totalInv = 0;
        $this->cancel = 0;
        $this->duplicate = 0;
        $this->missing = 0;
        $this->slate = 0;
        $this->pastdue = 0;
        $this->discrep = 0; 
        $this->totalPo = 0;
        
    }    
    
    private function conndb($db){
        //connect to replication to calculate statistics
        try {
            $conn = new PDO('mysql:host=prod-webediaurora.datatrans.internal;port=3306;dbname=webedi30', 'dtadmin', 'Ellx3~XAMRxsyUj4Y');
            //error_log(date('Y-m-d H:i:s') . ": Successful Conection PRD1\r", 0);
            return $conn;
        } catch (PDOException $ex) {
            error_log(date('Y-m-d H:i:s') . ": " . $ex->getMessage() . "\r", 3, $this->_flogs);
        }
    }

    private function updateXvalueStats($tp = null, $customer = null) {
        try{
            set_time_limit(0);
            $statPrefix = 'po';
            
            $sql = "UPDATE PartnerStatistics SET xvalue=CAST(xvalue AS UNSIGNED)+1 WHERE type LIKE '$statPrefix-%' ";
            if ($tp != null) {
                $sql = $sql . " AND userid=$tp ";
            }
            if ($customer != null) {
                $sql = $sql . " AND series='$customer' ";
            }
            $sql = $sql ." order by CAST(xvalue AS UNSIGNED) desc;";

            if($this->mode === 'debug'){
                $updates = 0;
            }else{
                //$deletes = 0;
                $updates = $this->doUpdate($sql);             
            }
            
            $resp = ($this->mode === 'debug' ? "MODE: " . strtoupper($this->mode) . "\r" : " ")
                    . "Update XvalueStats executed [ status: " . $this->status 
                    . ($this->status === 'Success' ? " | rows updates: ". $updates : " See log at: ". $this->_flogs)
                    . ($tp != null ? " | tp: " . $tp : "")
                    . ($customer != null ? " | cust: " . $customer : "")
                    . "\r with sql: ". $sql
                    . " ]";
            error_log(date('Y-m-d H:i:s') . ": " . $resp . "\r", 3, $this->_flogs);
            return (1);
            
        }catch (Exception $e){
            $this->status = "Error";
            error_log(date('Y-m-d H:i:s') . ": Failed Execution: " . $e->getMessage() . "\r", 3, $this->_flogs);
            return (0);
        }
    }

    private function deleteScoreStats($start = null, $end = null, $doctype2 = null, $tp = null, $customer = null) {
        try{
            
            set_time_limit(0);
            $statPrefix = '';
            switch ($doctype2) {
                case 810:
                    $statPrefix = 'po-inv';
                    break;
                case 855:
                    $statPrefix = 'po-ack';
                    break;
                case 856:
                    $statPrefix = 'po-asn';
                    break;
                default:
                    $statPrefix = 'po';
            }

            $sql = "DELETE FROM PartnerStatistics WHERE type LIKE '$statPrefix-%' ";
            if ($tp != null) {
                $sql = $sql . " AND userid=$tp ";
            }
            if ($customer != null) {
                $sql = $sql . " AND series='$customer' ";
            }
            if ($start != null && $end != null ) {
                $from = $this->dateToXvalue($start);
                $end = $this->dateToXvalue($end);            
                $sql = $sql . " AND xvalue BETWEEN $end AND $from ";
            }        
            $sql = $sql . ";";

            
            if($this->mode === 'debug'){
                $deletes = 0;
            }else{
                //$deletes = 0;
                $deletes = $this->doDelete($sql);             
            }
            
            $resp = ($this->mode === 'debug' ? "MODE: " . strtoupper($this->mode) . "\r" : " ")
                    . "Delete ScoreStats executed [ status: " . $this->status 
                    . ($this->status === 'Success' ? " | rows deletes: ". $deletes : " See log at: ". $this->_flogs)
                    . ($doctype2 != null ? " | for: " . $doctype2 : "")
                    . ($startDate != null ? " | from: " . $startDate : "")
                    . ($endDate != null ? " | to: " . $endDate : "")
                    . ($tp != null ? " | tp: " . $tp : "")
                    . ($customer != null ? " | cust: " . $customer : "")
                    . "\r with sql: ". $sql
                    . " ]";
            error_log(date('Y-m-d H:i:s') . ": " . $resp . "\r", 3, $this->_flogs);
            
        }catch (Exception $e){
            $this->status = "Error";
            error_log(date('Y-m-d H:i:s') . ": Failed Execution: " . $e->getMessage() . "\r", 3, $this->_flogs);
        }    
    }
    
    private function generateScoreStats($doctype2, $startDate, $endDate = null, $tp = null, $customer = null) {
        try{        
            $conn = $this->conndb('dr1');
            set_time_limit(0);
            $doctype1 = 850;
            $segment1 = 'PO1';
            $segmentdtm = 'DTM';
            switch ($doctype2) {
                case 810:
                    $statPrefix = 'po-inv-discr';
                    $statPrefixSc = 'po-inv';
                    $segment2 = 'IT1';
                    break;
                case 855:
                    $statPrefix = 'po-ack-discr';
                    $statPrefixSc = 'po-ack';
                    $segment2 = 'PO1';
                    $dtmQualifiers = '010, 996, 106, 002';
                    break;
                case 856:
                    $statPrefix = 'po-asn-discr';
                    $statPrefixSc = 'po-asn';
                    $segment2 = 'PO1';
                    $dtmQualifiers = '010, 038, 081, 084, 087';
                    $dtmQualifiers2 = '001, 061, 175, 177'; //cancel after
                    break;
                default:
                    $this->status = "Error";
                    return 0;
            }

            $sdate = new \DateTime($startDate);
            $sdate->setTime(00, 00, 00);
            $startDate = $sdate->format('Y-m-d H:i:s');

            if ($endDate == null) {            
                $date = new \DateTime($startDate);
                $date->setTime(23, 59, 59);
                $endDate = $date->format('Y-m-d H:i:s');

            }else{            
                $edate = new \DateTime($endDate);
                $edate->setTime(23, 59, 59);     
                $endDate = $edate->format('Y-m-d H:i:s');
            }
            //1: Find doctypes 850 sent by customer and received by TP
            $sql = "SELECT * FROM Messages WHERE srdatetime BETWEEN '$startDate' AND '$endDate' AND doctype=$doctype1 AND folder ='sent'";
            if ($tp != null) $sql = $sql . " AND customer=$tp";
            if ($customer != null) $sql = $sql . " AND tp=$customer ";
            $sql = $sql . ";";
            $msgs = $this->doQuery($sql,$conn);
            $insertOut = array(); // To discrepancies type error
            $insertWar = array(); // To discrepancies type warning
            $this->totalPo = sizeof($msgs) > 0 ? sizeof($msgs) : 0;
            
            foreach ($msgs as $msg) {
                $exit = false;
                if (!empty($msg['srdatetime'])) {
                    $msgdate = new \DateTime($msg['srdatetime']);
                }
                
                $xvalue = $this->dateToXvalue($msgdate);

                // Find document associates (doctype2)
                $sql = "SELECT * FROM MessageReferences ref INNER JOIN Messages m ON ref.FK_message = m.PK_id WHERE ref.reference='{$msg[reference]}' AND m.doctype=$doctype2 AND m.folder = 'inbox';";
                $assocs = $this->doQuery($sql,$conn);
                $tdoctype2 = sizeof($assocs);
                //error_log(date('Y-m-d H:i:s') . " sqlassocs: " . $sql . "\r", 3, $this->_flogs);
                //error_log(date('Y-m-d H:i:s') . " tdoctype: " . $tdoctype2 . "\r", 3, $this->_flogs);
                if($tdoctype2 > 0){
                    //TOTALS: Stores the quantity of documents(810,855,856 by customer, tp, xvalue) used in the calculation to best performance at frontend)
                    $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefixSc-total',{$msg['tp']},$xvalue,$tdoctype2) ON DUPLICATE KEY UPDATE data = data +$tdoctype2;";
                    //error_log(date('Y-m-d H:i:s') . " sqlins: " . $sql . "\r", 3, $this->_flogs);
                    
                    array_push($insertOut,$sql);
                    if($doctype2 == 855) $this->totalAck = $this->totalAck+$tdoctype2;
                    if($doctype2 == 856) $this->totalAsn = $this->totalAsn+$tdoctype2;
                    if($doctype2 == 810) $this->totalInv = $this->totalInv+$tdoctype2;                     
                }

                // Find nodes at PO - 850 by segment PO1       
                $sql1 = "SELECT n.PK_id, e.index, e.data FROM Elements e INNER JOIN Nodes n ON n.PK_id = e.nodepointer "
                      . "WHERE e.rootnode={$msg[rootnode]} AND n.title = '$segment1' AND e.index IN (1,2,3,4,5);";
                $nodes1 = $this->doQuery($sql1,$conn);

                $nodespo = array();
                foreach ($nodes1 as $node) {
                    if (!isset($nodespo[$node['PK_id']])) {
                        $nodespo[$node['PK_id']] = array();
                    }
                    $nodespo[$node['PK_id']][$node['index']] = $node['data'];
                }

                // Find nodes at PO - 850 by segment DTM to calculate ACK_PAST_DUE, ASN_SHIPLATE, SHIP_AFTER_CANCEL
                $sql1 = "SELECT n.PK_id, e.index, e.data FROM Elements e INNER JOIN Nodes n ON n.PK_id = e.nodepointer "
                      . "WHERE e.rootnode={$msg[rootnode]} AND n.title = '$segmentdtm' AND e.index IN (1,2,3,4,5);";
                $nodes1 = $this->doQuery($sql1,$conn);

                $nodespodtm = array();
                foreach ($nodes1 as $node) {
                    if (!isset($nodespodtm[$node['PK_id']])) {
                        $nodespodtm[$node['PK_id']] = array();
                    }
                    $nodespodtm[$node['PK_id']][$node['index']] = $node['data'];
                }
                
                //1: Calculate Duplicated Documents
                $sql3 = "SELECT m2.PK_id FROM Messages m1 JOIN MessageReferences r ON m1.reference=r.reference "
                      . "JOIN Messages m2 ON r.FK_message=m2.PK_id WHERE m1.doctype = $doctype1 AND m2.doctype = $doctype2 "
                      . "AND m1.reference='{$msg[reference]}' AND m2.folder = 'inbox' GROUP BY m2.PK_id HAVING COUNT(*) > 1;";
                      
                $duplicates = $this->doQuery($sql3,$conn);

                if(sizeof($duplicates) > 0){
                    $exit = true;
                    $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefixSc-duplicate',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                    array_push($insertOut,$sql);
                    $this->duplicate++;
                }else{                
                
                    if(sizeof($assocs) > 0){                       
                        foreach ($assocs as $msg2) {                          
                            $sd = false;  
                            $exit = false;
                            $wexit = false;
                            // Find nodes at ResponseDocument (ACK,ASN,INV)               
                            $sql2 = "SELECT n.PK_id, e.index, e.data FROM Elements e INNER JOIN Nodes n ON n.PK_id = e.nodepointer "
                                    . "WHERE e.rootnode=$msg2[rootnode] AND n.title = '$segment2' AND e.index IN (1,2,3,4,5);";
                            $nodes2 = $this->doQuery($sql2,$conn);
                            $nodesdoc2 = array();
                            foreach ($nodes2 as $node) {
                                if (!isset($nodesdoc2[$node['PK_id']])) {
                                    $nodesdoc2[$node['PK_id']] = array();
                                }
                                $nodesdoc2[$node['PK_id']][$node['index']] = $node['data'];
                            }
                            //1: Calculate Discrepancies PO vs Response(ACK,ASN,INV) 

                            foreach ($nodespo as $nodepo) {
                                foreach ($nodesdoc2 as $nodedoc2) {
                                    if (isset($nodepo[1]) && isset($nodedoc2[1]) && $nodepo[1] == $nodedoc2[1]) {
                                        if (isset($nodepo[2]) && isset($nodedoc2[2]) && $nodepo[2] != $nodedoc2[2]) {
                                            //PO1:02 - Quantity
                                            $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefix-qty',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                            array_push($insertOut,$sql);
                                            //Total documents discrepancies to calculate score suppliers
                                            if(!$sd){
                                                $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefixSc-sdiscr',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                                array_push($insertOut,$sql);
                                                $sd = true;
                                            }
                                            $this->discrep++;
                                        }
                                        if (isset($nodepo[3]) && isset($nodedoc2[3]) && $nodepo[3] != $nodedoc2[3]) {
                                            //PO1:03 - Quantity Unit
                                            $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefix-qtyunit',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                            array_push($insertOut,$sql);
                                            //Total documents discrepancies to calculate score suppliers
                                            if(!$sd){
                                                $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefixSc-sdiscr',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                                array_push($insertOut,$sql);
                                                $sd = true;
                                            }
                                            $this->discrep++;
                                        }
                                        if (isset($nodepo[4]) && isset($nodedoc2[4]) && $nodepo[4] != $nodedoc2[4]) {
                                            //PO1:04 - Price
                                            $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefix-price',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                            array_push($insertOut,$sql);
                                            //Total documents discrepancies to calculate score suppliers
                                            if(!$sd){
                                                $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefixSc-sdiscr',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                                array_push($insertOut,$sql);
                                                $sd = true;
                                            }   
                                            $this->discrep++;
                                        }
                                        if (isset($nodepo[5]) && isset($nodedoc2[5]) && $nodepo[5] != $nodedoc2[5]) {
                                            //PO1:05 - Price Unit
                                            $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefix-priceunit',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                            array_push($insertOut,$sql);
                                            //Total documents discrepancies to calculate score suppliers
                                            if(!$sd){
                                                $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefixSc-sdiscr',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                                array_push($insertOut,$sql);
                                                $sd = true;
                                            }
                                            $this->discrep++;
                                        }
                                    }
                                }
                            }

                            $datedoc2 = (isset($msg2['srdatetime']) && ($msg2['folder'] == 'inbox'))  ? new \DateTime($msg2['srdatetime']) : '0000-00-00 00:00:00';

                            //2 ASN: Calculate score stats to ASN
                            if($doctype2 == 856){
                                foreach ($nodespodtm as $key => $dtmnodepo) {
                                    if(strlen($dtmnodepo[2])  == 8){
                                        $datepo_dtm = substr($dtmnodepo[2], 0,4)."-". substr($dtmnodepo[2], 4,2)."-". substr($dtmnodepo[2], 6,2);
                                    }elseif(strlen($dtmnodepo[2])  == 6){
                                        $datepo_dtm = substr($dtmnodepo[2], 0,2)."-". substr($dtmnodepo[2], 2,2)."-". substr($dtmnodepo[2], 4,2);
                                    }else{
                                        $datepo_dtm = '0000-00-00';
                                    }  
                                    $dateReqDoc2 = new \DateTime($datepo_dtm);
                                    $dateReqDoc2->setTime(00, 00, 00);

                                    //2 ASN: Ship after cancel
                                    if(substr_count($dtmQualifiers2, $dtmnodepo[1]) > 0){
                                        if($datedoc2 !== '0000-00-00 00:00:00'){
                                            if($datedoc2 > $dateReqDoc2){
                                                $exit = true;
                                                $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefixSc-cancel',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                                array_push($insertOut,$sql);
                                                $this->cancel++;
                                                break;
                                            }
                                        }
                                    }                            

                                    //2 ASN: Asn Ship Late
                                    if(substr_count($dtmQualifiers, $dtmnodepo[1]) > 0){
                                        if($datedoc2 !== '0000-00-00 00:00:00'){
                                            if($datedoc2 > $dateReqDoc2){
                                                $wexit = true;
                                                $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefix-shiplate',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                                array_push($insertWar,$sql);
                                                $this->slate++;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            
                            if($exit || $wexit) continue;
                            
                            //3 ACK: Calculate score stats to ACK
                            if($doctype2 == 855){
                                foreach ($nodespodtm as $key => $dtmnodepo) {
                                    if(substr_count($dtmQualifiers, $dtmnodepo[1]) > 0){
                                        if(strlen($dtmnodepo[2])  == 8){
                                            $datepo_dtm = substr($dtmnodepo[2], 0,4)."-". substr($dtmnodepo[2], 4,2)."-". substr($dtmnodepo[2], 6,2);
                                        }elseif(strlen($dtmnodepo[2])  == 6){
                                            $datepo_dtm = substr($dtmnodepo[2], 0,2)."-". substr($dtmnodepo[2], 2,2)."-". substr($dtmnodepo[2], 4,2);
                                        }else{
                                            $datepo_dtm = '0000-00-00';
                                        }
                                        $dateReqDoc2 = new \DateTime($datepo_dtm);
                                        $dateReqDoc2->setTime(00, 00, 00); 
                                        if($datedoc2 !== '0000-00-00 00:00:00'){
                                            if($datedoc2 > $dateReqDoc2){
                                                $wexit = true;
                                                $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefixSc-pastdue',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                                array_push($insertWar,$sql);
                                                $this->pastdue++;
                                                break;
                                            }
                                        }
                                    }                   
                                }                              
                            }
                            
                            //Without erros add warnings                            
                            if(!$exit && $wexit)
                                if(sizeof($insertWar) > 0) array_merge ($insertOut,$insertWar);
                        }
                    }else{ 
                        //Don't find response documents then..
                        //4: Calculate Missing Docs 
                        $now = date("Y-m-d");
                        $dateNow = new \DateTime($now);
                        $dateNow->setTime(23, 59, 59);                     
                        foreach ($nodespodtm as $key => $dtmnodepo) {
                            $exit = false;
                            if(substr_count($dtmQualifiers, $dtmnodepo[1]) > 0){
                                if(strlen($dtmnodepo[2])  == 8)
                                    $datepo_dtm = substr($dtmnodepo[2], 0,4)."-". substr($dtmnodepo[2], 4,2)."-". substr($dtmnodepo[2], 6,2);
                                elseif(strlen($dtmnodepo[2])  == 6)
                                    $datepo_dtm = substr($dtmnodepo[2], 0,2)."-". substr($dtmnodepo[2], 2,2)."-". substr($dtmnodepo[2], 4,2);
                                else
                                    $datepo_dtm = '0000-00-00';

                                $dateReqDoc2 = new \DateTime($datepo_dtm);
                                $dateReqDoc2->setTime(00, 00, 00); 
                                if($dateNow > $dateReqDoc2){
                                    if($doctype2 == 856) //ShipNot Discrepancies
                                        $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefix-shipnot',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";    
                                    else
                                        $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefixSc-missing',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                    array_push($insertOut,$sql);
                                    $this->missing++;
                                    $exit = true;
                                    break;
                                }
                            }                   
                        }
                    }
                }      
            }

            if($this->mode === 'debug'){
                $totalIns = sizeof($insertOut);
                $fsql = explode('score.log',  $this->_flogs);
                $fromDate = new DateTime($startDate);
                $fDate = $fromDate->format('Ymd');            
                $toDate = new DateTime($endDate);
                $eDate = $toDate->format('Ymd');
                $outsql = file_put_contents($fsql[0].$doctype2.'_'.$fDate.'to'.$eDate.'.sql',print_r($insertOut, true));                   
            }else{
                $totalIns = 0;
                if (sizeof($insertOut) > 0) {
                    $totalIns = $this->doInsert($insertOut);
                }             
            }           
            
            $resp = ($this->mode === 'debug' ? "MODE: " . strtoupper($this->mode) . "\r" : " ")
                    . "Generate ScoreStats executed [ status: " . $this->status 
                    . ($this->status === 'Success' ? " | rows inserts: ".$totalIns : "")
                    . " | for: " . $doctype2
                    . " | from: " . $startDate
                    . ($endDate != null ? " | to: " . $endDate : "")
                    . ($tp != null ? " | tp: " . $tp : "")
                    . ($customer != null ? " | cust: " . $customer : "")
                    . ($this->totalPo != 0 ? " | Total PO: " . $this->totalPo : "")
                    . ($this->totalAck != 0 ? " | Total ACK: " . $this->totalAck : "")
                    . ($this->totalAsn != 0 ? " | Total ASN: " . $this->totalAsn : "")
                    . ($this->totalInv != 0 ? " | Total INV: " . $this->totalInv : "")
                    . ($this->discrep != 0 ? " | Total Discrepancies: " . $this->discrep : "")
                    . ($this->slate != 0 ? " | Total Asn Shiplate: " . $this->slate : "")
                    . ($this->cancel != 0 ? " | Total Asn Ship After Cancel: " . $this->cancel : "")
                    . ($this->pastdue != 0 ? " | Total Ack Pastdue: " . $this->pastdue : "")
                    . ($this->duplicate != 0 ? " | Total Duplicate: " . $this->duplicate : "")
                    . ($this->missing != 0 ? " | Total Missing Response: " . $this->missing : "")
                    . " ]";
            error_log(date('Y-m-d H:i:s') . ": " . $resp . "\r", 3, $this->_flogs);            
            return (1);            
        }catch (Exception $e){
            $this->status = "Error";
            error_log(date('Y-m-d H:i:s') . ": Failed Execution: " . $e->getMessage() . "\r", 3, $this->_flogs);
            return (0);            
        }
    }
    
    public function snapShotHub($startDate, $endDate = null, $tp = null, $customer = null){
        error_log("Start at   >   ".date('Y-m-d H:i:s'). "\r",3, $this->_flogs);
        $sdate = new \DateTime($startDate);
        $sdate->setTime(00, 00, 00);
        $startDate = $sdate->format('Y-m-d H:i:s');

        if ($endDate == null) {            
            $date = new \DateTime($startDate);
            $date->setTime(23, 59, 59);
            $endDate = $date->format('Y-m-d H:i:s');

        }else{            
            $edate = new \DateTime($endDate);
            $edate->setTime(23, 59, 59);     
            $endDate = $edate->format('Y-m-d H:i:s');
        }

        //Obtain the date from a year ago since the current date
        $yearAgo = date('Y-m-d', strtotime('-1 year'));

        $syear = new \DateTime($yearAgo);
        $syear->setTime(00, 00, 00);
        $startYearAgo = $syear->format('Y-m-d H:i:s');

        $eyear = new \DateTime($yearAgo);
        $eyear->setTime(23, 59, 59);
        $endYearAgo = $eyear->format('Y-m-d H:i:s');
        //die("-->sd: ".$startDate." ed: ".$endDate." yearago: ".$yearAgo." startYearAgo ".$startYearAgo." endYearAgo: ".$endYearAgo);
        //Delete the statistics from a year ago
        //$this->deleteScoreStats($startYearAgo,$endYearAgo);
        
        //Add 1 day to all the stored statistics
        $resUpdateXvalueStats = $this->updateXvalueStats($tp, $customer);

        if ($resUpdateXvalueStats > 0){
            $ss = 0;
            $ss = ($this->generateScoreStats(855, $startDate,$endDate,$tp,$customer) > 0) ? $ss+1 : $ss;
            $ss = ($this->generateScoreStats(856, $startDate,$endDate,$tp,$customer) > 0) ? $ss+1 : $ss;
            $ss = ($this->generateScoreStats(810, $startDate,$endDate,$tp,$customer) > 0) ? $ss+1 : $ss;

            //error_log(date('Y-m-d H:i:s') . "ss: " . $ss . "\r", 0);

            if($ss == 3){
                $conn = $this->conndb('dr1');
                set_time_limit(0);
                $this->totalPo = 0;
                //1: Find doctypes 850 sent by customer and received by TP
                $sql = "SELECT * FROM Messages WHERE srdatetime BETWEEN '$startDate' AND '$endDate' AND doctype=850 AND folder ='sent'";
                if ($tp != null) $sql = $sql . " AND customer=$tp";
                if ($customer != null) $sql = $sql . " AND tp=$customer ";
                $sql = $sql . ";";
                $msgs = $this->doQuery($sql,$conn);
                $insertOut = array();
                foreach ($msgs as $msg) {
                    if (!empty($msg['srdatetime'])) {
                        $msgdate = new \DateTime($msg['srdatetime']);
                    }                
                    $xvalue = $this->dateToXvalue($msgdate);
                    //2: TOTALS, Stores the quantity of documents(850 by customer, tp, xvalue) used in the calculation to best performance at frontend)
                    $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'po-total',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                    array_push($insertOut,$sql);   
                    $this->totalPo++;
                }

                if($this->mode === 'debug'){
                    $totalIns = sizeof($insertOut);
                }else{
                    $totalIns = 0;
                    if (sizeof($insertOut) > 0) {
                        $totalIns = $this->doInsert($insertOut);
                    }             
                }

                $resp = ($this->mode === 'debug' ? "MODE: " . strtoupper($this->mode) . "\r" : " ")
                        . "Generate ScoreStats executed [ status: " . $this->status 
                        . ($this->status === 'Success' ? " | rows inserts: ".$totalIns : "")
                        . " | for: 850 "
                        . " | from: " . $startDate
                        . ($endDate != null ? " | to: " . $endDate : "")
                        . ($tp != null ? " | tp: " . $tp : "")
                        . ($customer != null ? " | cust: " . $customer : "")
                        . ($this->totalPo != 0 ? " | Total PO: " . $this->totalPo : "")
                        . ($this->totalAck != 0 ? " | Total ACK: " . $this->totalAck : "")
                        . ($this->totalAsn != 0 ? " | Total ASN: " . $this->totalAsn : "")
                        . ($this->totalInv != 0 ? " | Total INV: " . $this->totalInv : "")
                        . ($this->discrep != 0 ? " | Total Discrepancies: " . $this->discrep : "")
                        . ($this->slate != 0 ? " | Total Asn Shiplate: " . $this->slate : "")
                        . ($this->cancel != 0 ? " | Total Asn Ship After Cancel: " . $this->cancel : "")
                        . ($this->pastdue != 0 ? " | Total Ack Pastdue: " . $this->pastdue : "")
                        . ($this->duplicate != 0 ? " | Total Duplicate: " . $this->duplicate : "")
                        . ($this->missing != 0 ? " | Total Missing Response: " . $this->missing : "")
                        . " ]";
                error_log(date('Y-m-d H:i:s') . ": " . $resp . "\r", 3, $this->_flogs);

            }else{
                //To delete because it is not run well
                $this->deleteScoreStats($startDate,$endDate);
                error_log(date('Y-m-d H:i:s'). "Fail Process revert changes... \r",3, $this->_flogs);
            }

            error_log("End at     >   ".date('Y-m-d H:i:s'). "\r",3, $this->_flogs);
        }else{
            //Saving the error by updating xvalue fields
            error_log(date('Y-m-d H:i:s'). "Failure updates at xvalue fields: The statistics cannot be generated... \r",3, $this->_flogs);
        }
    }
    
    private function tmpMessage($startDate,$endDate=null,$tp=null,$customer=null){
        
        $btrace = debug_backtrace();
        
        if($btrace[0]["function"] == 'deleteScoreStats'){
            $caller = "Delete ";
            $fact = "deletes ";
        }else{
            $caller = "Generate ";
            $fact = "inserts ";
        }
        
        $resp = ($this->mode === 'debug' ? "MODE: " . strtoupper($this->mode) . "\r" : " ")
                . $caller ."ScoreStats executed [ status: " . $this->status 
                . ($this->status === 'Success' ? " | rows ".$fact. ": " : "")
                . " | from: " . $startDate
                . ($endDate != null ? " | to: " . $endDate : "")
                . ($tp != null ? " | tp: " . $tp : "")
                . ($customer != null ? " | cust: " . $customer : "")
                . ($this->totalPo != 0 ? " | Total PO: " . $this->totalPo : "")
                . ($this->totalAck != 0 ? " | Total ACK: " . $this->totalAck : "")
                . ($this->totalAsn != 0 ? " | Total ASN: " . $this->totalAsn : "")
                . ($this->totalInv != 0 ? " | Total INV: " . $this->totalInv : "")
                . ($this->discrep != 0 ? " | Total Discrepancies: " . $this->discrep : "")
                . ($this->slate != 0 ? " | Total Asn Shiplate: " . $this->slate : "")
                . ($this->cancel != 0 ? " | Total Asn Ship After Cancel: " . $this->cancel : "")
                . ($this->pastdue != 0 ? " | Total Ack Pastdue: " . $this->pastdue : "")
                . ($this->duplicate != 0 ? " | Total Duplicate: " . $this->duplicate : "")
                . ($this->missing != 0 ? " | Total Missing Response: " . $this->missing : "")
                . " ]";
        error_log(date('Y-m-d H:i:s') . ": " . $resp . "\r", 3, $this->_flogs); 
    }

    private function dateToXvalue($date) {    
        $today = new \DateTime();
        $today->setTime(0, 0, 0);
        $date->setTime(0, 0, 0);
        return (int) $date->diff($today)->format('%a');
    }

    private function doUpdate($sql) {
        $result = 0;
        try {
            $stmt = $this->conndb('dr4')->prepare($sql);
            if (!$stmt->execute()) {
                $this->status = "Error";
                error_log(date('Y-m-d H:i:s') . ": " . $stmt->errorInfo() . "\r", 3, $this->_flogs);
            } else {
                $result = $stmt->rowCount();
                $stmt->closeCursor();
            }
        } catch (PDOException $e) {
            $this->status = "Error";
            error_log(date('Y-m-d H:i:s') . ": scored: Failed update at PartnerStatistics: " . $e->getMessage() . "\r", 3, $this->_flogs);
            
        }
        return $result;
    }
    
    private function doQuery($sql,$conn) {   
        $results = array();
        try {
            $stmt = $conn->prepare($sql);
            if (!$stmt->execute()) {
                $this->status = "Error";
                error_log(date('Y-m-d H:i:s') . "Sql: " .$sql." ". print_r($stmt->errorInfo(),1) . "\r", 3, $this->_flogs);            
            } else {
                while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $results[] = $result;
                }
                $stmt->closeCursor();
            }
        } catch (PDOException $e) {
            $this->status = "Error";
            error_log(date('Y-m-d H:i:s') . ": Failed Execution: " . $e->getMessage() . "\r", 3, $this->_flogs);
            
        }
        return $results;
    }

    private function doDelete($sql) {
        $result = 0;
        try {
            $stmt = $this->conndb('dr4')->prepare($sql);
            if (!$stmt->execute()) {
                $this->status = "Error";
                error_log(date('Y-m-d H:i:s') . ": " . $stmt->errorInfo() . "\r", 3, $this->_flogs);
            } else {
                $result = $stmt->rowCount();
                $stmt->closeCursor();
            }
        } catch (PDOException $e) {
            $this->status = "Error";
            error_log(date('Y-m-d H:i:s') . ": scored: Failed delete at PartnerStatistics: " . $e->getMessage() . "\r", 3, $this->_flogs);
            
        }
        return $result;
    }

    private function doInsert($sql) {
        $result = 0;
        try {
            $i = 0;
            $conndb = $this->conndb('dr4');
            foreach ($sql as $insert) {
                $stmt = $conndb->prepare($insert);
                if (!$stmt->execute()) { 
                    $this->status = "Error";
                    error_log(date('Y-m-d H:i:s') . ": " . $stmt->errorInfo() . "\r", 3, $this->_flogs);
                } else {
                    $result = $stmt->rowCount();
                    $stmt->closeCursor();
                }
                $i = $i + $result;
            }
        } catch (PDOException $e) {
            error_log(date('Y-m-d H:i:s') . ": scored: Failed insert at PartnerStatistics: " . $e->getMessage() . "\r", 3, $this->_flogs);
            
        }
        return $i;
    }
}
//$dateFrom = date('Y-m-d', strtotime(' -1 day')); 
$dateFrom = date('2016-07-15');
$dateTo = date('2016-08-02');
#MODE Debug, don't insert or delete rows => new HubScoreStats('debug')
$sc = new HubScoreStats();
$sc->snapShotHub($dateFrom,$dateTo,1826); 

