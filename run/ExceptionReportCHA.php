<?php
require_once(dirname(__DIR__)."/vendor/swiftmailer/swiftmailer/lib/swift_required.php");

//This class generates an exception report for CHA with PO (850) ACK (855) discrepances. 
//It's executed daily and it sends the results by email to analysts.
class ExceptionReportCHA {
    //Log file
    var $_flogs;
    var $status;
    var $doctype;
    var $doctype2;
    var $segment1;
    var $segment2;
    var $poid;
    var $poackid;
    var $date;
    var $buyerID;
    var $vendor;
    var $poReference;
    var $poAckReference;
    var $poAckLocation;
    var $result;
    var $from;
    var $to;
    var $mailer_host;
    var $mailer_port;
    var $mailer_encription;
    var $mailer_user;
    var $mailer_password;
    var $database_host;
    var $database_port;
    var $database_name;
    var $database_user;
    var $database_password;
        
    function __construct(){
        $this->_flogs = '/var/www/hubdashboard/app/logs/exceptionReport.log';
        $this->status = "Success";
        $poid = '';
        $poackid = '';
        $date = "";
        $buyerID = "";
        $vendor = "";
        $poReference = "";
        $poAckReference = "";
        $poAckLocation = "";
        $result = "";
        
        //Mailer
        $this->mailer_host = "slan-550-78.anhosting.com";
        $this->mailer_port = "465";
        $this->mailer_encription = "ssl";
        $this->mailer_user = "test@greicodex.com";
        $this->mailer_password = "NcTEST123";
        
        //DB
        $this->database_host = "172.18.37.114";
        $this->database_port = "3306";
        $this->database_name = "webedi30";
        $this->database_user = "dtadmin";
        $this->database_password = "Ellx3~XAMRxsyUj4Y";
        
        $this->from = "notify@datatrans-inc.com";
        $this->to = ['jmartin@datatrans-inc.com', 'maristigueta@datatrans-inc.com'];
    }

    private function conndb(){
        try {
            $conn = new PDO('mysql:host='.$this->database_host.';port='.$this->database_port.';dbname='.$this->database_name, $this->database_user, $this->database_password);
            return $conn;
        } catch (PDOException $ex) {
            error_log(date('Y-m-d H:i:s') . ": " . $ex->getMessage() . "\r", 3, $this->_flogs);
        }
    }
    
    public function exceptionReport($startDate, $customer, $tp = null, $endDate = null) {
        $doctype = 850;
        $doctype2 = 855;
        $segment1 = 'PO1';
        $segment2 = 'PO1';
        $reportdata = [];
        $ddata = [];

        $status =   array(  'AA' => 'Accepted - Order Forwarded to Alternate Supplier Location',
                'AC' => 'Accepted and Shipped',
                'AR' => 'Accepted and Released for Shipment',
                'BP' => 'Accepted - Partial Shipment, Balance Backordered',
                'DR' => 'Accepted - Date Rescheduled',
                'IA' => 'Accepted',
                'IB' => 'Backordered',
                'IC' => 'Accepted - Changes Made',
                'ID' => 'Deleted',
                'IE' => 'Accepted, Price Pending',
                'IF' => 'On Hold, Incomplete Description',
                'IH' => 'On Hold',
                'IP' => 'Accepted - Price Changed',
                'IQ' => 'Accepted - Quantity Changed',
                'IR' => 'Rejected',
                'IS' => 'Accepted - Substitution Made',
                'IW' => 'On Hold - Waiver Required',
                'R1' => 'Rejected, Not a Contract Item', 
                'R2' => 'Rejected, Invalid Item Product Number',
                'R3' => 'Rejected, Invalid Unit of Issue',
                'R4' => 'Rejected, Contract Item not Available',
                'R5' => 'Rejected, Reorder Item as a Just in Time (JIT) Order',
                'R6' => 'Rejected, Reorder Item as an Extended Delivery Order (EDO)',
                'R7' => 'Rejected, Reorder Item as a Drop Shipment',
                'R8' => 'Rejected, Reorder Item as a Surge Order',
                'SP' => 'Accepted - Schedule Date Pending');        
                
        if($startDate == date('Y-m-d')) {
            $startDate = date('Y-m-d', strtotime(' -1 day'));
        }        
        error_log("Start at   >   ".date('Y-m-d H:i:s'). "\r",3, $this->_flogs);
        $sdate = new \DateTime($startDate);
        $sdate->setTime(00, 00, 00);
        $startDate = $sdate->format('Y-m-d H:i:s');
        
        if ($endDate == null) {            
            $edate = new \DateTime($startDate);
            $edate->setTime(23, 59, 59);
            $toDate = $edate->format('Y-m-d H:i:s');

        }else{            
            $edate = new \DateTime($endDate);
            $edate->setTime(23, 59, 59);     
            $toDate = $edate->format('Y-m-d H:i:s');
        }

        $conn = $this->conndb();

        $sql = "SELECT * FROM Messages m WHERE m.customer=$customer AND m.srdatetime BETWEEN '$startDate' AND '$toDate' AND m.doctype=$doctype AND m.folder IN ('inbox','sent')";
        
        if ($tp != null)
            $sql = $sql . " AND m.tp=$tp ";        
        
        $msgs = $this->doQuery($sql,$conn);
        foreach ($msgs as $msg) {
            if (!empty($msg['srdatetime'])) {
                $msgdate = new \DateTime($msg['srdatetime']);
            }
            
            $sql = "SELECT * FROM MessageReferences ref INNER JOIN Messages m ON ref.FK_message = m.PK_id "
                    . "WHERE ref.reference={$msg['reference']} AND m.doctype=$doctype2 AND m.folder IN ('inbox','sent');";
            $assocs = $this->doQuery($sql,$conn);
            
            $sql1 = "SELECT n.PK_id, e.index, e.data FROM Elements e INNER JOIN Nodes n ON n.PK_id = e.nodepointer "
                    . "WHERE e.rootnode={$msg['rootnode']} AND n.title = '$segment1' AND e.index IN (1,2,3,4,5);";
            $nodes1 = $this->doQuery($sql1,$conn);
            $values1 = array();
            foreach ($nodes1 as $node) {
                if (!isset($values1[$node['PK_id']])) {
                    $values1[$node['PK_id']] = array();
                }
                $values1[$node['PK_id']][$node['index']] = $node['data'];
            }

            foreach ($assocs as $msg2) {
                $sql2 = "SELECT n.PK_id, e.index, e.data FROM Elements e INNER JOIN Nodes n ON n.PK_id = e.nodepointer "
                        . "WHERE e.rootnode=$msg2[rootnode] AND n.title = '$segment2' AND e.index IN (1,2,3,4,5);";
                $nodes2 = $this->doQuery($sql2,$conn);
                $values2 = array();
                foreach ($nodes2 as $node) {
                    if (!isset($values2[$node['PK_id']])) {
                        $values2[$node['PK_id']] = array();
                    }
                    $values2[$node['PK_id']][$node['index']] = $node['data'];
                }

                foreach ($values1 as $val1) {
                    foreach ($values2 as $val2) {
                        if ($val1[1] == $val2[1]) {
                            if (isset($val1[2]) && isset($val2[2]) && $val1[2] != $val2[2]) {
                                $data[] = array(
                                    'id1' => $msg['PK_id'],
                                    'id2' => $msg2['PK_id'],
                                    'val1' => $val1[2],
                                    'val2' => $val2[2],
                                );
                            }
                            if (isset($val1[3]) && isset($val2[3]) && $val1[3] != $val2[3]) {
                                $data[] = array(
                                    'id1' => $msg['PK_id'],
                                    'id2' => $msg2['PK_id'],
                                    'val1' => $val1[3],
                                    'val2' => $val2[3],
                                );
                            }
                            if (isset($val1[4]) && isset($val2[4]) && $val1[4] != $val2[4]) {
                                $data[] = array(
                                    'id1' => $msg['PK_id'],
                                    'id2' => $msg2['PK_id'],
                                    'val1' => $val1[4],
                                    'val2' => $val2[4],
                                );
                            }
                            if (isset($val1[5]) && isset($val2[5]) && $val1[5] != $val2[5]) {
                                $data[] = array(
                                    'id1' => $msg['PK_id'],
                                    'id2' => $msg2['PK_id'],
                                    'val1' => $val1[5],
                                    'val2' => $val2[5],
                                );
                            }
                        }
                    }
                }
                if($doctype2 == 855) {
                    $data855 = array();
                    $keys=array();
                    foreach ($data as $d) {
                    }
                    $ddata[] = $d;
                }                
            }
        }

        foreach ($ddata as $d) {
            //Starting to retireve the details
            $poid = $d['id1'];
            $poackid = $d['id2'];

            $sql = "select * from Messages where PK_id = $poid";
            $msgpo = $this->doQuery($sql,$conn);
            foreach ($msgpo as $msgpo1) {
                    $msgpo2 = $msgpo1['PK_id'];
            }
            $sql = "SELECT n.PK_id, n.title, group_concat(e.`index`,':',e.data ORDER BY e.`index` ASC SEPARATOR ';') as elem 
                    FROM Nodes n JOIN Elements e on e.nodepointer=n.PK_id JOIN Messages m ON m.rootnode=e.rootnode 
                    WHERE m.PK_id = ".$msgpo2.
                    " GROUP BY e.nodepointer;";
            $poArr = $this->doQuery($sql,$conn);
            
            $msgPo = array();
            $vendor = "";
            $buyerID = "";
            $date = "";
            $c = 0;
            foreach ($poArr as $po) {
                if($po['title'] == 'PO1' && $po['elem'] != "") {
                    $c++;
                    $det = explode(";", $po['elem']);
                    $detail = array();
                    foreach ($det as $de) {
                        $d = explode(":", $de);
                        $detail[$d[0]] = $d[1];
                    }
                    $k = ($detail[1] != "")?$detail[1]:$c;
                    $msgPo[$k] = $detail;
                } elseif($po['title'] == 'N1' && $po['elem'] != "") {
                    $det = explode(";", $po['elem']);
                    $detail = array();
                    foreach ($det as $de) {
                        $d = explode(":", $de);
                        $detail[$d[0]] = $d[1];
                    }
                    if($detail[1] == 'VN') {
                        $vendor = $detail[2];
                    }
                } elseif($po['title'] == 'PER' && $po['elem'] != "") {
                    $det = explode(";", $po['elem']);
                    $detail = array();
                    foreach ($det as $de) {
                        $d = explode(":", $de);
                        $detail[$d[0]] = $d[1];
                    }
                    $buyerID = $detail[2];
                }  elseif($po['title'] == 'BEG' && $po['elem'] != "") {
                    $det = explode(";", $po['elem']);
                    $detail = array();
                    foreach ($det as $de) {
                        $d = explode(":", $de);
                        $detail[$d[0]] = $d[1];
                    }
                    $poReference = $detail[3];
                }
            }
            $date = $msgpo[0]['shipdate'];

            $sql = "SELECT n.PK_id, n.title, group_concat(e.`index`,':',e.data ORDER BY e.`index` ASC SEPARATOR ';') as elem 
                    FROM Nodes n join Elements e on e.nodepointer=n.PK_id JOIN Messages m ON m.rootnode=e.rootnode 
                    WHERE m.PK_id = $poackid
                    GROUP BY e.nodepointer;";            
            $poAckArr = $this->doQuery($sql,$conn);

            $msgPoAck = array();
            $poAckReference = "";
            $poAckLocation = "";     
            $c = 0;
            for ($i=0; $i<count($poAckArr);$i++) {
                $ack = $poAckArr[$i];
                $newDate = "";
                $newDateQual = "";
                if($ack['title'] == 'PO1' && $ack['elem'] != "") {
                    $c++;
                    $det = explode(";", $ack['elem']);
                    $detail = array();
                    foreach ($det as $de) {
                        $d = explode(":", $de);
                        $detail[$d[0]] = $d[1];
                    }
                    $k = ($detail[1] != "")?$detail[1]:$c;
                    $msgPoAck[$k]['PO1'] = $detail;
                    $pid = array();
                    $ack = array();
                    for($j=1;$j<20;$j++) {
                        if(!isset($poAckArr[$i+$j])) break;
                        if($poAckArr[$i+$j]['title'] == 'PO1') {
                            break;
                        }elseif($poAckArr[$i+$j]['title'] == 'PID') {
                            $det = explode(";", $poAckArr[$i+$j]['elem']);
                            $detail = array();
                            foreach ($det as $de) {
                                $d = explode(":", $de);
                                if(isset($d[0]) && isset($d[1])) {
                                    $detail[$d[0]] = $d[1];
                                }
                            }
                            $pid = $detail;
                        }elseif($poAckArr[$i+$j]['title'] == 'ACK') {
                            $det = explode(";", $poAckArr[$i+$j]['elem']);
                            $detail = array();
                            foreach ($det as $de) {
                                $d = explode(":", $de);
                                $detail[$d[0]] = $d[1];
                            }
                            $ack = $detail;
                            if(isset($detail[4]) && isset($detail[5])) {
                                $newDateQual = $detail[4];
                                $newDate = $detail[5];
                            }
                        }
                    }
                    $newDateAck= "";
                    $newDateAckQual= "";
                    if(strlen($newDate) == 8 && !empty($newDateQual)) {
                        $newDateAck = substr($newDate, 4, 2) . "-".substr($newDate, 6, 2)."-".substr($newDate, 0, 4);
                        $sql = "SELECT * FROM Codes where elementcode = '374' and code = $newDateQual";
                        $dtm04 = $this->doQuery($sql,$conn);
                        $newDateAckQual = $dtm04[0]['description'];
                    }
                    $msgPoAck[$k]['PID'] = $pid;
                    $msgPoAck[$k]['ACK'] = $ack;
                    $msgPoAck[$k]['ACK']['ACK05'] = $newDateAck;
                    $msgPoAck[$k]['ACK']['ACK04'] = $newDateAckQual;
                } elseif($ack['title'] == 'BAK' && $ack['elem'] != "") {
                    $det = explode(";", $ack['elem']);
                    $detail = array();
                    foreach ($det as $de) {
                        $d = explode(":", $de);
                        $detail[$d[0]] = $d[1];
                    }
                    if (isset($detail[8]))
                        $poAckReference = $detail[8];
                } elseif($ack['title'] == 'N1' && $ack['elem'] != "") {
                    $det = explode(";", $ack['elem']);
                    $detail = array();
                    foreach ($det as $de) {
                        $d = explode(":", $de);
                        $detail[$d[0]] = $d[1];
                    }
                    if($detail[1] == "ST") {
                        $poAckLocation = $detail[2];
                    }
                }
            }
            $result = array();
            foreach($msgPo as $line => $po1) {
                if(isset($msgPoAck[$line]['PO1'])) {
                    if($msgPoAck[$line]['PO1'][2] != $po1[2] 
                            || $msgPoAck[$line]['PO1'][3] != $po1[3] 
                            || $msgPoAck[$line]['PO1'][4] != $po1[4]) {
                        $result[$line]['PO1'] = $po1;
                        $result[$line]['PO1ACK'] = $msgPoAck[$line]['PO1'];
                        $result[$line]['ACK'] = $msgPoAck[$line]['ACK'];
                        $result[$line]['ACK']['status'] = $status[$msgPoAck[$line]['ACK'][1]];
                        $result[$line]['PID'] = $msgPoAck[$line]['PID'];
                    }
                }
            }
            $report = array(
                    "poid" => $poid,
                    "poackid" => $poackid,
                    "datemsg" => $date,
                    "buyerID" => $buyerID,
                    "vendor" => $vendor,
                    "poReference" => $poReference,
                    "poAckReference" => $poAckReference,
                    "poAckLocation" => $poAckLocation,
                    "msgPO" => $result
            );
            $reportdata[] = $report;
        }
        
        return $reportdata;               
    }
        
    private function doQuery($sql,$conn) {   
        $results = array();
        try {
            $stmt = $conn->prepare($sql);
            if (!$stmt->execute()) {
                $this->status = "Error";
                error_log(date('Y-m-d H:i:s') . "Sql: " .$sql." ". print_r($stmt->errorInfo(),1) . "\r", 3, $this->_flogs);            
            } else {
                while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $results[] = $result;
                }
                $stmt->closeCursor();
            }
        } catch (PDOException $e) {
            $this->status = "Error";
            error_log(date('Y-m-d H:i:s') . ": Failed Execution: " . $e->getMessage() . "\r", 3, $this->_flogs);
            
        }
        return $results;
    }

    public function sendMail($subject, $body, $filepath, $filename)
    {
        $transport = \Swift_SmtpTransport::newInstance($this->mailer_host,  465, 'ssl');
        $transport->setUsername($this->mailer_user);
        $transport->setPassword($this->mailer_password);
        
        $message = \Swift_Message::newInstance();
        
        $message = (new \Swift_Message($subject))
            ->setFrom($this->from)
            ->setTo($this->to)
            ->setBody($body,
                'text/html');

        $message->attach(\Swift_Attachment::fromPath($filepath)->setFilename($filename));
        
        $mailer = \Swift_Mailer::newInstance($transport);
        $mailer->send($message);
    }
}

$date = date('mdY', strtotime(' -1 day'));
$filepath = "/var/www/hubdashboard/app/logs/oldReport_".$date.".csv";
$filename = "oldReport_".$date.".csv";
$file = fopen($filepath, 'w');

// headers
$header_table = array(
'PO',
'POACK',
'Confirmation',
'Date',
'Vendor',
'BuyerID',
'ACK Location',
'Line',
'QTY_850',
'QTY_855',
'UOM_850',
'UOM_855',
'Buyer Item#',
'Vendor Part#_850',
'Vendor Part#_855',
'Item Detail',
'Line Status',
'Date-Current Scheduled Ship',
'Price',
'Price Change',
'Extended Price',
'Extended Price Change'
);

//$dateFrom = date('2017-08-06'); //Date for testing
$dateFrom = date('Y-m-d', strtotime(' -1 day')); 
$customer = "2825"; //CHA

$er = new ExceptionReportCHA();
$dataresults = $er->exceptionReport($dateFrom, $customer); 
//$tp = "453"; //Test only for Medline Industries
//$dataresults = $er->exceptionReport($dateFrom, $customer, $tp); 

fputcsv($file, $header_table);

$data = [];
$total = 0;
foreach ($dataresults as $dataresult) {
    foreach ($dataresult['msgPO'] as $po1) { 
        $data[] = $dataresult['poReference']; //PO
        $data[] = $dataresult['poAckReference']; //POACK
        $data[] = ""; //Confirmation 
        $datemsg = new \DateTime($dataresult['datemsg']);
        $data[] = ($dataresult['datemsg'] =="" ? "" : $datemsg->format('m-d-Y')); //Date
        $data[] = ($dataresult['vendor'] ? $dataresult['vendor'] : ""); //Vendor
        $data[] = ($dataresult['buyerID'] ? $dataresult['buyerID'] : ""); //BuyerID
        $data[] = $dataresult['poAckLocation']; //ACK Location        
        $data[] = $po1['PO1']['1']; //Line
        $data[] = $po1['PO1']['2']; //Qty_850
        $data[] = (isset($po1['PO1ACK']) ? $po1['PO1ACK']['2'] : ""); //Qty_855
        $data[] = $po1['PO1']['3']; //UOM_850
        $data[] = (isset($po1['PO1ACK']) ? $po1['PO1ACK']['3'] : ""); //UOM_855
        $data[] = $po1['PO1']['7']; //Buyer Item#
        $data[] = $po1['PO1']['9']; //Vendor Part#_850
        $data[] = (isset($po1['PO1ACK']) ? $po1['PO1ACK']['7'] : ""); //Vendor Part#_855
        $data[] = (isset($po1['PID']['5']) ? $po1['PID']['5'] : ""); //Item Detail
        $data[] = (isset($po1['ACK']) ? $po1['ACK']['status'] : ""); //Line Status
        $data[] = ($po1['PO1ACK']['4'] != '' && $po1['ACK']['ACK05'] != '' ? $po1['ACK']['ACK05'] : ""); //Date-Current Scheduled Ship
        $data[] = $po1['PO1']['4']; //Price
        $data[] = (isset($po1['PO1ACK']) ? $po1['PO1ACK']['4'] : ""); //Price Change
        $data[] = $po1['PO1']['4'] * $po1['PO1']['2']; //Extended Price
        $data[] = (isset($po1['PO1ACK']) ? $po1['PO1ACK']['4'] * $po1['PO1ACK']['2'] : ""); //Extended Price Change
        
        fputcsv($file, $data);
        $data = [];
        $total = $total+1;
    }
}

fclose($file);

if ($total>0)
{
    $subject = "Exception Report PO (850) ACK Discrepancies (855) for CHA - ".$date;
    $body = "
    <p>Dear Customer</p>
    <p>The Exception Report PO (850) ACK Discrepancies (855) for CHA - ".$date." was created successfuly.</p>
    <p>This e-mail is sent from a non-monitored account. Please reply to support@datatrans-inc.com if you need assistance.</p>
    <p>
    Regards,<br>
    DataTrans Support
    </p>
    ";

    $er->sendMail($subject, $body, $filepath, $filename);    
}

error_log("Results   >>>   ". "\r",3, $er->_flogs);
error_log("Date of the processed data: ".$date."\r",3, $er->_flogs);
error_log("Number of discrepancies found: ".$total."\r",3, $er->_flogs);
error_log("Finished at   >   ".date('Y-m-d H:i:s'). "\r",3, $er->_flogs);
error_log(">>>>>>>>>>>>>>>>>>>>>>   ". "\r\r",3, $er->_flogs);
