<?php

namespace Report;

require_once(dirname(__DIR__) . "/vendor/autoload.php");

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

define('ISDEBUG', false);
define('EXRES_OK', -1);
define('EXRES_ERR_CONT', 1);
define('EXRES_ERR_STOP', 2);

class DiscrepancyException {

    public $docMsgId;
    public $orderMsgId;
    public $lineNumber;
    public $elementId;
    public $originalValue;
    public $rule_id;
    public $docElementId;
    public $changedValue;

    /**
     * 
     * @param int $docId
     * @param int $orderId
     * @param \Report\LineData $orderLine
     * @param string $elementTag
     * @param int $rule_id
     * @param \Report\LineData $docLine or Boolean
     * @param string $docElementTag
     */
    public function __construct($docId, $orderId, LineData $orderLine, $elementTag, $rule_id, $docLine = false, $docElementTag = null) {
        $docTag = ($docElementTag === null ) ? $elementTag : $docElementTag;
        $this->docMsgId = $docId;
        $this->orderMsgId = $orderId;
        $this->lineNumber = $orderLine->getLineNumber();
        $this->rule_id = $rule_id;
        $this->elementId = $orderLine->getTagPointer($elementTag);
        $this->originalValue = $orderLine->{$elementTag};
        if ($docLine) {
            $this->docElementId = $docLine->getTagPointer($docTag);
            $this->changedValue = $docLine->{$docTag};
            if($this->originalValue == "" && $docElementTag != "") {
                $this->originalValue = $orderLine->{$docElementTag};
            }
        } else {
            $this->docElementId = 0;
            $this->changedValue = null;
        }
    }

}

class LineData {

    private $pointers;
    private $tags;
    private $lineNumber;
    private $nodePointer;
    private $nodeTitle;
    public $db;

    public function __construct($lineNum, $nodePointer, $nodeTitle) {
        $this->pointers = array();
        $this->tags = array();
        $this->exceptions = array();
        $this->nodePointer = $nodePointer;
        $this->lineNumber = $lineNum;
        $this->nodeTitle = $nodeTitle;
    }

    public function setInfo($pointer, $tag, $value) {
        $this->pointers[$pointer] = $value;
        $this->tags[$tag] = $pointer;
    }

    public function __get($name) {
        if (isset($this->pointers[$name])) {
            return $this->pointers[$name];
        } else if (isset($this->tags[$name]) && isset($this->pointers[$this->tags[$name]])) {
            return $this->pointers[$this->tags[$name]];
        }
        return null;
    }

    public function getTagPointer($tag) {
        if (isset($this->tags[$tag])) {
            return $this->tags[$tag];
        }
        return 0;
    }

    public function getLineNumber() {
        return $this->lineNumber;
    }

    public function getNodePointer() {
        return $this->nodePointer;
    }

    public function getNodeTitle() {
        return $this->nodeTitle;
    }
    
    public function calculateTDS() {
        $this->db = $this->conndb();
        $sql = "   SELECT  n.PK_id,n.title, GROUP_CONCAT(e.`index`,':',e.data ORDER BY e.`index` ASC) AS elem  
                FROM    Nodes n JOIN Elements e ON e.nodepointer=n.PK_id
                WHERE   e.rootnode= :rootnode AND n.title IN ('IT1') and e.`index` IN (2, 4)
                GROUP BY e.nodepointer;";
        $rows = $this->doQuery($sql, array('rootnode' => $this->nodePointer));

        $total = 0;         
        foreach ($rows as $row) {
            $linetotal = 0;
            $arrData = explode(",", $row['elem']);
            if(!empty($arrData[0]) && !empty($arrData[1])) {
                $arrData[0] = explode(":", $arrData[0]);
                $arrData[1] = explode(":", $arrData[1]);
                if(!empty($arrData[0][1]) && !empty($arrData[1][1])) {
                   $linetotal = $arrData[0][1] * $arrData[1][1];
                }
            }
            $total += $linetotal;
        }

        $sql = "   SELECT  n.PK_id,n.title, GROUP_CONCAT(e.`index`,':',e.data ORDER BY e.`index` ASC) AS elem  
                FROM    Nodes n JOIN Elements e ON e.nodepointer=n.PK_id
                WHERE   e.rootnode= :rootnode AND n.title IN ('SAC') and e.`index` IN (1, 5)
                GROUP BY e.nodepointer;";
        $rows = $this->doQuery($sql, array('rootnode' => $this->nodePointer));
        foreach ($rows as $row) {
            $arrData = explode(",", $row['elem']);
            if(!empty($arrData[0]) && !empty($arrData[1])) {
                $arrData[0] = explode(":", $arrData[0]);
                $arrData[1] = explode(":", $arrData[1]);
                if(!empty($arrData[0][1]) && !empty($arrData[1][1])) {
                    if($arrData[0][1] == "S" || $arrData[0][1] == "C") {
                        $total += $arrData[1][1]/100;
                    } elseif($arrData[0][1] == "A" || $arrData[0][1] == "P") {
                        $total -= $arrData[1][1]/100;
                    }
                }
            }
        }
        $total = round($total * 100);
        //var_dump($this->{TDS01});
        //var_dump($total);
        return $total;
    }
    
    private function conndb() {
        if ($this->db == null) {
            error_log("Connecting to DB");
            $host = "";
            $port = "";
            $database = "";
            $user = "";
            $password = "";

            try {
                $config = Yaml::parse(file_get_contents(__DIR__ . '/../app/config/parameters.yml'));
            } catch (ParseException $e) {
                printf("Unable to parse the YAML string: %s", $e->getMessage());
            }
            
            $host = (isset($config['parameters']['database_host']) && !empty($config['parameters']['database_host'])) ? $config['parameters']['database_host'] : "prod-webediaurora.datatrans.internal";
            $user = (isset($config['parameters']['database_user']) && !empty($config['parameters']['database_user'])) ? $config['parameters']['database_user'] : "dtadmin";
            $password = (isset($config['parameters']['database_password']) && !empty($config['parameters']['database_password'])) ? $config['parameters']['database_password'] : "Ellx3~XAMRxsyUj4Y";
            $port = (isset($config['parameters']['database_port']) && !empty($config['parameters']['database_port'])) ? $config['parameters']['database_port'] : "3306";
            $database = (isset($config['parameters']['database_name']) && !empty($config['parameters']['database_name'])) ? $config['parameters']['database_name'] : "webedi30";
            try {
                $this->db = new \PDO('mysql:host=' . $host . ';port=' . $port . ';dbname=' . $database, $user, $password);
                $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            } catch (\PDOException $ex) {
                error_log(date('Y-m-d H:i:s') . ": " . $ex->getMessage() . "\r", 3, $this->_flogs);
                printf("Unable to connect to Database");
                throw $ex;
            }
        }
        return $this->db;
    }
    
    private function doQuery($sql, array $params) {
        $results = false;
        try {
            $stmt =  $this->db->prepare($sql);
            if (!$stmt->execute($params)) {
                error_log(date('Y-m-d H:i:s') . "Sql: " . $sql . " " . print_r($stmt->errorInfo(), 1) . "\r", 3, $this->_flogs);
            }
            if ($stmt->columnCount()>0) {
                $results = array();
                while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                    $results[] = $row;
                }
                $stmt->closeCursor();
            }
        } catch (PDOException $e) {
            $this->status = "Error";
            error_log(date('Y-m-d H:i:s') . ": Failed Execution: " . $e->getMessage() . "\r", 3, $this->_flogs);
        }
        return $results;
    }
}
//This class generates an exception report for CHA with PO (850) ACK (855) discrepances. 
//It's executed daily and it sends the results by email to analysts.
class Discrepancies {

    //Log file
    var $_flogs;
    var $status;
    var $doctype;
    var $doctype2;
    var $segment1;
    var $segment2;
    var $poid;
    var $poackid;
    var $date;
    var $buyerID;
    var $vendor;
    var $poReference;
    var $poAckReference;
    var $poAckLocation;
    var $result;
    var $from;
    var $to;
    var $mailer_host;
    var $mailer_port;
    var $mailer_encription;
    var $mailer_user;
    var $mailer_password;
    var $database_host;
    var $database_port;
    var $database_name;
    var $database_user;
    var $database_password;
    var $preparedStatements;
    var $db;
    var $is_demo_customer;
    var $ack01Status = array('AA' => 'Accepted - Order Forwarded to Alternate Supplier Location',
        'AC' => 'Accepted and Shipped',
        'AR' => 'Accepted and Released for Shipment',
        'BP' => 'Accepted - Partial Shipment, Balance Backordered',
        'DR' => 'Accepted - Date Rescheduled',
        'IA' => 'Accepted',
        'IB' => 'Backordered',
        'IC' => 'Accepted - Changes Made',
        'ID' => 'Deleted',
        'IE' => 'Accepted, Price Pending',
        'IF' => 'On Hold, Incomplete Description',
        'IH' => 'On Hold',
        'IP' => 'Accepted - Price Changed',
        'IQ' => 'Accepted - Quantity Changed',
        'IR' => 'Rejected',
        'IS' => 'Accepted - Substitution Made',
        'IW' => 'On Hold - Waiver Required',
        'R1' => 'Rejected, Not a Contract Item',
        'R2' => 'Rejected, Invalid Item Product Number',
        'R3' => 'Rejected, Invalid Unit of Issue',
        'R4' => 'Rejected, Contract Item not Available',
        'R5' => 'Rejected, Reorder Item as a Just in Time (JIT) Order',
        'R6' => 'Rejected, Reorder Item as an Extended Delivery Order (EDO)',
        'R7' => 'Rejected, Reorder Item as a Drop Shipment',
        'R8' => 'Rejected, Reorder Item as a Surge Order',
        'SP' => 'Accepted - Schedule Date Pending');

    function __construct() {
        $this->_flogs = '/tmp/exceptions.log'; //'/var/www/hubdashboard/app/logs/exceptionReport.log';
        $this->status = "Success";
        $poid = '';
        $poackid = '';
        $date = "";
        $buyerID = "";
        $vendor = "";
        $poReference = "";
        $poAckReference = "";
        $poAckLocation = "";
        $result = "";
        $this->preparedStatements = array();

        try {
            $config = Yaml::parse(file_get_contents(__DIR__ . '/../app/config/parameters.yml'));
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
        }
        foreach ($config as $key => $section) {
            echo "[$key]:\n";
            foreach ($section as $pkey => $param) {
                echo "\t" . ucwords(str_replace('_', ' ', $pkey)) . ":$param\n";
            }
        }
        //Mailer
        $this->mailer_host = (isset($config['parameters']['mailer_host']) && !empty($config['parameters']['mailer_host'])) ? $config['parameters']['mailer_host'] : "slan-550-78.anhosting.com";
        $this->mailer_port = (isset($config['parameters']['mailer_port']) && !empty($config['parameters']['mailer_port'])) ? $config['parameters']['mailer_port'] : "465";
        $this->mailer_encription = (isset($config['parameters']['mailer_encription']) && !empty($config['parameters']['mailer_encription'])) ? $config['parameters']['mailer_encription'] : "ssl";
        $this->mailer_user = (isset($config['parameters']['mailer_user']) && !empty($config['parameters']['mailer_user'])) ? $config['parameters']['mailer_user'] : "test@greicodex.com";
        $this->mailer_password = (isset($config['parameters']['mailer_password']) && !empty($config['parameters']['mailer_password'])) ? $config['parameters']['mailer_password'] : "NcTEST123";

        //DB

        $this->database_host = (isset($config['parameters']['database_host']) && !empty($config['parameters']['database_host'])) ? $config['parameters']['database_host'] : "prod-webediaurora.datatrans.internal";
        $this->database_user = (isset($config['parameters']['database_user']) && !empty($config['parameters']['database_user'])) ? $config['parameters']['database_user'] : "dtadmin";
        $this->database_password = (isset($config['parameters']['database_password']) && !empty($config['parameters']['database_password'])) ? $config['parameters']['database_password'] : "Ellx3~XAMRxsyUj4Y";

        $this->database_port = (isset($config['parameters']['database_port']) && !empty($config['parameters']['database_port'])) ? $config['parameters']['database_port'] : "3306";
        $this->database_name = (isset($config['parameters']['database_name']) && !empty($config['parameters']['database_name'])) ? $config['parameters']['database_name'] : "webedi30";



        $this->from = "notify@datatrans-inc.com";
        $this->to = array('jmartin@datatrans-inc.com', 'maristigueta@datatrans-inc.com');
    }

    /**
     * 
     * @return \PDO
     */
    private function conndb() {
        if ($this->db == null) {
            error_log("Connecting to DB");
            try {
                $this->db = new \PDO('mysql:host=' . $this->database_host . ';port=' . $this->database_port . ';dbname=' . $this->database_name, $this->database_user, $this->database_password);
                $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            } catch (\PDOException $ex) {
                print(date('Y-m-d H:i:s') . ": " . $ex->getMessage() . "\r" );
                throw $ex;
            }
        }
        return $this->db;
    }

    public function findExceptionsInDocument($sent_doctype, $msgid) {
        
        $this->db = $this->conndb();
        //Check if we have already processed this message
        if ($this->dbFindOne('SELECT * FROM discrepancy_report WHERE doc_msgid=:id', $msgid)) {
            error_log("Found report for $msgid, skipping");
            return false;
        }
        error_log("Loading $msgid");
        $msgDoc = $this->loadMessageMeta($msgid);

        // Find the original Order sent by the Hub
        $msgPO = $this->findSentDocument($sent_doctype, $msgid);

        if (false == $msgPO) {
            error_log("PO not found for $msgid");
            return false;
        }

        $docHeaderSegment = $this->getHeaderSegment($msgDoc);
        $poHeaderSegment = $this->getHeaderSegment($msgPO);

        $docLineSegment = $this->getLineSegment($msgDoc);
        $poLineSegment = $this->getLineSegment($msgPO);



        $line_results = array();

        $poHeader = $this->getDocHeader($msgPO['PK_id'], $poHeaderSegment);
        $docHeader = $this->getDocHeader($msgDoc['PK_id'], $docHeaderSegment);

        $exceptionRules = $this->getRules($msgDoc['customer'], $msgDoc['tp'], $msgDoc['doctype'], $docHeaderSegment);
        var_dump("Found ".count($exceptionRules). " rules");
        foreach ($exceptionRules as $rule) {
            $rule_result = $this->checkRule($rule, $docHeader, $poHeader, $msgPO, $msgDoc);
            if ($rule_result != EXRES_OK) {
                $ex = new DiscrepancyException($msgid, $msgPO['PK_id'], $poHeader, $rule['tgt'], $rule['id'], $docHeader, $rule['src']);

                $line_results[] = $ex;
            }
            if ($rule_result == EXRES_ERR_STOP) {
                // On error we stop evaluating rules.
                break;
            }
        }

        $poLines = $this->getDocLines($msgPO['PK_id'], $poLineSegment);
        //echo var_dump($poLines) . "\n";
        $docLines = $this->getDocLines($msgDoc['PK_id'], $docLineSegment);
        //echo var_dump($docLines) . "\n";
        $exceptionRules = $this->getRules($msgDoc['customer'], $msgDoc['tp'], $msgDoc['doctype'], $docLineSegment);
        // Compare Lines
        foreach ($poLines as $lineNum => $pLine) {
            error_log("Searching for $lineNum in Doc");
            $dLine = $this->findLine($lineNum, $pLine, $docLines,$poLineSegment[0],$docLineSegment[0]);
//            if(false == $dLine) {
//                // Missing, Line not found
//                continue;
//            }
            
            foreach ($exceptionRules as $rule) {
                $rule_result = $this->checkRule($rule, $dLine, $pLine, $msgPO, $msgDoc);
                if ($rule_result != EXRES_OK) {
                    $ex = new DiscrepancyException($msgid, $msgPO['PK_id'], $pLine, $rule['tgt'], $rule['id'], $dLine, $rule['src']);

                    $line_results[] = $ex;
                }
                if ($rule_result == EXRES_ERR_STOP) {
                    // On error we stop evaluating rules.
                    break;
                }
            }
        }

        return $line_results;
    }

    public function persist(array $resultSet) {

        $headers = array('doc_msgid', 'order_msgid', 'line_number', 'element_id', 'original_value', 'rule_id', 'doc_element_id', 'changed_value');
        $insertSQL = 'INSERT INTO discrepancy_report(doc_msgid,order_msgid,line_number,element_id,original_value,rule_id,doc_element_id,changed_value) '
                . 'VALUES (:doc_msgid,:order_msgid,:line_number,:element_id,:original_value,:rule_id,:doc_element_id,:changed_value) '
                . 'ON DUPLICATE KEY UPDATE element_id=:element_id, original_value=:original_value, rule_id=:rule_id, doc_element_id=:doc_element_id, changed_value=:changed_value;';
        foreach ($resultSet as $object) {
            $data = array_combine($headers, get_object_vars($object));
            //var_dump($data);
            $this->doQuery($insertSQL, $data);
        }
    }

    public function reportLine($resultSet) {
        var_dump($reportLine);die();
        $headers = array('Order Reference', 'Doc Reference', 'Confirmation', 'Date', 'Vendor', 'BuyerID', 'ACK Location', 'Line', 'Orig QTY', 'Doc QTY', 'Orig UOM',
            'Doc UOM', 'Buyer Item#', 'Orig Vendor Part#', 'Doc Vendor Part#', 'Item Detail', 'Line Status', 'Date-Current Scheduled Ship', 'Price',
            'Price Change', 'Extended Price', 'Extended Price Change');
        $data = @array_combine($headers, array());
        $data['Order Reference'] = $msgPO['reference']; //PO
        $data['Doc Reference'] = $msgDoc['reference']; //POACK
        $data['Confirmation'] = ""; //Confirmation 
        $data['Date'] = date(time());
        $data['Vendor'] = $msgDoc['tp']; //Vendor
        $data['BuyerID'] = $msgPO['buyername']; //BuyerID
        //$data['ACK Location'] = $dataresult['poAckLocation']; //ACK Location        
        $data['Line'] = $pLine->{'PO101'}; //Line
        $data['Orig QTY'] = $pLine->{'PO102'}; //Qty_850
        $data['Doc QTY_855'] = $dLine->{'PO102'}; //Qty_855
        $data['Orig UOM'] = $pLine->{'PO103'}; //UOM_850
        $data['Doc UOM'] = $dLine->{'PO103'}; //UOM_855
        $data['Buyer Item#'] = $pLine->{'PO107'}; //Buyer Item#
        $data['Orig Vendor Part#'] = $pLine->{'PO109'}; //Vendor Part#_850
        $data['Doc Vendor Part#'] = $dLine->{'PO109'}; //Vendor Part#_855
        $data['Item Detail'] = $pLine->{'PID05'}; //Item Detail
        $data['Line Status'] = $this->ack01Status[$dLine->{'ACK01'}]; //Line Status
        $data['Date-Current Scheduled Ship'] = $dLine->{'ACK05'}; //Date-Current Scheduled Ship
        $data['Price'] = $pLine->{'PO104'}; //Price
        $data['Price Change'] = $dLine->{'PO104'}; //Price Change
        $data['Extended Price'] = $pLine->{'PO104'} * $pLine->{'PO102'}; //Extended Price
        $data['Extended Price Change'] = $dLine->{'PO104'} * $dLine->{'PO102'}; //Extended Price Change
    }

    public function checkRule($rule, $dLine, $pLine) {
        print("Checking rule {$rule['stat_code']} '{$rule['src']}' {$rule['operator']} '{$rule['tgt']}'\n");
        $exception = false;
        switch ($rule['operator']) {
            case '==':
            case '!=':
                $exception = $dLine->{$rule['src']} == $pLine->{$rule['tgt']};
                break;
            case 'in':
            case 'not in':
                $list = explode(',', $rule['tgt']);
                $exception = in_array($dLine->{$rule['src']}, $list);
                break;
            case '>':
                if (null === $pLine->{$rule['tgt']}) {
                    return EXRES_OK;
                }
                $exception = $dLine->{$rule['src']} > $pLine->{$rule['tgt']};
                break;
            case '<':
                if (null === $pLine->{$rule['tgt']}) {
                    return EXRES_OK;
                }
                $exception = $dLine->{$rule['src']} < $pLine->{$rule['tgt']};
                break;
            case '>=':
                if (null === $pLine->{$rule['tgt']}) {
                    return EXRES_OK;
                }
                $exception = $dLine->{$rule['src']} >= $pLine->{$rule['tgt']};
                break;
            case '<=':
                if (null === $pLine->{$rule['tgt']}) {
                    return EXRES_OK;
                }
                $exception = $dLine->{$rule['src']} <= $pLine->{$rule['tgt']};
                break;
            case 'duplicate':
                //$exception = $this->findDuplicate($dLine, $rule['src']);
                break;
            case '1epsilon': 
            case '2epsilon':
            case '5epsilon':
            case '10epsilon':
                // Evaluate the percentual difference
                if(floatval($dLine->{$rule['src']}) != 0) { // avoid div-0
                    $epsilon = floatval($rule['operator']) / 100.0; // calculate error
                    $error=abs(floatval($dLine->{$rule['src']}) - floatval($pLine->{$rule['tgt']}))/floatval($dLine->{$rule['src']});
                    $exception = $epsilon < $error; 
                }
                break;
            case 'function':
                $lambda_id=md5($rule['tgt']);
                if(!isset($this->lambdas[$lambda_id])) {
                    $fnc = create_function('$rule,$dLine,$pLine', $rule['tgt']);
                    if (false === $fnc) {
                        throw new \Exception('Unable to parse code:' . $rule['tgt']);
                    }
                    $this->lambdas[$lambda_id]=$fnc;
                }
                return call_user_func_array($this->lambdas[$lambda_id], array($rule, $dLine, $pLine));
                
                break;
        }
        if (strpos($rule['operator'], 'not ') !== false || strpos($rule['operator'], '!') !== false) {
            $exception = !$exception;
        }

        if ($exception) {
            return EXRES_ERR_CONT;
        }
        return EXRES_OK;
    }

    public function findDuplicate(LineData $line, $tag) {
        $ptr = $line->getTagPointer($tag);
        $rows = $this->doQuery('SELECT count(*) FROM Elements e JOIN Messages m1 ON e.rootnode=m1.rootnode '
                . 'LEFT JOIN Messages m2 ON m1.customer=m2.customer AND m1.tp=m2.tp AND m1.doctype=m2.doctype '
                . 'JOIN Elements e2 ON e2.rootnode=m2.rootnode '
                . 'WHERE e.PK_id=:ptr AND e.`index`=e2.`index` AND e2.data=:data', array('ptr' => $ptr, 'data' => $line->{$tag}));

        return ($rows[0] > 1) ? EXRES_ERR_CONT : EXRES_OK;
    }
    
    public function checkMissingOrder(LineData $line,$tag) {
        $rows = $this->doQuery('SELECT count(*) FROM Elements e JOIN Messages m1 ON e.rootnode=m1.rootnode '
                . 'LEFT JOIN Messages m2 ON m1.customer=m2.customer AND m1.tp=m2.tp AND m2.doctype=850 '
                . 'JOIN Elements e2 ON e2.rootnode=m2.rootnode '
                . 'WHERE e2.index=3 AND e2.data=:data', array('data' => $line->{$tag}));
        return ($rows[0] < 1) ? EXRES_ERR_CONT : EXRES_OK;
    }

    /**
     * 
     * @param integer $lineNum Index of the line from the Purchase order
     * @param \LineData $needleLine Document Line we are processing
     * @param array<\LineData> $haystackLines array of LineData objects found in the Purchase Order
     * @param string $docTitle  Line segment title name on the document being processed. i.e. "IT1"
     * @param string $orderTitle  Line Segment title name on the PO i.e. "PO1"
     * @return boolean
     */
    public function findLine($lineNum, $needleLine , $haystackLines, $needleTitle, $haystackTitle) {
        error_log("Searching for $needleTitle in $haystackTitle that matches lineNum: $lineNum");
        if (isset($haystackLines[$lineNum])) {
            return $haystackLines[$lineNum];
        }
        error_log("Searching for $needleTitle in $haystackTitle that matches IN,VC,UP,EA ");
        foreach (array('IN', 'VC', 'UP', 'EA') as $qual) {
            foreach ($haystackLines as $pLine) {
                if ($pLine->{$haystackTitle.$qual} == $needleLine->{$needleTitle.$qual}) {
                    error_log("Found");
                    return $pLine;
                }
            }
        }
        error_log("Searching for {$needleTitle}07 in $haystackTitle that matches {$haystackTitle}09");
        foreach ($haystackLines as $pLine) { // Force crosscheck between 
            if ($pLine->{$haystackTitle.'09'} == $needleLine->{$needleTitle.'07'}) {
                error_log("Found");
                return $pLine;
            }
        }
        error_log("Searching for {$needleTitle}07 in $haystackTitle that matches {$haystackTitle}09");
        foreach ($haystackLines as $pLine) { // Force check on PO107
            if ($pLine->{$haystackTitle.'07'} == $needleLine->{$needleTitle.'07'}) {
                error_log("Found");
                return $pLine;
            }
        }
        return false;
    }

    private function getRules($customer, $partner, $doctype, $segments = null) {
        if (is_array($segments) && !empty($segments)) {
            return $this->doQuery('SELECT * FROM discrepancy_exception_rules WHERE doc_type=:doctype AND (src REGEXP :segments)>0 AND (customer=:customer OR customer=0) AND (tp=:partner OR tp=0)  order by priority', array('doctype' => $doctype, 'customer' => $customer, 'partner' => $partner, 'segments' => "^(" . join("|", $segments) . ")"));
        }
        return $this->doQuery('SELECT * FROM discrepancy_exception_rules WHERE doc_type=:doctype AND (customer=:customer OR customer=0) AND (tp=:partner OR tp=0)  order by priority', array('doctype' => $doctype, 'customer' => $customer, 'partner' => $partner));
    }

    private function getDocHeader($msgid, $headerSegmentTitles) {
        $sql = implode("','", $headerSegmentTitles);
        $rows = $this->doQuery("SELECT n.PK_id,n.title,e.PK_id as eleid, e.`index`,e.isqual, e.`data` FROM Nodes n JOIN Elements e ON e.nodepointer = n.PK_id JOIN Messages m ON m.rootnode=e.rootnode "
                . "WHERE m.PK_id = :id AND n.title IN ('$sql') ORDER BY n.PK_id , e.`index`; ", array('id' => $msgid));
        $segment = null;
        $lastQualifier = null;
        foreach ($rows as $row) {
            if ($segment == null && ($row['title'] == $headerSegmentTitles[0]) && $row['index'] == 1) {
                $segment = new LineData(0, $row['PK_id'], $row['title']);
            }
            if ($row['isqual'] == 1) { // If its a qualifier, we store the qualifier data temporarily and skip to the next element
                $lastQualifier = $row['data'];
                continue;
            }
            if ($lastQualifier !== null) { // On the next element we set the qualifier if its defined and clear the temporary value
                $row['qual'] = $lastQualifier;
                $lastQualifier = null;
            }
            if (!empty($row['qual'])) { // Qualifiers have a different naming format
                $segment->setInfo($row['eleid'], sprintf('%s%s', $row['title'], $row['qual']), $row['data']);
            }
            $segment->setInfo($row['eleid'], sprintf('%s%02d', $row['title'], $row['index']), $row['data']);
        }
        return $segment;
    }

    private function getDocLines($msgid, $detailSegmentTitles) {
        $sql = implode("','", $detailSegmentTitles);
        $rows = $this->doQuery("SELECT n.PK_id,n.title,e.PK_id as eleid, e.`index`,e.isqual, e.`data` FROM Nodes n JOIN Elements e ON e.nodepointer = n.PK_id JOIN Messages m ON m.rootnode=e.rootnode "
                . "WHERE m.PK_id = :id AND n.title IN ('$sql') ORDER BY n.PK_id , e.`index`; ", array('id' => $msgid));
        $lastnode = 0;
        $result = array();
        $lineOrdinal = 1;
        $segment = null;
        $lastQualifier = null;
        foreach ($rows as $row) {
            if (ISDEBUG) {
                var_dump($row);
                var_dump($segment);
                echo "************\n\n";
            }
            if (($lastnode != $row['PK_id']) && ($row['title'] == $detailSegmentTitles[0]) && $row['index'] == 1) {
                //append
                $lineNum = ($row['index'] == 1 && !empty($row['data'])) ? $row['data'] : $lineOrdinal++;
                $segment = new LineData($lineNum, $row['PK_id'], $row['title']);
                $result[$lineNum] = $segment;
            }
            if ($row['isqual'] == 1) { // If its a qualifier, we store the qualifier data temporarily and skip to the next element
                $lastQualifier = $row['data'];
                continue;
            }
            if ($lastQualifier !== null) { // On the next element we set the qualifier if its defined and clear the temporary value
                $row['qual'] = $lastQualifier;
                $lastQualifier = null;
            }

            if ($segment === null) {
                continue;
            }

            if (!empty($row['qual'])) { // Qualifiers have a different naming format
                $segment->setInfo($row['eleid'], sprintf('%s%s', $row['title'], $row['qual']), $row['data']);
            }
            $segment->setInfo($row['eleid'], sprintf('%s%02d', $row['title'], $row['index']), $row['data']);

            if ($row['title'] == $detailSegmentTitles[0]) {
                $lastnode = $row['PK_id'];
            }
        }
        if (ISDEBUG) {
            var_dump($result);
        }
        return $result;
    }

    private function getHeaderSegment(array $msgMeta) {
        //TODO: Add this to Database And Type to Hub Profile and Rules
        $headerSegmentTitles = array();
        switch ($msgMeta['doctype']) {
            case '810':
                $headerSegmentTitles = array('BIG', 'DTM', 'PER', 'N1', 'TDS');
                break;
            case '856':
                $headerSegmentTitles = array('BSN', 'DTM', 'N1', 'PER');
                break;
            case '850':
                $headerSegmentTitles = array('BEG', 'DTM', 'N1', 'PER');
                break;
            case '855':
                $headerSegmentTitles = array('BAK', 'DTM');
                break;
            case '860':
                $headerSegmentTitles = array('BCH', 'PO6');
                break;
            case '830':
                $headerSegmentTitles = array('BFR');
                break;
        }
        return $headerSegmentTitles;
    }

    private function getLineSegment(array $msgMeta) {
        //TODO: Add this to Database And Type to Hub Profile and Rules
        $detailSegmentTitles = array();
        switch ($msgMeta['doctype']) {
            case '810':
                $detailSegmentTitles = array('IT1');
                break;
            case '856':
                $detailSegmentTitles = array('LIN','SN1','PID','DTM');
                break;
            case '850':
                $detailSegmentTitles = array('PO1', 'PID', 'DTM');
                break;
            case '855':
                $detailSegmentTitles = array('PO1', 'ACK');
                break;
            case '860':
                $detailSegmentTitles = array('PO1', 'PO6');
                break;
            case '830':
                $detailSegmentTitles = array('PO1');
                break;
        }
        return $detailSegmentTitles;
    }

    /**
     * 
     * @param string $sqlString
     * @return \PDOStatement
     */
    private function dbPrepare($sqlString) {
        if (!isset($this->preparedStatements[md5($sqlString)])) {
            $this->preparedStatements[md5($sqlString)] = $this->db->prepare($sqlString);
        }
        return $this->preparedStatements[md5($sqlString)];
    }

    /**
     * 
     * @return mixed FALSE | Array
     */
    private function dbFindOne($sqlString, $id) {
        $found = $this->doQuery($sqlString, array('id' => $id));
        if (isset($found[0])) {
            return $found[0];
        }
        return false;
    }

    /**
     * 
     * @return mixed FALSE | Array
     */
    private function loadMessageMeta($id) {
        return $this->dbFindOne('SELECT * FROM Messages WHERE PK_id=:id', $id);
    }

    /**
     * 
     * @return mixed FALSE | Array
     */
    private function findSentDocument($sent_doctype, $id) {
        $sqlString = 'SELECT m.* FROM Messages m JOIN Associations a ON m.PK_id=a.associate '
                . 'WHERE m.doctype =:sent_doctype AND a.msgid=:id ORDER BY m.srdatetime DESC';
        
        $found = $this->doQuery($sqlString, array('sent_doctype' => $sent_doctype, 'id' => $id));
        if (isset($found[0])) {
            return $found[0];
        }
        return false;
    }

    public function exceptionReport($sent_doctype = 850, $recv_doctype = 855, $startDate, $customer, $tp = null, $endDate = null) {
        
        $reportdata = array();
        $ddata = array();

        if ($startDate == date('Y-m-d')) {
            $startDate = date('Y-m-d', strtotime(' -1 day'));
        }
        print("Start at   >   " . date('Y-m-d H:i:s') . "\r");
        $sdate = new \DateTime($startDate);
        $sdate->setTime(00, 00, 00);
        $startDate = $sdate->format('Y-m-d H:i:s');

        if ($endDate == null) {
            $edate = new \DateTime($startDate);
            $edate->setTime(23, 59, 59);
            $toDate = $edate->format('Y-m-d H:i:s');
        } else {
            $edate = new \DateTime($endDate);
            $edate->setTime(23, 59, 59);
            $toDate = $edate->format('Y-m-d H:i:s');
        }

        $this->db = $this->conndb();
        
        $customer_data=$this->doQuery("SELECT webtpid,name,demo,industry,softwareversion FROM Partners WHERE PK_id=:customer_id;",array('customer_id'=>$customer));
        $customer_data=$customer_data[0];
        $this->is_demo_customer=($customer_data['demo'] == true);
        print("Processing $recv_doctype Exception Analysis on Messages for Customer: {$customer_data['name']} ($customer/{$customer_data['webtpid']}) ". (($this->is_demo_customer) ? "[DEMO]":"") );
        
        
        if($this->is_demo_customer && $this->database_name == 'webedi30_demo') {
            // Do steps for Demo customers
            // Move all data to the last 30 days
            // Hardcoding Demo Customer IDs, I don't want accidents
            // TODO: Replace this code for Traffic Generator
            $this->doQuery('UPDATE Messages SET srdatetime = (NOW() - INTERVAL FLOOR(RAND() * 30) DAY) WHERE customer=:custid;',array('custid'=>2919));
            $this->doQuery('UPDATE Messages SET docdatetime=srdatetime WHERE customer=:custid;',array('custid'=>2919));
        }
        
        $sql = "SELECT * FROM Messages m WHERE m.customer=:customer AND m.srdatetime BETWEEN :startDate AND :toDate AND m.doctype=:doctype AND m.folder IN ('inbox','sent')";
        $params = array('customer' => $customer, 'startDate' => $startDate, 'toDate' => $toDate, 'doctype' => $recv_doctype);
        if ($tp != null) {
            $sql = $sql . " AND m.tp=:tp ";
            $params['tp'] = $tp;
        }
        $msgs = $this->doQuery($sql, $params);
        print('Found ' . count($msgs) . ' messages');
        $counter = 0;
        $start_time = microtime(true);
        foreach ($msgs as $counter=>$msg) {
            $resultSet = $this->findExceptionsInDocument($sent_doctype, $msg['PK_id']);
            if ($resultSet) {
                try {
                    $this->persist($resultSet);
                } catch (\Exception $e) {
                    print($e->getMessage() . $e->getTraceAsString());
                    print("DATA: $resultSet");
                    die();
                }
            }
            //$this->reportLine($resultSet);
            if ((microtime(true)-$start_time) > 3) { // People get annoyed after 3 seconds of inactivity on the screen
                $start_time= microtime(true);
                print( ceil($counter/count($msgs) * 100) . "% done");
            }
        }
        
        $this->recalculateStats($customer);
        return true;
    }
    
    private function doQuery($sql, array $params) {
        $results = false;
        try {

            $stmt = $this->dbPrepare($sql);
            if (!$stmt->execute($params)) {
                $this->status = "Error";
                print(date('Y-m-d H:i:s') . "Sql: " . $sql . " " . print_r($stmt->errorInfo(), 1) . "\r");
            } else {
                $results = true;
            }
            if ($stmt->columnCount()>0) {
                $results = array();
                while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                    $results[] = $row;
                }
                $stmt->closeCursor();
            }
        } catch (PDOException $e) {
            $this->status = "Error";
            print(date('Y-m-d H:i:s') . ": Failed Execution: " . $e->getMessage() . "\r");
        }
        return $results;
    }

    public function sendMail($subject, $body, $filepath, $filename) {
        $transport = \Swift_SmtpTransport::newInstance($this->mailer_host, 465, 'ssl');
        $transport->setUsername($this->mailer_user);
        $transport->setPassword($this->mailer_password);

        $message = \Swift_Message::newInstance();

        $message = new \Swift_Message($subject);
        $message->setFrom($this->from);
        $message->setTo($this->to);
        $message->setBody($body, 'text/html');

        $message->attach(\Swift_Attachment::fromPath($filepath)->setFilename($filename));

        $mailer = \Swift_Mailer::newInstance($transport);
        $mailer->send($message);
    }

    public function recalculateStats($customer) {
        $date = new \DateTime();
        $date->sub(new \DateInterval('P90D'));
        $back_date = $date->format('Y-m-d');
        $this->doQuery("DELETE FROM PartnerStatistics WHERE userid=:customer_id AND xvalue >= DATE_FORMAT(:back_date,'%Y%m%d') AND `type` LIKE 'po-ack%';", array('back_date' => $back_date, 'customer_id' => $customer));
        $this->doQuery("DELETE FROM PartnerStatistics WHERE userid=:customer_id AND xvalue >= DATE_FORMAT(:back_date,'%Y%m%d') AND `type` LIKE 'po-inv%';", array('back_date' => $back_date, 'customer_id' => $customer));
        $this->doQuery("DELETE FROM PartnerStatistics WHERE userid=:customer_id AND xvalue >= DATE_FORMAT(:back_date,'%Y%m%d') AND `type` LIKE 'po-asn%';", array('back_date' => $back_date, 'customer_id' => $customer));
        $this->doQuery("CREATE TEMPORARY TABLE tmp1 AS SELECT * FROM PartnerStatistics WHERE 0=1;",array());


        /** ACK * */
        /** Adding each discrepancy type **/
        $this->doQuery("INSERT INTO tmp1 SELECT 
    m2.customer AS userid,
    ru.stat_code AS `type`,
    m2.tp AS series,
    DATE_FORMAT(m2.srdatetime,'%Y%m%d') AS xvalue,
    COUNT(distinct m2.PK_id) AS data
FROM
    discrepancy_report dr
        JOIN
    Messages m1 ON m1.PK_id = dr.order_msgid
        JOIN
    Messages m2 ON m2.PK_id = dr.doc_msgid
        JOIN
    discrepancy_exception_rules ru ON ru.id = dr.rule_id
WHERE
	m2.customer=:customer_id AND m2.doctype = 855 AND (ru.stat_code LIKE '%discr%' or ru.stat_code LIKE '%-pastdue%' or ru.stat_code LIKE'-cancel%' or ru.stat_code LIKE '%-duplicate') AND m2.srdatetime >= :back_date
GROUP BY ru.stat_code , m2.tp , DATE(m2.srdatetime) ", array('back_date' => $back_date, 'customer_id' => $customer));

        /** Adding Total Discrepancies **/
        $this->doQuery("INSERT INTO tmp1 SELECT  
    m2.customer AS userid,
	'po-ack-sdiscr' AS `type`,
    m2.tp AS series,
    DATE_FORMAT(m2.srdatetime,'%Y%m%d') AS xvalue,
    COUNT(distinct m2.PK_id) AS data
FROM
    discrepancy_report dr
        JOIN
    Messages m1 ON m1.PK_id = dr.order_msgid
        JOIN
    Messages m2 ON m2.PK_id = dr.doc_msgid
        JOIN
    discrepancy_exception_rules ru ON ru.id = dr.rule_id
WHERE
	m2.customer=:customer_id AND m2.doctype = 855 AND (ru.stat_code LIKE '%discr%' or ru.stat_code LIKE '%-xpastdue%' or ru.stat_code LIKE'-xcancel%' or ru.stat_code LIKE '%-xduplicate') AND m2.srdatetime >= :back_date
GROUP BY  m2.tp , DATE(m2.srdatetime)", array('back_date' => $back_date, 'customer_id' => $customer));

        /** Adding total messages */
        $this->doQuery("INSERT INTO tmp1 SELECT 
	m2.customer as `userid`,
	'po-ack-total' as `type`,
	m2.tp as `series`, 
	DATE_FORMAT(m2.srdatetime,'%Y%m%d') as `xvalue`, 
	count(distinct m2.PK_id) as `data` 
FROM 
	Messages m2
WHERE
	m2.doctype=855 AND m2.customer=:customer_id AND m2.srdatetime >= :back_date 
GROUP BY m2.customer, m2.tp, DATE(m2.srdatetime)", array('back_date' => $back_date, 'customer_id' => $customer));

        /** Adding total error messages */
        $this->doQuery("INSERT INTO tmp1 SELECT 
	m2.customer as `userid`,
	'po-ack-total-err' as `type`,
	m2.tp as `series`, 
	DATE_FORMAT(m2.srdatetime,'%Y%m%d') as `xvalue`, 
	count(distinct m2.PK_id) as `data` 
FROM 
	discrepancy_report dr
            JOIN
    Messages m2 ON m2.PK_id = dr.doc_msgid
            JOIN
    discrepancy_exception_rules ru ON ru.id = dr.rule_id
WHERE
	m2.doctype=855 AND m2.customer=:customer_id AND (ru.stat_code LIKE '%discr%' or ru.stat_code LIKE '%-xpastdue%' or ru.stat_code LIKE'-xcancel%' or ru.stat_code LIKE '%-xduplicate') AND m2.srdatetime >= :back_date 
GROUP BY m2.customer, m2.tp, DATE(m2.srdatetime);", array('back_date' => $back_date, 'customer_id' => $customer));


        /*         * * INVOICES ** */
        /** Adding each discrepancy type **/
        $this->doQuery("INSERT INTO tmp1 SELECT 
    m2.customer AS userid,
    ru.stat_code AS `type`,
    m2.tp AS series,
    DATE_FORMAT(m2.srdatetime,'%Y%m%d') AS xvalue,
    COUNT(distinct m2.PK_id) AS data
FROM
    discrepancy_report dr
        JOIN
    Messages m1 ON m1.PK_id = dr.order_msgid
        JOIN
    Messages m2 ON m2.PK_id = dr.doc_msgid
        JOIN
    discrepancy_exception_rules ru ON ru.id = dr.rule_id
WHERE
	m2.customer=:customer_id AND m2.doctype = 810 AND (ru.stat_code LIKE '%discr%' or ru.stat_code LIKE '%-pastdue%' or ru.stat_code LIKE'-cancel%' or ru.stat_code LIKE '%-duplicate') AND m2.srdatetime >= :back_date
GROUP BY ru.stat_code , m2.tp , DATE(m2.srdatetime)", array('back_date' => $back_date, 'customer_id' => $customer));

        /** Adding Total Discrepancies **/
        $this->doQuery("INSERT INTO tmp1 SELECT  
    m2.customer AS userid,
    'po-inv-sdiscr' AS `type`,
    m2.tp AS series,
    DATE_FORMAT(m2.srdatetime,'%Y%m%d') AS xvalue,
    COUNT(distinct m2.PK_id) AS data
FROM
    discrepancy_report dr
        JOIN
    Messages m1 ON m1.PK_id = dr.order_msgid
        JOIN
    Messages m2 ON m2.PK_id = dr.doc_msgid
        JOIN
    discrepancy_exception_rules ru ON ru.id = dr.rule_id
WHERE
	m2.customer=:customer_id AND m2.doctype = 810 AND (ru.stat_code LIKE '%discr%' or ru.stat_code LIKE '%-xpastdue%' or ru.stat_code LIKE'-xcancel%' or ru.stat_code LIKE '%-xduplicate') AND m2.srdatetime >= :back_date
GROUP BY  m2.tp , DATE(m2.srdatetime) ", array('back_date' => $back_date, 'customer_id' => $customer));

        /** Adding total messages */
        $this->doQuery("INSERT INTO tmp1 SELECT 
	m2.customer as `userid`,
	'po-inv-total' as `type`,
	m2.tp as `series`, 
	DATE_FORMAT(m2.srdatetime,'%Y%m%d') as `xvalue`, 
	count(distinct m2.PK_id) as `data` 
FROM 
	Messages m2
WHERE
	m2.doctype=810 AND m2.customer=:customer_id AND m2.srdatetime >= :back_date 
GROUP BY m2.customer, m2.tp, DATE(m2.srdatetime) ", array('back_date' => $back_date, 'customer_id' => $customer));

        /** Adding total error messages */
        $this->doQuery("INSERT INTO tmp1 SELECT 
	m2.customer as `userid`,
	'po-inv-total-err' as `type`,
	m2.tp as `series`, 
	DATE_FORMAT(m2.srdatetime,'%Y%m%d') as `xvalue`, 
	count(distinct m2.PK_id) as `data` 
FROM 
	discrepancy_report dr
		JOIN
    Messages m2 ON m2.PK_id = dr.doc_msgid
    JOIN
    discrepancy_exception_rules ru ON ru.id = dr.rule_id
WHERE
	m2.doctype=810 AND m2.customer=:customer_id AND (ru.stat_code LIKE '%discr%' or ru.stat_code LIKE '%-xpastdue%' or ru.stat_code LIKE'-xcancel%' or ru.stat_code LIKE '%-xduplicate') AND m2.srdatetime >= :back_date 
GROUP BY m2.customer, m2.tp, DATE(m2.srdatetime);", array('back_date' => $back_date, 'customer_id' => $customer));



        /*         * * SHIPNOTICES ** */
        /** Adding each discrepancy type **/
        $this->doQuery("INSERT INTO tmp1 SELECT 
    m2.customer AS userid,
    ru.stat_code AS `type`,
    m2.tp AS series,
    DATE_FORMAT(m2.srdatetime,'%Y%m%d') AS xvalue,
    COUNT(distinct m2.PK_id) AS data
FROM
    discrepancy_report dr
        JOIN
    Messages m1 ON m1.PK_id = dr.order_msgid
        JOIN
    Messages m2 ON m2.PK_id = dr.doc_msgid
        JOIN
    discrepancy_exception_rules ru ON ru.id = dr.rule_id
WHERE
	m2.customer=:customer_id AND m2.doctype = 856 AND (ru.stat_code LIKE '%discr%' or ru.stat_code LIKE '%-pastdue%' or ru.stat_code LIKE'-cancel%' or ru.stat_code LIKE '%-duplicate') AND m2.srdatetime >= :back_date
GROUP BY ru.stat_code , m2.tp , DATE(m2.srdatetime) ", array('back_date' => $back_date, 'customer_id' => $customer));

    /** Adding Total Discrepancies **/
        $this->doQuery("INSERT INTO tmp1 SELECT  
    m2.customer AS userid,
	'po-asn-sdiscr' AS `type`,
    m2.tp AS series,
    DATE_FORMAT(m2.srdatetime,'%Y%m%d') AS xvalue,
    COUNT(distinct m2.PK_id) AS data
FROM
    discrepancy_report dr
        JOIN
    Messages m1 ON m1.PK_id = dr.order_msgid
        JOIN
    Messages m2 ON m2.PK_id = dr.doc_msgid
        JOIN
    discrepancy_exception_rules ru ON ru.id = dr.rule_id
WHERE
	m2.customer=:customer_id AND m2.doctype = 856 AND (ru.stat_code LIKE '%discr%' or ru.stat_code LIKE '%-xpastdue%' or ru.stat_code LIKE'-xcancel%' or ru.stat_code LIKE '%-xduplicate') AND m2.srdatetime >= :back_date
GROUP BY  m2.tp , DATE(m2.srdatetime) ", array('back_date' => $back_date, 'customer_id' => $customer));

        /** Adding total messages */
        $this->doQuery("INSERT INTO tmp1 SELECT 
	m2.customer as `userid`,
	'po-asn-total' as `type`,
	m2.tp as `series`, 
	DATE_FORMAT(m2.srdatetime,'%Y%m%d') as `xvalue`, 
	count(distinct m2.PK_id) as `data` 
FROM 
	Messages m2
WHERE
	m2.doctype=856 AND m2.customer=:customer_id AND m2.srdatetime >= :back_date 
GROUP BY m2.customer, m2.tp, DATE(m2.srdatetime) ", array('back_date' => $back_date, 'customer_id' => $customer));

        /** Adding total error messages */
        $this->doQuery("INSERT INTO tmp1 SELECT 
	m2.customer as `userid`,
	'po-asn-total-err' as `type`,
	m2.tp as `series`, 
	DATE_FORMAT(m2.srdatetime,'%Y%m%d') as `xvalue`, 
	count(distinct m2.PK_id) as `data` 
FROM 
	discrepancy_report dr
		JOIN
    Messages m2 ON m2.PK_id = dr.doc_msgid
    JOIN
    discrepancy_exception_rules ru ON ru.id = dr.rule_id
WHERE
	m2.doctype=856 AND m2.customer=:customer_id AND (ru.stat_code LIKE '%discr%' or ru.stat_code LIKE '%-xpastdue%' or ru.stat_code LIKE'-xcancel%' or ru.stat_code LIKE '%-xduplicate') AND m2.srdatetime >= :back_date 
GROUP BY m2.customer, m2.tp, DATE(m2.srdatetime);", array('back_date' => $back_date, 'customer_id' => $customer));


        /** All POs * */
        /** Adding total  messages */
        $this->doQuery("INSERT INTO tmp1 SELECT 
	m2.customer as `userid`,
	'po-total' as `type`,
	m2.tp as `series`, 
	DATE_FORMAT(m2.srdatetime,'%Y%m%d') as `xvalue`, 
	count(distinct m2.PK_id) as `data` 
    FROM 
        Messages m2
    WHERE
	m2.doctype=850 AND m2.customer=:customer_id AND m2.srdatetime >= :back_date 
    GROUP BY m2.customer, m2.tp, DATE(m2.srdatetime);", array('back_date' => $back_date, 'customer_id' => $customer));


        $this->doQuery("INSERT INTO PartnerStatistics(userid,type,series,xvalue,data) SELECT userid,type,series,xvalue,data FROM tmp1 b ON DUPLICATE KEY UPDATE data=b.data;",array());
        $count = $this->doQuery("SELECT ROW_COUNT();",array());
        $this->doQuery("DROP temporary TABLE tmp1;",array());
        return $count;
    }
    
}

date_default_timezone_set('America/Chicago');

//$dateFrom = date('2017-08-06'); //Date for testing
$dateFrom = date('Y-m-d', strtotime(' -10 day'));
$customer = 2825; //CHA
$sentDoctype = 850;
$recvDoctype = 855;

$er = new Discrepancies();
define('ISDEMO',$er->database_name === 'webedi30_demo');
$argNames=array('progname','customer','sentDoctype','recvDoctype','dateFrom');
echo "[Arguments]:\n";
foreach ($argv as $key => $value) {
    echo "\tARG[$key] ({$argNames[$key]}):$value\n";
}
var_dump('ISDEMO',ISDEMO);
if($argc ==2 && isset($argv[1]) && intval($argv[1]) > 0) {
 $x = $er->findExceptionsInDocument($sentDoctype,intval($argv[1]));
 var_dump($x);
 $er->persist($x);
 die();
} else {
    $customer = (isset($argv[1])) ? $argv[1] : $customer;
    $sentDoctype = (isset($argv[2])) ? $argv[2] : $sentDoctype;
    $recvDoctype = (isset($argv[3])) ? $argv[3] : $recvDoctype;
    $dateFrom = (isset($argv[4])) ? $argv[4] : $dateFrom;
}
echo "Processing: $recvDoctype responses to: $sentDoctype for tpid: $customer since $dateFrom\n";

$dataresults = $er->exceptionReport($sentDoctype, $recvDoctype, $dateFrom, $customer, null, date('Y-m-d'));
//var_dump($dataresults);

