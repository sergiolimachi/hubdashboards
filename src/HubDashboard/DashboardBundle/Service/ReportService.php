<?php

namespace HubDashboard\DashboardBundle\Service;

use Doctrine\ORM\EntityManager;

class ReportService  implements \Psr\Log\LoggerAwareInterface {

    /**
     *
     * @var EntityManager 
     */
    protected $em;
    protected $partnerID;
    protected $start;
    protected $end;
    /**
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    public function __construct(EntityManager $em, $partnerID, $dateFrom = null, $dateTo = null) {
        
        $this->em = $em;
        $this->partnerID = $partnerID;
        if (!($dateFrom instanceof \DateTime)) {
            if (empty($dateFrom)) {
                $dateFrom = new \DateTime();
                $dateFrom->sub(new \DateInterval('P1M'));
            } else {
                $dateFrom = new \DateTime($dateFrom);
            }
        }
        if (!($dateTo instanceof \DateTime)) {
            if (empty($dateTo)) {
                $dateTo = new \DateTime();
            } else {
                $dateTo = new \DateTime($dateTo);
            }
        }        
        
        $this->start = self::dateToXvalue($dateFrom);
        $this->end = self::dateToXvalue($dateTo);        
    }

    public static function dateToXvalue($date) {
        $today = new \DateTime();
        $today->setTime(0, 0, 0);
        $date->setTime(0, 0, 0);
        return (int) $date->format('Ymd');
    }

    public static function xvalueToDate($xvalue) {
        return \DateTime::createFromFormat('Ymd', $xvalue );
    }

    public function getReport($report, $subreport, $serieID, $parentSerieID = null) {
        $this->logger->debug($report);
        switch ($subreport) {
            case 'sup':
                $report = $this->getReportDiscr($report);//aqui
                break;
            case 'cust':
                $report = $this->getReportDiscrXCust($report, $serieID);
                break;
            case 'cust-detail':
                $report = $this->getReportDiscrXCustDetail($report, $serieID, $parentSerieID);
                break;
            default:
                $report = array('status' => 'Invalid report.');
                break;
        }
        return $report;
    }
    
    private static function getTitle($report, $sub = '') {
        switch ($report) {
            case 'po-ack-discr':
                $title = 'PO (850) - PO Ack (855) Discrepancies'.$sub;
                break;
            case 'po-asn-discr':
                $title = 'PO (850) - ASN (856) Discrepancies'.$sub;
                break;
            case 'po-inv-discr':
                $title = 'PO (850) - Invoice (810) Discrepancies'.$sub;
                break;
            default:
                $title = '';
                break;
        }
        return $title;
    }

    /**
     * Discrepancies Report
     */
    public function getReportDiscr($report) {
        $chartOptions = array(
            'credits' => array('enabled' => false),
            'chart' => array(
                'renderTo' => 'mainChart',
                'type' => 'pie',
                'events' => array(
                    'click' => '',
                ),
            ),
            'title' => array(
                'text' => self::getTitle($report)
            ),
//            'subtitle' => array(
//                'text' => 'Based on quantity, quantity unit, price and price unit discrepancies.'
//            ),
            'plotOptions' => array(
                'pie' => array(
                    'cursor' => 'pointer',
                    'dataLabels' => array(
                        'enabled' => false
                    ),
                    'showInLegend' => true
                ),
                'series' => array(
                    'dataLabels' => array(
                        'enabled' => false,
                        'format' => '{point.name}: {point.y:.1f}%',
                    ),
                )
            ),
            'series' => $this->getReportSeriesDiscr($report),
            'drilldown' => array(
                'series' => array()
            ),
        );
        return $chartOptions;
    }

    public function getReportSeriesDiscr($report) {
        $sql = "SELECT ps.series, sum(ps.data) sumdata, p.name "
                . "FROM PartnerStatistics ps INNER JOIN Partners p ON p.PK_id = ps.series "
                . "WHERE ps.userid = :userid AND ps.type LIKE :type AND CAST(xvalue AS UNSIGNED) BETWEEN :start AND :end AND ps.type <> 'po-ack-discr-priceunit' "
                . "GROUP BY ps.series "
                . "HAVING sum(ps.data) > 0;";
        $this->logger->debug($sql);
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('userid', $this->partnerID);
        $stmt->bindValue('type', $report . '-%');
        $stmt->bindValue('start', $this->start);
        $stmt->bindValue('end', $this->end);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $xdata = array();
        foreach ($result as $row) {
            $xdata[] = array(
                'id' => $row['series'],
                'name' => $row['name'],
                'y' => (int) $row['sumdata'],
                'events' => array(
                    'click' => '',
                ),
            );
        }

        $series = array();
        $data = array(
            'id' => $report,
            'name' => 'Supplier',
            'data' => $xdata
        );
        $series[] = $data;
        return $series;
    }

    public function getReportDiscrXCust($report, $serieID) {
        $range = $this->start - $this->end;
        if ($range >= 90) {
            $xstep = 10;
        } else if ($range >= 30) {
            $xstep = 5;
        } else {
            $xstep = 1;
        }

        $chartOptions = array(
            'credits' => array('enabled' => false),
            'chart' => array(
                'renderTo' => 'mainChart',
                'type' => 'line',
                'events' => array(
                    'click' => '',
                ),
            ),
            'title' => array(
                'text' => self::getTitle($report, ' by Supplier')
            ),
            'subtitle' => array(
                'text' => 'Based on quantity, quantity unit, price and price unit discrepancies.'
            ),
            'plotOptions' => array(
                'series' => array(
                    'dataLabels' => array(
                        'enabled' => false,
                        'format' => '{point.name}: {point.y:.1f}%',
                    ),
                )
            ),
            'xAxis' => array(
                'type' => 'category',
                'labels' => array(
                    'rotation' => -45,
                    'step' => $xstep,
                ),
            ),
            'yAxis' => array(
                'min' => 0,
            ),
            'series' => $this->getReportSeriesDiscrXCust($report, $serieID),
            'drilldown' => array(
                'series' => array()
            ),
        );
        return $chartOptions;
    }
    
    public function getReportSeriesDiscrXCust($report, $serieID) {
        $sql = "SELECT CAST(ps.xvalue AS UNSIGNED INTEGER) AS xvalue, sum(ps.data) as sumdata, p.name "
                . "FROM PartnerStatistics ps INNER JOIN Partners p ON p.PK_id = ps.series "
                . "WHERE ps.userid = :userid AND ps.type LIKE :type AND CAST(xvalue AS UNSIGNED) BETWEEN :start AND :end AND ps.series = :series  AND ps.type <> 'po-ack-discr-priceunit' "
                . "GROUP BY xvalue, p.name "
                . "HAVING sum(ps.data) > 0 "
                . "ORDER BY xvalue ASC;";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('userid', $this->partnerID);//396
        $stmt->bindValue('type', $report . '-%');
        $stmt->bindValue('start', $this->start);
        $stmt->bindValue('end', $this->end);
        $stmt->bindValue('series', $serieID);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $xdata = array();
        for( $date = self::xvalueToDate($this->start); self::dateToXvalue($date) <= (int)$this->end; $date->add(new \DateInterval('P1D'))) {
            $xvalue = self::dateToXvalue($date);
            $xdata["X-$xvalue"] = array(
                'id' => (string)$xvalue,
                'pserie' => $serieID,
                'name' => $date->format('m/d/Y'),
                'y' => 0,
                'events' => array(
                    'click' => '',
                ),
            );
        }

        foreach ($result as $i => $row) {
            $date = self::xvalueToDate((int) $row['xvalue']);
            $xdata["X-{$row['xvalue']}"]['y'] = (int) $row['sumdata'];
        }
        
        $series = array();
        $data = array(
            'id' => $report . '-' . $serieID,
            'name' => $row['name'],
            'data' => array_values($xdata)
        );
        $series[] = $data;
        return $series;
    }

    public function getReportDiscrXCustDetail($report, $serieID, $parentSerieID) {
        $chartOptions = array(
            'credits' => array('enabled' => false),
            'chart' => array(
                'renderTo' => 'mainChart',
                'type' => 'column',
                'events' => array(
                    'click' => '',
                ),
            ),
            'title' => array(
                'text' => self::getTitle($report, ' by Supplier - Detail')
            ),
            'subtitle' => array(
                'text' => 'Detail for Quantity, Quantity unit, Price and Price unit discrepancies.'
            ),
            'plotOptions' => array(
                'series' => array(
                    'dataLabels' => array(
                        'enabled' => false,
                        'format' => '{point.name}: {point.y:.1f}%',
                    ),
                    'point' => array(
                        'events' => array(
                            'click' => '',
                        )
                    )
                )
            ),
            'xAxis' => array(
                'categories' => array(
                    self::xvalueToDate($serieID)->format('m/d/Y'),
                )
            ),
            'yAxis' => array(
                'min' => 0,
            ),
            'series' => $this->getReportSeriesDiscrXCustDetail($report, $parentSerieID, $serieID),
            'drilldown' => array(
                'series' => array()
            ),
        );
        return $chartOptions;
    }

    public function getReportSeriesDiscrXCustDetail($report, $series, $xvalue) {
        $sql = "SELECT ps.xvalue, ps.data, ps.type, p.name "
                . "FROM PartnerStatistics ps INNER JOIN Partners p ON p.PK_id = ps.series "
                . "WHERE ps.userid = :userid AND ps.type LIKE :type AND ps.series = :series AND ps.xvalue = :xvalue AND ps.type <> 'po-ack-discr-priceunit'"
                //. "WHERE ps.userid = :userid AND ps.type LIKE :type AND CAST(xvalue AS UNSIGNED) BETWEEN :start AND :end AND ps.series = :series AND ps.xvalue = :xvalue "
                . "ORDER BY ps.xvalue DESC;";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('userid', $this->partnerID);
        $stmt->bindValue('type', $report . '-%');
        //$stmt->bindValue('start', $this->start);
        //$stmt->bindValue('end', $this->end);
        $stmt->bindValue('series', $series);
        $stmt->bindValue('xvalue', $xvalue);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        $type = array(
            $report . '-qty' => 'Quantity (02)',
            $report . '-qtyunit' => 'Quantity Unit (03)',
            $report . '-price' => 'Price (04)',
            $report . '-priceunit' => 'Price Unit (05)',
            $report . '-shiplate' => 'Late Shipping',
            $report . '-shipnot' => 'No Shipping',
        );
        $series = array();
        foreach ($result as $row) {
            $data = array(
                'name' => $type[$row['type']],
                'data' => array((int) $row['data'])
            );
            $series[] = $data;
        }
        
        return $series;
    }
    /**
     * 
     * @param string $report
     * @param string $xdate
     * @param integer $tp Trading Partner PK_id. 
     * @param integer $elementIndex
     * @return type
     */
    public function getDiscrMessages($report, $xdate, $tp, $elementIndex = -1) {
        $header = array(
            'id1' => 'PO',
            'val1' => 'Value',
            'id2' => '',
            'val2' => 'Value',
        );
        
        switch ($report) {
            case 'po-ack-discr':
                $doctype = 850;
                $doctype2 = 855;
                $segment2 = 'PO1';
                $header['id2'] = 'PO-Ack';
                $header['val1'] = 'BUYER';
                $header['val2'] = ' ';
                break;
            case 'po-asn-discr':
                $doctype = 850;
                $doctype2 = 856;
                $segment2 = 'SN1';
                $header['val1'] = 'SHIP DATE';
                $header['val2'] = ' ';
                $header['id2'] = 'ASN';
                break;
            case 'po-inv-discr':
                $doctype = 850;
                $doctype2 = 810;
                $segment2 = 'IT1';
                $header['id2'] = 'Invoice';
                break;
        }
        
        $data = array();
        $customer = $this->partnerID;
        $date = self::xvalueToDate($xdate)->format('Y-m-d');
        $this->logger->debug($date);
        $this->logger->debug($tp);
        $sql = "SELECT  m1.PK_id as m1_PK_id,
                        m1.reference as m1_reference,
                        m1.shipdate as m1_shipdate,
                        m1.srdatetime as m1_srdatetime,
                        m1.buyername as m1_buyername,
                        m2.PK_id as m2_PK_id,
                        m2.reference as m2_reference,
                        m2.shipdate as m2_shipdate,
                        m2.srdatetime as m2_srdatetime,
                        dr.rule_id as rule_id,
                        dr.line_number as line_number,
                        dr.original_value as original_value,
                        dr.changed_value as changed_value,
                        e.`index` as el_index
            FROM discrepancy_report dr 
            JOIN discrepancy_exception_rules r ON r.id = dr.rule_id
            JOIN Messages m1 ON m1.PK_id=dr.order_msgid 
            JOIN Messages m2 ON m2.PK_id=dr.doc_msgid 
            JOIN Elements e ON e.PK_id = dr.element_id
            WHERE 
		m2.customer=:customer 
                AND m2.tp=:tp 
                AND m2.srdatetime 
                BETWEEN CONCAT(:date,' 00:00:00') 
                AND CONCAT(:date,' 23:59:59') 
                AND m2.doctype=:doctype 
                AND m2.folder='inbox'
                AND r.stat_code like '%-discr%'
                GROUP BY m2.PK_id,dr.rule_id;";
        
        $this->logger->debug($sql);
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute(array(
            'customer'=>$customer,
            'tp'=>$tp,
            'doctype'=>$doctype2,
            'date'=>$date
        ));
        $msgs = $stmt->fetchAll();
        foreach ($msgs as $msg) {
            if (!empty($msg['srdatetime'])) {
                $msgdate = new \DateTime($msg['srdatetime']);
            }
            if ($doctype2 == 856 && $msg['m1_shipdate'] != $msg['m2_shipdate']) {
                $shipdate1 = new \DateTime(str_replace("-", "/", $msg['m1_shipdate']));
                if($msg['m2_shipdate'] == '0000-00-00 00:00:00') {
                    //shipdate - No Shipping
                    if($elementIndex == -2){
                        $data[] = array(
                            'id1' => $msg['m1_PK_id'],
                            'id2' => $msg['m2_PK_id'],
                            'val1' => $shipdate1->format('m/d/Y'),
                            'val2' => '-',
                        );
                    }
                } else {
                    //shipdate - Late Shipping
                    if($elementIndex == -3){
                        $shipdate2 = new \DateTime(str_replace("-", "/", $msg['m2_shipdate']));
                        $delay = (int)$shipdate1->diff($shipdate2)->format('%a');
                        if($delay > 0){
                            $data[] = array(
                                'id1' => $msg['m1_PK_id'],
                                'id2' => $msg['m2_PK_id'],
                                'val1' => $shipdate1->format('m/d/Y'),
                                'val2' => $shipdate2->format('m/d/Y'),
                            );
                        }
                    }
                }
            }
            if(empty($msg['m1_buyername'])) {
                $msg['m1_buyername']='Unknown Buyer';
            }
            if($elementIndex > -1 && $msg['el_index'] == $elementIndex ) {
                $row= array(
                    'id1'=>$msg['m1_reference'],
                    'id2'=>$msg['m2_reference'],
                    'val2'=>'<a href="'.$msg['m1_PK_id'].':'.$msg['m2_PK_id'].'" data-remote="false" data-toggle="modal" data-target="#myModal">Details</a>');
                switch ($report) {
                    case 'po-ack-discr':
                    case 'po-inv-discr':
                        $row['val1']=$msg['m1_buyername'];
                        break;
                    case 'po-asn-discr':
                        $row['val1']=$msg['m2_shipdate'];
                        break;
                }
                $data[] = $row;
            }
        }
        return array('header' => $header, 'body' => $data);
    }

    
    public function getBuyersReport() 
    {
        $sql = "SELECT m1.buyername AS buyername
                FROM Messages m1 
                WHERE customer = :userid
                AND buyername IS NOT NULL 
                GROUP BY m1.buyername
                ORDER BY m1.buyername";

        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('userid', $this->partnerID);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        $buyers = array();
        foreach ($result as $row) {
            $buyers[] = $row['buyername'];
        }
        
        return $buyers;        
    }
    
    public function getSuppliersReport() 
    {
        $sql = "SELECT p.name, a.tp 
                FROM PartnerAccess a 
                INNER JOIN Partners p ON a.tp = p.PK_id 
                WHERE a.customer = :userid AND p.status = 'Active' AND p.`type` IN
                ('WebEDI','TP','Hosted','Software','Van','HUB')
                ORDER BY name";

        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('userid', $this->partnerID);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $suppliers = array();
        foreach ($result as $row) {
            $data = array(
                'name' => $row['name'],
                'tp' => $row['tp']
            );
            $suppliers[] = $data;
        }

        return $suppliers;
    }
    
    
    public function extractDetails($segment) {
        $detail=array();
        if(isset($segment['elem']) && $segment['elem'] != "") {
            $det = explode(";", $segment['elem']);
            $detail = array();
            foreach ($det as $de) {
                $d = explode(":", $de);
                $detail[$d[0]] = $d[1];
            }
        }
        return $detail;
    }

    public function getExceptionReport($date, $filter, $buyer, $supplier,$origDoctype=850,$changeDoctype=855)
    {
        switch ($date) {
            case '1': //Current day
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d');
                break;
            case '2': //Yesterday
                $startDate = date('Y-m-d', strtotime(' -1 day'));
                $endDate = date('Y-m-d', strtotime(' -1 day'));
                break;
            case '3': //Current week
                $startDate = date('Y-m-d', strtotime(' Monday this week'));
                $endDate = date('Y-m-d', strtotime(' Sunday this week'));
                break;
            case '4': //Past week
                $startDate = date('Y-m-d', strtotime(' Monday last week'));
                $endDate = date('Y-m-d', strtotime(' Sunday last week'));
                break;
            case '5': //Current month
                $startDate = date('Y-m-d', strtotime(' first day of this month'));
                $endDate = date('Y-m-d', strtotime(' last day of this month'));
                break;
            case '6': //Last month
                $startDate = date('Y-m-d', strtotime(' first day of last month'));
                $endDate = date('Y-m-d', strtotime(' last day of last month'));
                break;
        }
        $sdate = new \DateTime($startDate);
        $sdate->setTime(00, 00, 00);
        $startDate = $sdate->format('Y-m-d H:i:s');
        
        if ($endDate == null) {            
            $edate = new \DateTime($startDate);
            $edate->setTime(23, 59, 59);
            $toDate = $edate->format('Y-m-d H:i:s');

        }else{            
            $edate = new \DateTime($endDate);
            $edate->setTime(23, 59, 59);     
            $toDate = $edate->format('Y-m-d H:i:s');
        }
        
        $status =   array(  'AA' => 'Accepted - Order Forwarded to Alternate Supplier Location',
                'AC' => 'Accepted and Shipped',
                'AR' => 'Accepted and Released for Shipment',
                'BP' => 'Accepted - Partial Shipment, Balance Backordered',
                'DR' => 'Accepted - Date Rescheduled',
                'IA' => 'Accepted',
                'IB' => 'Backordered',
                'IC' => 'Accepted - Changes Made',
                'ID' => 'Deleted',
                'IE' => 'Accepted, Price Pending',
                'IF' => 'On Hold, Incomplete Description',
                'IH' => 'On Hold',
                'IP' => 'Accepted - Price Changed',
                'IQ' => 'Accepted - Quantity Changed',
                'IR' => 'Rejected',
                'IS' => 'Accepted - Substitution Made',
                'IW' => 'On Hold - Waiver Required',
                'R1' => 'Rejected, Not a Contract Item', 
                'R2' => 'Rejected, Invalid Item Product Number',
                'R3' => 'Rejected, Invalid Unit of Issue',
                'R4' => 'Rejected, Contract Item not Available',
                'R5' => 'Rejected, Reorder Item as a Just in Time (JIT) Order',
                'R6' => 'Rejected, Reorder Item as an Extended Delivery Order (EDO)',
                'R7' => 'Rejected, Reorder Item as a Drop Shipment',
                'R8' => 'Rejected, Reorder Item as a Surge Order',
                'SP' => 'Accepted - Schedule Date Pending',
                '-' => 'Missing Line',
                'INV'=>'Invoiced');        
        
        $sql = "SELECT 
                    m1.PK_id AS `msgid-ORIG`,
                    m2.PK_id AS `msgid-CHANGE`,
                    m1.rootnode AS `rootnode-ORIG`,
                    m2.rootnode AS `rootnode-CHANGE`,
                    m1.reference AS `po-reference`,
                    m2.reference AS `poack-reference`,
                    m1.ack AS `po-confirmation-997`,
                    m2.srdatetime AS `date`,
                    p.name AS `vendor-name`,
                    m1.buyername AS `buyer-name`,
                    d.line_number AS `line-number`,
                    n1.PK_id AS `PO1-ORIG`,
                    n2.PK_id AS `PO1-CHANGE`,
                    n1.title AS `title-ORIG`,
                    n2.title AS `title-CHANGE`,
                    m1.shipdate AS `ship-date`
                FROM
                    Messages m1
                    JOIN discrepancy_report d ON d.order_msgid = m1.PK_id
                    JOIN Partners p ON p.PK_id = m1.tp
                    JOIN Messages m2 ON d.doc_msgid = m2.PK_id
                    LEFT JOIN Elements e1 ON e1.PK_id = d.element_id
                    LEFT JOIN Elements e2 ON e2.PK_id = d.doc_element_id
                    LEFT JOIN Nodes n1 ON n1.PK_id = e1.nodepointer
                    LEFT JOIN Nodes n2 ON n2.PK_id = e2.nodepointer
                WHERE
                    m2.customer = :customer 
                    AND m2.doctype = :change_doctype
                    AND m1.customer = :customer 
                    AND m1.doctype = :orig_doctype
                    AND m2.srdatetime BETWEEN :startdate AND :enddate ";
        
        if ($filter == '2') {
            $sql .= " AND m1.buyername = :buyer";
        }
        else if ($filter == '3'){
            $sql .= " AND p.PK_id = :supplier";
        }
	$sql .= " GROUP BY m1.PK_id,m2.PK_id,d.line_number";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('customer', $this->partnerID);
        $stmt->bindValue('change_doctype', $changeDoctype);
        $stmt->bindValue('orig_doctype', $origDoctype);
        $stmt->bindValue('startdate', $startDate);
        $stmt->bindValue('enddate', $toDate);

        if ($filter == '2') {
            $stmt->bindValue('buyer', $buyer);
        }
        else if ($filter == '3'){
            $stmt->bindValue('supplier', $supplier);
        }

        $ret=$stmt->execute();
        $result = $stmt->fetchAll();
        $datareport = array();        
        $node850 = "";
        $itemdetail = "";
        $ackNode = "";
        $qty850 = "";
        $qty855 = "";
        $uom850 = "";
        $uom855 = "";
        $buyeritem = "";
        $vendorpart850 = "";
        $vendorpart855 = "";
        $datecurrentship = "";
        $statusname = "";
        $price = "";
        $pricechange = "";
        $extendedprice = "";
        $extendedpricechange = "";
        $ackstatus = "";        
        foreach ($result as $row) {
            $reducedOrder=$this->getLines($row['rootnode-ORIG']);
            $reducedDoc=$this->getLines($row['rootnode-CHANGE']);
            
            
            $msgPo = $this->extractDetails($reducedOrder[$row['line-number']]);
            $msgPoAck = $this->extractDetails($reducedDoc[$row['line-number']]);
            $detail=$this->extractDetails($reducedDoc[$row['line-number']]['PID']);
            $itemdetail=(isset($detail[5]) ? $detail[5] : "");
            
            if($changeDoctype == '855') {
                $detail=$this->extractDetails($reducedDoc[$row['line-number']]['ACK']);
                $ackstatus = (isset($detail[1]) ? $detail[1] : '-');
                $datecurrentship = (isset($detail[5]) ? $detail[5] : "");
            }else if($changeDoctype == '810') {
                $detail=$this->extractDetails($reducedDoc[$row['line-number']]); 
                $ackstatus=(isset($detail[1]) ? 'INV' : '-');
            }else if($changeDoctype == '856') {
                $detail=$this->extractDetails($reducedDoc[$row['line-number']]['SN1']); 
                $ackstatus=(isset($detail[1]) ? 'ASN' : '-');
            }
            $statusname = ((isset($ackstatus) && $ackstatus !='') ? $status[$ackstatus] : '');
                       
            if (isset($msgPo)) { 
                $qty850 = $msgPo[2];
                $uom850 = $msgPo[3];
                $buyeritem = $msgPo[7];
                $vendorpart850 = $msgPo[9];
                $price = $msgPo[4];
                $extendedprice = $msgPo[4] * $msgPo[2];
            }
            if (isset($msgPoAck)) {
                $qty855 = (isset($msgPoAck[2]) ? $msgPoAck[2] : "");
                $uom855 = (isset($msgPoAck[3]) ? $msgPoAck[3] : "");
                $vendorpart855 = (isset($msgPoAck[7]) ? $msgPoAck[7] : "");
                $pricechange = (isset($msgPoAck[4]) ? $msgPoAck[4] : "");
                $extendedpricechange = (isset($msgPoAck[4]) ? $msgPoAck[4] * $msgPoAck[2] : "");
            }
            
            $data = array(
                'poreference' => $row['po-reference'],
                'poackreference' => $row['poack-reference'],
                'poconfirmation997' => $row['po-confirmation-997'],
                'date' => $row['date'],
                'vendorname' => $row['vendor-name'],
                'buyername' => $row['buyer-name'],
                'linenumber' => $row['line-number'],
                'qty850' => $qty850,
                'qty855' => $qty855,
                'uom850' => $uom850,
                'uom855' => $uom855,
                'buyeritem' => $buyeritem,
                'vendorpart850' => $vendorpart850,
                'vendorpart855' => $vendorpart855,
                'itemdetail' => $itemdetail,
                'idstatus' => $ackstatus,
                'status' => $statusname,
                'datecurrentship' => $datecurrentship,
                'price' => $price,
                'pricechange' => $pricechange,
                'extendedprice' => $extendedprice,
                'extendedpricechange' => $extendedpricechange
            );
            if($changeDoctype == '856') {
                $detail=$this->extractDetails($reducedDoc[$row['line-number']]['SN1']); 
                $data['qty855']=$detail[2];
                $data['uom855']=$detail[3];
                $data['pricechange'] =(isset($pricechange))?$price:'';
                $data['extendedpricechange']=(isset($extendedprice))?$extendedprice:'';
            }
            $datareport[] = $data;
        }
        return $datareport;
    }
    
    function getLines($rootnode) {
        $sql = "SELECT n.PK_id ,n.title as `title`,n.parent,n.left,n.right, "
                . "group_concat(e.`index`,':',e.`data` ORDER BY e.`index` ASC SEPARATOR ';') as `elem` "
                . "FROM Nodes n JOIN Elements e ON e.nodepointer=n.PK_id "
                . "WHERE e.rootnode=:msgroot "
                . "GROUP BY n.PK_id ORDER BY n.PK_id ASC";
        
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('msgroot', $rootnode);
        $stmt->execute();
        $rows = $stmt->fetchAll(); 
        
        $reduced=array();
        $lineNumber='header';
        foreach($rows as $row) {
            if(in_array($row['title'], array('PO1','IT1','LIN')) ) {
                $lineNumber=($lineNumber != 'header')?++$lineNumber:1; // Use Line Number or Start at 1 and increment
                $reduced[$lineNumber] = $row;
            }else{
                $reduced[$lineNumber][$row['title']]=$row;
            }
        }
        //Remove index 'header' from reduce result
        unset($reduced['header']);
        return $reduced;
    }

    public function setLogger(\Psr\Log\LoggerInterface $logger) {
        $this->logger=$logger;
    }

}
