<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace HubDashboard\DashboardBundle\Service;

/**
 * Description of DataService
 *
 * @author xmsim
 */
class DataService {

    public static function updateStats($em, $doctype2) {
        set_time_limit(0);
        $statPrefix = '';
        switch ($doctype2) {
            case 810:
                $statPrefix = 'po-inv-discr';
                break;
            case 855:
                $statPrefix = 'po-ack-discr';
                break;
            case 856:
                $statPrefix = 'po-asn-discr';
                break;
            default:
                $statPrefix = 'po';
        }

        $sql = "SELECT *, cast(xvalue as unsigned) as xv FROM PartnerStatistics WHERE type LIKE '$statPrefix-%' ORDER BY xv DESC;";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        $sql = "UPDATE LOW_PRIORITY PartnerStatistics SET xvalue=:xvalue+1 WHERE userid=:userid and type=:type and series=:series and xvalue=:xvalue and data=:data;";
        $stmt = $em->getConnection()->prepare($sql);
        foreach ($result as $row) {    
            $stmt->execute($row);
        }
        
        $startDate = new \DateTime();
        $startDate->setTime(0, 0, 0);

        $endDate = new \DateTime();
        $endDate->add(new \DateInterval('P1D'));
        $endDate->setTime(23, 59, 59);

        return self::generateStats($em, $doctype2, $startDate->format('Y-m-d'), $endDate->format('Y-m-d'));
    }

    /**
     * 
     * @param type $em
     * @param type $doctype2
     * @param type $tp Is the Logged user
     * @param type $cust Is the Vendor/Supplier ID
     * @return int
     */
    public static function deleteStats($em, $doctype2, $tp = null, $cust = null) {
        set_time_limit(0);
        $statPrefix = '';
        switch ($doctype2) {
            case 810:
                $statPrefix = 'po-inv-discr';
                break;
            case 855:
                $statPrefix = 'po-ack-discr';
                break;
            case 856:
                $statPrefix = 'po-asn-discr';
                break;
            default:
                return 550;
        }

        $sql = "DELETE FROM PartnerStatistics WHERE type LIKE '$statPrefix-%' ";
        if ($tp != null) {
            $sql = $sql . " AND userid=$tp ";
        }
        if ($cust != null) {
            $sql = $sql . " AND series=$cust ";
        }
        $sql = $sql . ";";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        return 250;
    }

    /**
     * 
     * @param type $em
     * @param type $doctype2
     * @param type $startDate
     * @param type $endDate
     * @param type $tp Is the logged User (HUB)
     * @param type $customer Is the Vendor/Supplier
     * @return int
     */
    public static function generateStats($em, $doctype2, $startDate = '2014-01-01', $endDate = null, $tp = null, $customer = null) {
        set_time_limit(0);
        $statPrefix = '';
        $doctype1 = 850;
        $segment1 = 'PO1';
        switch ($doctype2) {
            case 810:
                $statPrefix = 'po-inv-discr';
                $segment2 = 'IT1';
                break;
            case 855:
                $statPrefix = 'po-ack-discr';
                $segment2 = 'PO1';
                break;
            case 856:
                $statPrefix = 'po-asn-discr';
                $segment2 = 'PO1';
                break;
            default:
                return 550;
        }

//        $sql0 = "DELETE FROM PartnerStatistics WHERE userid=$tp AND type LIKE 'po-ack-discr%'";
//        $stmt0 = $em->getConnection()->prepare($sql0);
//        $stmt0->execute();

        if ($endDate == null) {
            $date = new \DateTime();
            $date->setTime(23, 59, 59);
            $endDate = $date->format('Y-m-d');
        }
        $sql = "SELECT * FROM Messages WHERE srdatetime BETWEEN '$startDate' AND '$endDate' AND doctype=$doctype1 ";
        if ($tp != null) {
            $sql = $sql . " AND customer=$tp";
        }
        if ($customer != null) {
            $sql = $sql . " AND tp=$customer ";
        }
        $sql = $sql . ";";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $msgs = $stmt->fetchAll();

        foreach ($msgs as $msg) {
            if (!empty($msg['srdatetime'])) {
                $msgdate = new \DateTime($msg['srdatetime']);
            }
            $xvalue = ReportService::dateToXvalue($msgdate);

            $sql = "SELECT * FROM MessageReferences ref INNER JOIN Messages m ON ref.FK_message = m.PK_id WHERE ref.reference={$msg['reference']} AND m.doctype=$doctype2;";
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $assocs = $stmt->fetchAll();
            
            $sql1 = "SELECT n.PK_id, e.index, e.data FROM Elements e INNER JOIN Nodes n ON n.PK_id = e.nodepointer "
                    . "WHERE e.rootnode={$msg[rootnode]} AND n.title = '$segment1' AND e.index IN (1,2,3,4,5);";
            $stmt1 = $em->getConnection()->prepare($sql1);
            $stmt1->execute();
            $nodes1 = $stmt1->fetchAll();
            $values1 = array();
            foreach ($nodes1 as $node) {
                if (!isset($values1[$node['PK_id']])) {
                    $values1[$node['PK_id']] = array();
                }
                $values1[$node['PK_id']][$node['index']] = $node['data'];
            }

            foreach ($assocs as $msg2) {
                if ($doctype2 == 856 && $msg['shipdate'] != $msg2['shipdate']) {
                    if($msg2['shipdate'] == '0000-00-00 00:00:00') {
                        //shipdate - No Shipping
                        
                        $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefix-shipnot',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                        $stmt = $em->getConnection()->prepare($sql);
                        $stmt->execute();
                    } else {
                        //shipdate - Late Shipping
                        $shipdate1 = new \DateTime(str_replace("-", "/", $msg['shipdate']));
                        $shipdate2 = new \DateTime(str_replace("-", "/", $msg2['shipdate']));
                        $delay = (int)$shipdate1->diff($shipdate2)->format('%a');
                        if($delay > 0){
                            $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefix-shiplate',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                            $stmt = $em->getConnection()->prepare($sql);
                            $stmt->execute();
                        }
                    }
                }
                
                $sql2 = "SELECT n.PK_id, e.index, e.data FROM Elements e INNER JOIN Nodes n ON n.PK_id = e.nodepointer "
                        . "WHERE e.rootnode={$msg2['rootnode']} AND n.title = '$segment2' AND e.index IN (1,2,3,4,5);";
                $stmt2 = $em->getConnection()->prepare($sql2);
                $stmt2->execute();
                $nodes2 = $stmt2->fetchAll();
                $values2 = array();
                foreach ($nodes2 as $node) {
                    if (!isset($values2[$node['PK_id']])) {
                        $values2[$node['PK_id']] = array();
                    }
                    $values2[$node['PK_id']][$node['index']] = $node['data'];
                }

                foreach ($values1 as $val1) {
                    foreach ($values2 as $val2) {
                        if (isset($val1[1]) && isset($val2[1]) && $val1[1] == $val2[1]) {
                            if (isset($val1[2]) && isset($val2[2]) && $val1[2] != $val2[2]) {
                                //PO1:02 - Quantity
                                $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefix-qty',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                $stmt = $em->getConnection()->prepare($sql);
                                $stmt->execute();
                            }
                            if (isset($val1[3]) && isset($val2[3]) && $val1[3] != $val2[3]) {
                                //PO1:03 - Quantity Unit
                                $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefix-qtyunit',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                $stmt = $em->getConnection()->prepare($sql);
                                $stmt->execute();
                            }
                            if (isset($val1[4]) && isset($val2[4]) && $val1[4] != $val2[4]) {
                                //PO1:04 - Price
                                $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefix-price',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                $stmt = $em->getConnection()->prepare($sql);
                                $stmt->execute();
                            }
                            if (isset($val1[5]) && isset($val2[5]) && $val1[5] != $val2[5]) {
                                //PO1:05 - Price Unit
                                $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefix-priceunit',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                $stmt = $em->getConnection()->prepare($sql);
                                $stmt->execute();
                            }
                        }
                    }
                }
            }
        }
        return 250;
    }   
    
    /**
     * 
     * @param type $em
     * @param type $doctype2
     * @param type $startDate
     * @param type $endDate
     * @param type $tp Is the Logged User (HuB)
     * @param type $customer is the Vendor/Supplier
     * @return int
     */
    public static function generateScored($em, $doctype2, $startDate = '2014-01-01', $endDate = null, $tp = null, $customer = null) {
        set_time_limit(0);
        $statPrefix = '';
        $doctype1 = 850;
        $segment1 = 'BEG';

        switch ($doctype2) {
            case 810:
                $statPrefix2 = 'po-inv-missing';
                $segment2 = $segment1 .", IT1";
                $title = 'BEG';
                break;
            case 850: 
                $statPrefix = 'po-duplicate-send';
                break;               
            case 855:
                $statPrefix = 'po-ack-pastdue';
                $statPrefix2 = 'po-ack-missing';
                $segment2 = $segment1 .", PO1";
                $title = 'DTM';
                $dtmQualifiers = '002, 010, 036, 038, 055, 063, 071, 074, 081, 084, 106, 146, 374'; 
                break;
            case 856:
                $statPrefix = 'po-asn-cancel';
                $statPrefix2 = 'po-asn-missing';
                $dtmQualifiers = '001, 061, 175, 177';
                $segment2 = $segment1 .", PO1";
                $title = 'DTM';
                break;
            default:
                return 550;
        }
        
        $sdate = new \DateTime($startDate);
        $sdate->setTime(0, 0, 0);
        $startDate = $sdate->format('Y-m-d H:i:s');

        if ($endDate == null) {
            
            $date = new \DateTime();
            $date->setTime(23, 59, 59);
            $endDate = $date->format('Y-m-d H:i:s');
            
        }else{
            
            $edate = new \DateTime($endDate);
            $edate->setTime(23, 59, 59);     
            $endDate = $edate->format('Y-m-d H:i:s');
        }        
       
        if($doctype2 != 850 ){
            //Nota verificar lo de las fecha shipdate docdatetime
            $sql = "SELECT e.rootnode msgnode, n.PK_id node, n.parent, n.left, 
                           n.right, m.tp, m.customer, m.PK_id msg, m.srdatetime, 
                           ma.PK_id msga, ma.docdatetime as datedoc2, e.index, 
                           e.data , ma.doctype doctype2
                      FROM Elements e 
                     INNER JOIN Nodes n ON e.nodepointer = n.PK_id 
                     INNER JOIN Messages m ON e.rootnode = m.rootnode 
                      LEFT JOIN MessageReferences a ON m.reference = a.reference 
                      LEFT JOIN Messages ma ON a.FK_message = ma.PK_id 
                     WHERE m.srdatetime BETWEEN '" . $startDate . "' AND '" . $endDate . "' 
                       AND m.doctype = " . $doctype1 . " 
                       AND n.title = '" . $title ."'";

            if ($tp != null) {
                $sql = $sql . " AND m.customer = $tp";
            }
            
            if ($customer != null) {
                $sql = $sql . " AND m.tp = " . $customer ;
            }
            
            $sql = $sql . " ORDER BY msg, msga, n.PK_id, e.index;";
            
//            echo $sql."<br>";
//            die();

            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $msgs = $stmt->fetchAll();
            $i = 0;
            foreach ($msgs as $msg) {

                if (!empty($msg['srdatetime'])) {
                    $msgdate = new \DateTime($msg['srdatetime']);
                }

                $xvalue = ReportService::dateToXvalue($msgdate);

                if($msg['index'] == 1){

                    $node = $msg['parent'];
                    var_dump($node);
                    

                    while($node != null){
                        
                        if($msg['doctype2'] == $doctype2){

                            $sql = "SELECT * FROM Nodes WHERE PK_id =" . $node . ";";                

                            $stmt = $em->getConnection()->prepare($sql);
                            $stmt->execute();
                            $parents = $stmt->fetchAll();

                            if(strpos($segment2, $parents[0]['title']) >= 0){ 
;
                                $date = (strlen($msgs[$i+1]['data'])  > 6 ) 
                                      ? substr($msgs[$i+1]['data'], 0,4)."-". substr($msgs[$i+1]['data'], 4,2)."-". substr($msgs[$i+1]['data'], 6,2) 
                                      : substr($msgs[$i+1]['data'], 0,2)."-". substr($msgs[$i+1]['data'], 2,2)."-". substr($msgs[$i+1]['data'], 4,2);
                                if($msg['index'] == 1){
                                    $datepo = new \DateTime($date);
                                    $datepo->setTime(23, 59, 59);                            
                                    $datedoc2 = (isset($msg['datedoc2'])) ? new \DateTime($msg['datedoc2']) : NULL;
                                    
                                    if(isset($datedoc2)) {                                
                                        if($datedoc2 > $datepo){
                                            if($$msg['doctype2'] == 855 || $$msg['doctype2'] == 856){
                                                $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefix',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                                $stmt = $em->getConnection()->prepare($sql);
                                                $stmt->execute();
                                                //echo $sql."<br>";
                                            }
                                        }
                                    }else{
                                            $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ({$msg['customer']},'$statPrefix2',{$msg['tp']},$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                                                $stmt = $em->getConnection()->prepare($sql);
                                                $stmt->execute();
                                                //echo $sql."<br>";                         
                                    }    
                                }                        
                                break;
                            }

                            $node = $parents[0]['parent'];  
                        }
                    }
                }
                $i++;

            }
            
        }else{            
            //Find Duplicate documents (Error) 
            $sql = "SELECT DISTINCT
                        count(d.reference), m . *
                    FROM
                        Messages m
                            INNER JOIN
                        Messages d ON m.reference = d.reference
                    WHERE 
                        m.doctype = d.doctype 
                        AND m.customer = d.customer 
                        AND m.sent = d.sent 
                        AND m.folder = 'sent' 
                        AND d.sent = 1 
                        AND d.doctype = " . $doctype2 . " 
                        AND d.srdatetime BETWEEN '" . $startDate . "' AND '" . $endDate . "'";

            if ($tp != null) {
                $sql = $sql . " AND d.tp = $tp AND m.tp = d.tp ";
            }
            
            if ($customer != null) {
                $sql = $sql . " AND d.customer = " . $customer ;
            }            

            $sql = $sql . "GROUP BY d.PK_id 
                           HAVING count(d.reference) > 1 
                           ORDER BY d.customer , d.reference , d.PK_id;";

            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $msgs = $stmt->fetchAll();
            foreach ($msgs as $msg) {
                if (!empty($msg['srdatetime'])) {
                    $msgdate = new \DateTime($msg['srdatetime']);
                }                
                $xvalue = ReportService::dateToXvalue($msgdate);
                $sql = "INSERT DELAYED INTO PartnerStatistics(userid,type,series,xvalue,data) VALUES ($msg[tp],'$statPrefix',$msg[customer],$xvalue,1) ON DUPLICATE KEY UPDATE data = data +1";
                $stmt = $em->getConnection()->prepare($sql);
                $stmt->execute();                
            }
        }
        
    }
    public static function updateScored($em, $doctype2) {
        set_time_limit(0);
        $statPrefix = '';
        switch ($doctype2) {
            case 810:
                $statPrefix2 = 'po-inv-missing';
                break;
            case 850: 
                $statPrefix = 'po-duplicate-send';
                break;               
            case 855:
                $statPrefix = 'po-ack-pastdue';
                $statPrefix2 = 'po-ack-missing';
                break;
            case 856:
                $statPrefix = 'po-asn-cancel';
                $statPrefix2 = 'po-asn-missing';
                break;                
        }

        $sql = "SELECT *, cast(xvalue as unsigned) as xv FROM PartnerStatistics WHERE type IN('$statPrefix','$statPrefix2') ORDER BY xv DESC;";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $sql = "UPDATE LOW_PRIORITY PartnerStatistics SET xvalue=:xvalue+1 WHERE userid=:userid and type=:type and series=:series and xvalue=:xvalue and data=:data;";
        $stmt = $em->getConnection()->prepare($sql);
        foreach ($result as $row) {    
            $stmt->execute($row);
        }
        
        $startDate = new \DateTime();
        $startDate->setTime(0, 0, 0);

        $endDate = new \DateTime();
        $endDate->add(new \DateInterval('P1D'));
        $endDate->setTime(23, 59, 59);

        return self::generateScored($em, $doctype2, $startDate->format('Y-m-d'), $endDate->format('Y-m-d'));
    }

    /**
     * 
     * @param type $em
     * @param type $doctype2
     * @param type $tp is the logged User (Hub)
     * @param type $cust Is the Vendor/Supplier
     * @return int
     */
    public static function deleteScored($em, $doctype2, $tp = null, $cust = null) {
        set_time_limit(0);
        $statPrefix = '';
        switch ($doctype2) {
            case 810:
                $statPrefix2 = 'po-inv-missing';
                break;
            case 850: 
                $statPrefix = 'po-duplicate-send';
                break;               
            case 855:
                $statPrefix = 'po-ack-pastdue';
                $statPrefix2 = 'po-ack-missing';
                break;
            case 856:
                $statPrefix = 'po-asn-cancel';
                $statPrefix2 = 'po-asn-missing';
                break;   
            default:
                return 550;
        }

        $sql = "DELETE FROM PartnerStatistics WHERE type IN('$statPrefix','$statPrefix2') ";
        if ($tp != null) {
            $sql = $sql . " AND userid=$tp ";
        }
        if ($cust != null) {
            $sql = $sql . " AND series=$cust ";
        }
        $sql = $sql . ";";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        return 250;
    }    

}