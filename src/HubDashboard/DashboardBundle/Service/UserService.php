<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace HubDashboard\DashboardBundle\Service;

/**
 * Description of UserService
 *
 * @author maristigueta
 */
class UserService {

    public static function getValidHubDashboardUser($em, $sid) {
        $user = '-';
        $sql = "SELECT partner_id FROM Session WHERE session_id = '$sid'";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll();
        
        foreach ($results as $result) {
            $partnerId = $result['partner_id'];
            
            //check hubdashboard config for access
            $confsql = "SELECT * FROM PartnerConfig WHERE partnerid = '$partnerId' && config = 'hubdashboard'";
            $stmt = $em->getConnection()->prepare($confsql);
            $stmt->execute();
            $confresult = $stmt->fetch();
            
            //get user detailss
            $sql = "SELECT username FROM Users WHERE partner_id = '$partnerId'";
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $userresult = $stmt->fetch();

            if($confresult && $confresult['value'] == 1) {
                $user = 'beta'.$userresult['username']; //User in HubDashboard
            } else {
                $user = '-';
            }
        }
        
        if ($user == '-')
            return NULL;
        
        return $user;
    }
    
    public static function isWebEdiSessionIsActive($em, $sid) {
        $user = '-';
        $sql = "SELECT partner_id FROM Session WHERE session_id = '$sid'";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll();
        
        if ($results)
            return true;
        else
            return false;        
    }    
}
