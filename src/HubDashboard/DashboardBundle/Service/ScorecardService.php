<?php

namespace HubDashboard\DashboardBundle\Service;
use Symfony\Component\HttpFoundation\Request;

use Doctrine\ORM\EntityManager;


class ScorecardService {
    
    public function __construct(EntityManager $em, $userID, $dateFrom = null, $dateTo = null) {
        $this->em = $em;
        $this->userID = $userID;
        if (!($dateFrom instanceof \DateTime)) {
            if (empty($dateFrom)) {
                $dateFrom = new \DateTime();
                $dateFrom->sub(new \DateInterval('P1M'));
            } else {
                $dateFrom = new \DateTime($dateFrom);
            }
        }
        if (!($dateTo instanceof \DateTime)) {
            if (empty($dateTo)) {
                $dateTo = new \DateTime();
            } else {
                $dateTo = new \DateTime($dateTo);
            }
        }
        $this->start = self::dateToXvalue($dateFrom);
        $this->end = self::dateToXvalue($dateTo);
    }
    
    public static function dateToXvalue($date) {
        $today = new \DateTime();
        $today->setTime(0, 0, 0);
        $date->setTime(0, 0, 0);
        return (int) $date->format('Ymd');
    }    
    
    public function getReport($report, $subreport, $serieID, $parentSerieID = null) {
        switch ($subreport) {
            case 'sup':
                $report = $this->getScoredcardSuppliers($serieID);
                break;
            case 'pbr':
                $report = $this->getProcessByResults($serieID);
                break;       
            case 'scp':
                $report = $this->getSyntaxCompliance($serieID);
                break;    
            case 'pcp':
                $report = $this->getProcessCompliance($serieID);
                break;      
            case 'sta':
                $report = $this->getStatistics($serieID);
                break;               
            default:
                $report = array('status' => 'Invalid report.');
                break;
        }
        return $report;
    }
    
    public function getScoredcardSuppliers($tp) {
        $request = Request::createFromGlobals();  
        //Find Customer´s TP
        $sql = "SELECT DISTINCT p.name, a.tp FROM PartnerAccess a INNER JOIN Partners p ON a.tp = p.PK_id "
             . "WHERE a.customer = :userid AND p.status = 'Active' AND p.`type` IN "
             . "('WebEDI','TP','Hosted','Software','Van','HUB') ";
        if ($tp > 0) {
            $sql = $sql . " AND a.tp = :tp";   
        }
        $stmt = $this->em->getConnection()->prepare($sql);        
        $stmt->bindValue('userid', $this->userID);
        if ($tp > 0) {
            $stmt->bindValue('tp', $tp);
        }
        $sql = $sql . " ORDER BY `name` ;";                
        $stmt->execute();        
        $suppliers = $stmt->fetchAll();
        $series = array();        
        //Find Values
        $sql = "SELECT 
                    SUM(IF(ps.`type` IN('po-inv-total', 'po-asn-total', 'po-ack-total'), ps.data , 0 )) docs, 
                    SUM(IF(ps.`type` IN('po-inv-total-err', 'po-asn-total-err', 'po-ack-total-err'),ps.data,0)) errs,
                    ps.series as tp 
                    FROM PartnerStatistics ps 
                   WHERE ps.userid = :userid
                     AND xvalue BETWEEN :start AND :end ";
        if ($tp > 0) {
            $sql = $sql . " AND ps.series = :tp ";
        }
        $sql = $sql . " GROUP BY ps.series;";           
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('userid', $this->userID);
        $stmt->bindValue('start', $this->start);
        $stmt->bindValue('end', $this->end);
        if ($tp > 0) {
            $stmt->bindValue('tp', $tp);        
        }
        $stmt->execute();
        $messages = $stmt->fetchAll();  
        $messages_mapped=array();
        foreach($messages as $msg) {
            $messages_mapped[$msg['tp']]=$msg;
        }
        foreach ($suppliers as $supplier) {
            $score = -1;
            if(isset($messages_mapped[$supplier['tp']])){
                $stats=$messages_mapped[$supplier['tp']];
                $sc=0;
                if($stats['docs'] >0 ) {
                    $sc = round(100 - ($stats['errs'] * 100 / $stats['docs']),2); 
                }
                $score = ($sc > 0) ? ceil($sc) : 0;  
            }
            
            if($score < 0) {
                continue;
            }
            
            $data = array($supplier['name'],$score,$supplier['tp']);
            array_push($series, $data);              
        }
        $json_data = array(
            "draw"            => intval($request->query->get('draw')),
            "recordsTotal"    => intval(sizeof($suppliers)),
            "recordsFiltered" => intval(sizeof($suppliers)),
            "data"            => $series
        );
        return $json_data;
    }    
    
    /**
     * 
     * @param int $tp Trading PArtner ID or minus one (-1) for all
     * @return array
     */
    public function getProcessByResults($tp) {        
        $results = array();
        if($this->userID){
            $sql = "SELECT count(m2.PK_id) docs,  DATE_FORMAT(m2.srdatetime,'%m') mont, m2.doctype 
                    FROM Messages m2 WHERE m2.customer=:userid 
                    AND m2.srdatetime BETWEEN DATE_SUB(NOW(), INTERVAL 90 DAY) 
                    AND DATE_SUB(NOW(), INTERVAL 0 DAY) AND m2.doctype in(855,810,856) ";
            if ($tp > 0) $sql = $sql . " AND m2.tp = :tp ";
            $sql = $sql . " GROUP BY m2.tp;";                
            
            $stmt = $this->em->getConnection()->prepare($sql);
            $stmt->bindValue('userid', $this->userID);
            if ($tp > 0) { // Filter TP
                $stmt->bindValue('tp', $tp);
            }
            $stmt->execute();
            $result = $stmt->fetchAll();
            if(sizeof($result) > 0){
                $data = array();
                foreach ($result as $row) {
                    $month = (substr($row['mont'],1,1) != "0") ? substr($row['mont'],1,1) : $row['mont'];
                    $data[$month][$row['doctype']] = (int)$row['docs'];
                }
                //Find statistics
                $sql = "SELECT ps.*, DATE_FORMAT(STR_TO_DATE(ps.xvalue,'%Y%m%d'),'%m') mont
                          FROM PartnerStatistics ps
                         WHERE ps.type IN ('po-asn-duplicate','po-inv-duplicate','po-ack-duplicate',
                                           'po-inv-missing','po-ack-missing','po-ack-pastdue',
                                           'po-asn-discr-shipnot','po-asn-cancel','po-asn-discr-shiplate')
                           AND ps.xvalue BETWEEN DATE_SUB(CURDATE(), INTERVAL 90 DAY) AND  DATE_SUB(CURDATE(), INTERVAL 0 DAY)
                           AND ps.userid = :userid ";       
                    if ($tp > 0) {
                        $sql = $sql . " AND ps.series = :tp ";
                    }

                    //$sql = $sql . " ORDER BY type, xvalue;";        
                    $sql = $sql . ";";        

                //echo $sql;exit();
                $stmt = $this->em->getConnection()->prepare($sql);   
                $stmt->bindValue('userid', $this->userID);        
                if ($tp > 0) $stmt->bindValue('tp', $tp);      
                $stmt->execute();
                $result = $stmt->fetchAll();

                $shipLate = array();
                $ackPasDue = array();
                $shipAfterCancel = array();

                $respMissingInv = array();
                $respMissingAck = array();
                $respMissingAsn = array();

                $duplicateSendInv = array();
                $duplicateSendAck= array();
                $duplicateSendAsn = array();

                $results = array();
                $customer = null;

                foreach ($result as $row) {

                    $customer = $row['userid'];

                    $month = (substr($row['mont'],1,1) != "0") ? substr($row['mont'],1,1) : $row['mont'];

                    switch ($row['type']) {

                        case 'po-inv-missing':
                            if(array_key_exists($month, $respMissingInv)) 
                                $respMissingInv[$month] =  $respMissingInv[$month] +  (int)$row['data'];
                            else
                                $respMissingInv[$month] = (int)$row['data'];
                            break;
                        case 'po-ack-missing':
                            if(array_key_exists($month, $respMissingAck)) 
                                $respMissingAck[$month] =  $respMissingAck[$month] +  (int)$row['data'];
                            else
                                $respMissingAck[$month] = (int)$row['data'];
                            break;
                        case 'po-asn-discr-shipnot':
                            if(array_key_exists($month, $respMissingAsn)) 
                                $respMissingAsn[$month] =  $respMissingAsn[$month] +  (int)$row['data'];
                            else
                                $respMissingAsn[$month] = (int)$row['data'];
                            break;                        

                        case 'po-ack-duplicate':
                            if(array_key_exists($month, $duplicateSendAck)) 
                                $duplicateSendAck[$month] =  $duplicateSendAck[$month] +  (int)$row['data'];
                            else
                                $duplicateSendAck[$month] = (int)$row['data'];                  
                            break;
                        case 'po-asn-duplicate':
                            if(array_key_exists($month, $duplicateSendAsn)) 
                                $duplicateSendAsn[$month] =  $duplicateSendAsn[$month] +  (int)$row['data'];
                            else
                                $duplicateSendAsn[$month] = (int)$row['data'];                  
                            break;

                        case 'po-inv-duplicate':
                            if(array_key_exists($month, $duplicateSendInv)) 
                                $duplicateSendInv[$month] =  $duplicateSendInv[$month] +  (int)$row['data'];
                            else
                                $duplicateSendInv[$month] = (int)$row['data'];                  
                            break; 

                        case 'po-ack-pastdue':
                            if(array_key_exists($month, $ackPasDue)) 
                                $ackPasDue[$month] =  $ackPasDue[$month] +  (int)$row['data'];
                            else
                                $ackPasDue[$month] = (int)$row['data'];       
                            break;

                        case 'po-asn-cancel':
                            if(array_key_exists($month, $shipAfterCancel)) 
                                $shipAfterCancel[$month] =  $shipAfterCancel[$month] +  (int)$row['data'];
                            else
                                $shipAfterCancel[$month] = (int)$row['data'];           
                            break; 

                        case 'po-asn-discr-shiplate':
                            if(array_key_exists($month, $shipLate)) 
                                $shipLate[$month] =  $shipLate[$month] +  (int)$row['data'];
                            else
                                $shipLate[$month] = (int)$row['data'];                 
                            break;
                    }            
                }
                $i = 1;
                while ($i < date('n')+1){
                    if(array_key_exists($i,$data)){
                        if(sizeof($data[$i]) < 4){
                            if(!array_key_exists(810, $data[$i])){
                                $data[$i][810] = 0;                    
                            }
                            if(!array_key_exists(850, $data[$i])){
                                $data[$i][850] = 0;                    
                            }
                            if(!array_key_exists(855, $data[$i])){
                                $data[$i][855] = 0;                    
                            }
                            if(!array_key_exists(856, $data[$i])){
                                $data[$i][856] = 0;                    
                            }
                        }
                    }
                    $i++;
                }

                $series = $this->completeArray($data,1);
                //var_dump($series);
    //            exit();
                $results['shipLate'] = $this->completeArray($shipLate);
                $results['ackPasDue'] = $this->completeArray($ackPasDue);            
                $results['shipAfterCancel'] = $this->completeArray($shipAfterCancel);
                $ddAck = $this->completeArray($duplicateSendAck);
                $ddAsn = $this->completeArray($duplicateSendAsn);            
                $ddInv = $this->completeArray($duplicateSendInv);
                //echo 'duplicateSend por doc';
                //var_dump($ddInv);echo '<br>';var_dump($ddAck);echo '<br>';var_dump($ddAsn);echo '<br><br>';
                $rmAck = $this->completeArray($respMissingAck);
                $rmAsn = $this->completeArray($respMissingAsn);            
                $rmInv = $this->completeArray($respMissingInv);
    //            var_dump($rmAck);echo '<br>';var_dump($rmAsn);echo '<br>';var_dump($rmInv);echo '<br><br>';

                //Array details by month by statistic to drilldown graffic
                $ddshipLate = array();
                foreach ($results['shipLate'] as $key => $value) {
                    //$ddshipLate[$key]['850'] = $series[$key][850]; 
                    $ddshipLate[$key]['856 Er'] = $value;
                    $ddshipLate[$key]['856'] = (isset($series[$key][856]))?$series[$key][856]:0;                
                    $ddshipLate[$key]['856 Ok'] = $ddshipLate[$key]['856'] - $value;
                }

                $ddshipAfterCancel = array();
                foreach ($results['shipAfterCancel'] as $key => $value) {
                    //$ddshipAfterCancel[$key]['850'] = $series[$key][850];
                    $ddshipAfterCancel[$key]['856 Er'] = $value;
                    $ddshipAfterCancel[$key]['856'] = (isset($series[$key][856]))?$series[$key][856]:0; 
                    $ddshipAfterCancel[$key]['856 Ok'] = $ddshipAfterCancel[$key]['856'] - $value;                                
                }

                $ddackPasDue = array();
                foreach ($results['ackPasDue'] as $key => $value) {
                    //$ddackPasDue[$key]['850'] = $series[$key][850];    
                    $ddackPasDue[$key]['855 Er'] = $value;                
                    $ddackPasDue[$key]['855'] = (isset($series[$key][855]))?$series[$key][855]:0; 
                    $ddackPasDue[$key]['855 Ok'] = $ddackPasDue[$key]['855'] - $value;                                
                }

                $ddduplicateSend = array();
                foreach ($ddAck as $key => $value) {
                    //$ddduplicateSend[$key][810] = 9620;
                    $ddduplicateSend[$key][810] = $ddInv[$key];
                    $ddduplicateSend[$key][855] = $ddAck[$key];                
                    $ddduplicateSend[$key][856] = $ddAsn[$key];
                    $results['duplicateSend'][$key] = $ddInv[$key] + $ddAck[$key] + $ddAsn[$key];
                }
                //echo 'duplicateSend details';
                //var_dump($ddduplicateSend);echo '<br>';
                //echo 'duplicateSend result';
                //svar_dump($results['duplicateSend']);echo '<br><br>';

                $ddrespMissing = array();
                foreach ($rmAsn as $key => $value) {
                    //$ddrespMissing[$key]['850'] = $rmInv[$key];
                    $ddrespMissing[$key][810] = $rmInv[$key];
                    $ddrespMissing[$key][855] = $rmAck[$key];                
                    $ddrespMissing[$key][856] = $rmAsn[$key]; 
                    $results['respMissing'][$key] = $rmInv[$key] + $rmAck[$key] + $rmAsn[$key];
                }
    //            var_dump($ddrespMissing);echo '<br>';
    //            var_dump($results['respMissing']);echo '<br><br>';

                $mons = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 
                              6 => "Jun", 7 => "Jul", 8 => "Aug", 9 => "Sep", 10 => "Oct", 
                              11 => "Nov", 12 => "Dec");

                //Array results all series and drilldrowns
                foreach ($ddackPasDue as $key => $val){
                    $results[$mons[$key+1]]['ddackPasDue'] = $val;
                }

                foreach ($ddshipAfterCancel as $key => $val){
                    $results[$mons[$key+1]]['ddshipAfterCancel'] = $val;
                }

                foreach ($ddshipLate as $key => $val){
                    $results[$mons[$key+1]]['ddshipLate'] = $val;
                }

                foreach ($ddduplicateSend as $key => $val){
                    $results[$mons[$key+1]]['ddduplicateSend'] = $val;
                }

                foreach ($ddrespMissing as $key => $val){
                    $results[$mons[$key+1]]['ddrespMissing'] = $val;
                }
            }
        }
            
        //print_r($results);  exit();
        return $results;
    } 
 
    public function getProcessCompliance($tp) {
        // Process, are quantity of 850 sents by customer
        $sql = "SELECT sum(`data`) pos FROM PartnerStatistics ps 
                WHERE ps.userid = :userid AND xvalue BETWEEN :start AND :end 
                AND ps.`type` = 'po-total' ";
        if ($tp > 0) $sql = $sql . " AND series = :tp ";
        $sql = $sql .";";        
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('userid', $this->userID);
        $stmt->bindValue('start', $this->start);
        $stmt->bindValue('end', $this->end);
        if ($tp > 0) $stmt->bindValue('tp', $tp);
        $stmt->execute();
        $rs = $stmt->fetchAll();
        $pos = $rs[0]['pos'];     
   
        $sql = "SELECT sum(`data`) tdocs, ps.`type` as doctype FROM PartnerStatistics ps 
                WHERE ps.userid = :userid AND xvalue BETWEEN :start AND :end AND ps.`type` 
                IN('po-inv-total', 'po-asn-total', 'po-ack-total') ";
        if ($tp > 0) $sql = $sql . " AND ps.series = :tp ";
        $sql = $sql . " GROUP BY ps.`type` ";        
        
        //echo $sql." ".$this->start." ".$this->end." ".$this->userID ."<br>";

        $stmt = $this->em->getConnection()->prepare($sql);        
        $stmt->bindValue('userid', $this->userID);
        $stmt->bindValue('start', $this->start);
        $stmt->bindValue('end', $this->end);
        if ($tp > 0) $stmt->bindValue('tp', $tp);
        $stmt->execute();
        $result = $stmt->fetchAll();      
        
        $totalAck = 0;
        $totalAsn = 0;
        $totalInv = 0;

        foreach ($result as $row) {
            $totalAck = $row['doctype'] == 'po-ack-total' ? $row['tdocs'] : $totalAck;
            $totalAsn = $row['doctype'] == 'po-asn-total' ? $row['tdocs'] : $totalAsn;
            $totalInv = $row['doctype'] == 'po-inv-total' ? $row['tdocs'] : $totalInv;
        }  
        
        $tdprocs = $totalAck + $totalAsn + $totalInv;

        $warning = 0;
        $warningAsn = 0;
        $warningAck = 0;
        $warningInv = 0;
        
        $error = 0;
        $errorAsn = 0;
        $errorAck = 0;      
        $errorInv = 0;
        
        $success = 0;        
        $successAsn = 0;
        $successPo = 0;
        $successAck = 0;       
        $successInv = 0;        
        
        if($tdprocs){
            $sql = "SELECT type, sum(`data`) sdata FROM PartnerStatistics 
                     WHERE type IN ('po-asn-duplicate','po-inv-duplicate','po-ack-duplicate',
                                    'po-asn-discr-shipnot','po-inv-missing','po-ack-missing',
                                    'po-ack-pastdue','po-asn-cancel','po-asn-discr-shiplate') 
                       AND xvalue BETWEEN :start AND :end AND userid = :userid ";
            if ($tp > 0)  $sql = $sql . " AND series = :tp ";

            $sql = $sql . " GROUP BY `type`;"; 
            
            //echo $sql." ".$this->start." ".$this->end." ".$customer ."\n";
            //die();

            $stmt = $this->em->getConnection()->prepare($sql);
            $stmt->bindValue('end', $this->end);
            $stmt->bindValue('start', $this->start);
            $stmt->bindValue('userid', $this->userID);
            if ($tp > 0) $stmt->bindValue('tp', $tp);
            $stmt->execute();   
            $result = $stmt->fetchAll();
            foreach ($result as $row) {
                
                switch ($row['type']) {
                    case 'po-inv-missing':
                        $errorInv = $errorInv + $row['sdata'];
                    break;
                    case 'po-ack-missing':
                        $errorAck = $errorAck + $row['sdata'];
                    break;
                    case 'po-asn-discr-shipnot':
                        $errorAsn = $errorAsn + $row['sdata'];
                    break;   
                    case 'po-asn-cancel':
                        $errorAsn = $errorAsn + $row['sdata'];
                    break;
                    case 'po-asn-discr-shiplate':
                        $warningAsn = $warningAsn + $row['sdata'];
                    break;
                    case 'po-ack-pastdue':
                        $warningAck = $warningAck + $row['sdata'];
                    break;                      
                    case 'po-asn-duplicate':
                        $errorAsn= $errorAsn + $row['sdata'];
                    break;
                    case 'po-ack-duplicate':
                        $errorAck = $errorAck + $row['sdata'];
                    break;
                    case 'po-inv-duplicate':
                        $errorInv = $errorInv + $row['sdata'];                        
                    break;                          
                }
            }

            $successAsn = ($totalAsn - $warningAsn - $errorAsn) < 0 ? 0 : $totalAsn - $warningAsn - $errorAsn; 
            $successAck = ($totalAck - $warningAck - $errorAck) < 0 ? 0 : $totalAck - $warningAck - $errorAck; 
            $successPo  = 0;
            $successInv = ($totalInv - $warningInv - $errorInv) < 0 ? 0 : $totalInv - $warningInv - $errorInv; 
            
            $warning = $warningAsn + $warningAck + $warningInv;
            $error = $errorAsn + $errorAck + $errorInv;
            
            if($error > $tdprocs) $error = $tdprocs;
            if($warning > $tdprocs) $warning = $tdprocs;

            //echo $tprocs .' '.$warning.' '.$error;
            $success = $tdprocs - $warning - $error; 
            
            //Calculate 
            $warnPerc = $tdprocs ? $warning/$tdprocs : 0;
            $errorPerc = $tdprocs ? $error/$tdprocs : 0;
            $successPerc = $tdprocs ? $success/$tdprocs : 0;
            
            $procSuccess = ceil($pos * $successPerc);
            $procWarning = ceil($pos * $warnPerc);
            $procError = ceil($pos * $errorPerc);
            
            $total = $procSuccess  + $procWarning + $procError;
            //echo $procSuccess .' '.$procWarning.' '.$procError;
            $rest = $pos - $total;
            if($rest < 0 ){                
                if($procSuccess > abs($rest))
                    $procSuccess = $procSuccess + $rest;
                elseif($procWarning > abs($rest))
                    $procWarning = $procWarning + $rest;
                elseif($procError > abs($rest))
                    $procError = $procError + $rest;
            }else{
                if($procSuccess < abs($rest))
                    $procSuccess = $procSuccess + $rest;
                elseif($procWarning < abs($rest))
                    $procWarning = $procWarning + $rest;
                elseif($procError < abs($rest))
                    $procError = $procError + $rest;
            }

        }else{
            $procSuccess = 0;
            $procWarning = 0;
            $procError = 0;            
        }
            
        
        $series = array();
        $drilldown = array();
        $serie = array();
        
        $ssuccess = array(
            'name' => 'Success',
            'y' => $procSuccess,
            'drilldown' => 'success'
        );
        
        $swarning = array(
            'name' => 'Warning',
            'y' => $procWarning,
            'drilldown' => 'warning'
        );
        
        $serror = array(
            'name' => 'Error',
            'y' => $procError,
            'drilldown' => 'error'
        );  

        $dderror = array(
            'errorPo'  => 0,
            'errorInv' => $errorInv,
            'errorAck' => $errorAck,  
            'errorAsn' => $errorAsn          
        );      
        
        $ddwarning = array(
            'warningPo'  => 0,
            'warningInv' => $warningInv,
            'warningAck' => $warningAck,  
            'warningAsn' => $warningAsn       
        ); 
        
        
        $ddsuccess = array(
            'successPo'  => $successPo,
            'successInv' => $successInv,
            'successAck' => $successAck,  
            'successAsn' => $successAsn       
        );   

        array_push($drilldown, $ddsuccess);
        array_push($drilldown, $ddwarning);
        array_push($drilldown, $dderror);        

        array_push($serie, $ssuccess);
        array_push($serie, $swarning);
        array_push($serie, $serror);
        
        $series['drilldown'] = $drilldown;
        $series['series'] = $serie;
        
        return $series; 
    }    
    
    public function getSyntaxCompliance($tp) {
     
//        $sql = "SELECT count(m2.PK_id) tdocs, m1.tp, m1.customer, m1.PK_id msg, m1.srdatetime,
//                m2.PK_id msga, m2.srdatetime as datedoc2, m2.doctype doctype FROM Messages m1 
//                INNER JOIN MessageReferences r ON m1.reference=r.reference INNER JOIN Messages m2 ON 
//                r.FK_message=m2.PK_id WHERE m1.customer=:userid and m1.doctype=850 AND m1.srdatetime 
//                BETWEEN DATE_SUB(NOW(), INTERVAL :start DAY) AND DATE_SUB(NOW(), INTERVAL :end DAY) 
//                AND m2.doctype in(855,856,810) ";
//        if ($tp > 0) {
//                $sql = $sql . " AND m1.tp = :tp ";
//        }
//
//        $sql = $sql . " GROUP BY m2.doctype;"; 
        $sql = "SELECT sum(`data`) tdocs, ps.`type` as doctype FROM PartnerStatistics ps 
                WHERE ps.userid = :userid AND xvalue BETWEEN :start AND :end AND ps.`type` 
                IN('po-inv-total', 'po-asn-total', 'po-ack-total') ";
        if ($tp > 0) $sql = $sql . " AND ps.series = :tp ";
        $sql = $sql . " GROUP BY ps.`type` ";        
        
        //echo $sql." ".$this->start." ".$this->end." ".$this->userID;

        $stmt = $this->em->getConnection()->prepare($sql);        
        $stmt->bindValue('userid', $this->userID);
        $stmt->bindValue('start', $this->start);
        if ($tp > 0) $stmt->bindValue('tp', $tp);
        $stmt->bindValue('end', $this->end);
        $stmt->execute();
        $result = $stmt->fetchAll();        
        
        $totalAck = 0;
        $totalAsn = 0;
        $totalInv = 0;

        
        foreach ($result as $row) {
            $totalAck = $row['doctype'] == 'po-ack-total' ? $row['tdocs'] : $totalAck;
            $totalAsn = $row['doctype'] == 'po-asn-total' ? $row['tdocs'] : $totalAsn;
            $totalInv = $row['doctype'] == 'po-inv-total' ? $row['tdocs'] : $totalInv;
        }  
        
        $tdocs = $totalAck + $totalAsn + $totalInv;
        
        $warning = 0;
        $warningAsn = 0;
        $warningAck = 0;
        $warningPo = 0;
        $warningInv = 0;
        
        $error = 0;
        $errorAsn = 0;
        $errorAck = 0;
        $errorPo = 0;        
        $errorInv = 0;
        
        $success = 0;        
        $successAsn = 0;
        $successAck = 0;
        $successPo = 0;        
        $successInv = 0;        
                    
        if($tdocs){ //'po-asn-discr-shipnot'
            $sql = "SELECT sum(`data`) cant, `type` FROM PartnerStatistics WHERE `type` 
                    like 'po-%-sdiscr'
                    AND xvalue BETWEEN :start AND :end AND userid = :userid";
            if ($tp > 0) {
                $sql = $sql . " AND series = :tp ";
            }

            $sql = $sql . " GROUP BY `type`;";
            $stmt = $this->em->getConnection()->prepare($sql);
            if ($tp > 0) $stmt->bindValue('tp', $tp);
            $stmt->bindValue('end', $this->end);
            $stmt->bindValue('start', $this->start);
            $stmt->bindValue('userid', $this->userID);
            
            $stmt->execute();   
            $result = $stmt->fetchAll();
            //print_r();
            foreach ($result as $row) {
                if(substr_count('po-ack-sdiscr',$row['type']) > 0){
                    $errorAck = $errorAck + $row['cant'];
                }elseif(substr_count('po-asn-sdiscr',$row['type']) > 0){
                    $errorAsn = $errorAsn + $row['cant'];  
                }elseif(substr_count('po-inv-sdiscr',$row['type']) > 0){
                    $errorInv = $errorInv + $row['cant'];
                }
            }
            
            $warning = $warningAsn + $warningAck + $warningInv;
            $error = $errorAsn + $errorAck + $errorInv;
            
            $warning = $error >= $tdocs ? 0 : $warning;
            $error = $error >= $tdocs ? $tdocs : $error;
            $success = $tdocs - $warning - $error;            
            
            $successAsn = ($totalAsn - $warningAsn - $errorAsn) < 0 ? 0 : $totalAsn - $warningAsn - $errorAsn; 
            $successAck = ($totalAck - $warningAck - $errorAck) < 0 ? 0 : $totalAck - $warningAck - $errorAck; 
            $successInv = ($totalInv - $warningInv - $errorInv) < 0 ? 0 : $totalInv - $warningInv - $errorInv;               

        }
        
        $series = array();
        $drilldown = array();
        $serie = array();
        
        $ssuccess = array(
            'name' => 'Success',
            'y' => $success,
            'drilldown' => 'success'
        );
        
        $swarning = array(
            'name' => 'Warning',
            'y' => $warning,
            'drilldown' => 'warning'
        );
        
        $serror = array(
            'name' => 'Error',
            'y' => $error,
            'drilldown' => 'error'
        );  

        $dderror = array(
            'errorPo'  => $errorPo,
            'errorInv' => $errorInv,
            'errorAck' => $errorAck,  
            'errorAsn' => $errorAsn          
        );      
        
        $ddwarning = array(
            'warningPo'  => $warningPo,
            'warningInv' => $warningInv,
            'warningAck' => $warningAck,  
            'warningAsn' => $warningAsn       
        ); 
        
        
        $ddsuccess = array(
            'successPo'  => $successPo,
            'successInv' => $successInv,
            'successAck' => $successAck,  
            'successAsn' => $successAsn       
        );   

        array_push($drilldown, $ddsuccess);
        array_push($drilldown, $ddwarning);
        array_push($drilldown, $dderror);        

        array_push($serie, $ssuccess);
        array_push($serie, $swarning);
        array_push($serie, $serror);
        
        $series['drilldown'] = $drilldown;
        $series['series'] = $serie;
        
        return $series; 
    }
    
    public function getStatistics($tp) {
        //1: Documents Statics  
        $sql = "SELECT sum(`data`) tdocs, ps.`type` as doctype FROM PartnerStatistics ps 
                WHERE ps.userid = :userid AND xvalue BETWEEN :start AND :end AND ps.`type` 
                IN('po-inv-total', 'po-asn-total', 'po-ack-total') ";
        if ($tp > 0) $sql = $sql . " AND ps.series = :tp ";
        $sql = $sql . " GROUP BY ps.`type` ";
        $stmt = $this->em->getConnection()->prepare($sql);        
        $stmt->bindValue('userid', $this->userID);
        $stmt->bindValue('start', $this->start);
        $stmt->bindValue('end', $this->end);
        if ($tp > 0) $stmt->bindValue('tp', $tp);      
        $stmt->execute();
        $resultp = $stmt->fetchAll();
        
        $warning = 0;
        $error = 0;
        $success = 0;
        
        $totalAck = 0;
        $totalAsn = 0;
        $totalInv = 0;
        
        foreach ($resultp as $row) {
            $totalAck = $row['doctype'] == 'po-ack-total' ? $row['tdocs'] : $totalAck;
            $totalAsn = $row['doctype'] == 'po-asn-total' ? $row['tdocs'] : $totalAsn;
            $totalInv = $row['doctype'] == 'po-inv-total' ? $row['tdocs'] : $totalInv;
        }        
        
        $tdocs = $totalAck + $totalAsn + $totalInv;
        
        if($tdocs){
            $sql = "SELECT sum(`data`) cant, `type` FROM PartnerStatistics "
                 . "WHERE `type` IN ('po-ack-sdiscr','po-asn-sdiscr','po-inv-sdiscr') "
                 . "AND xvalue BETWEEN :start AND :end AND userid = :userid ";
            if ($tp > 0) {
                    $sql = $sql . " AND series = :tp ";
            }

            $sql = $sql . " GROUP BY `type`;";                
            //echo $sql." ".$this->start." ".$this->end." ".$customer ."\n";

            $stmt = $this->em->getConnection()->prepare($sql);
            $stmt->bindValue('userid', $this->userID);
            $stmt->bindValue('start', $this->start);
            $stmt->bindValue('end', $this->end);
            if ($tp > 0) $stmt->bindValue('tp', $tp);
            $stmt->execute();   
            $result = $stmt->fetchAll();
            
            foreach ($result as $row) {
                if(substr_count('po-asn-sdiscr, po-inv-sdiscr, po-ack-sdiscr', $row['type']) > 0){
                    $error = $error+ $row['cant'];
                } 
//                else {
//                    $warning = $warning + $row['cant'];
//                }
            }
            $warning = $error >= $tdocs ? 0 : $warning;
            $error = $error >= $tdocs ? $tdocs : $error;
            $success = $tdocs - $warning - $error;
        }
        
        $msuccess = $tdocs ? round(($success * 100)/$tdocs,2) : 0;
        $mwarning = $tdocs ? round(($warning * 100)/$tdocs ,2) : 0;
        $merror = $tdocs ? round(($error * 100)/$tdocs,2) : 0;

        if($msuccess < 0) $msuccess = 0; elseif ($msuccess > 100) $msuccess = 100;
        if($mwarning < 0) $mwarning = 0; elseif ($mwarning > 100) $mwarning = 100;
        if($merror < 0)   $merror = 0;   elseif ($merror > 100) $merror = 100;   
        
        $documents = array(
            'total' => $tdocs,
            'success' => $tdocs ? $msuccess . "%" : 0,
            'warnings' => $tdocs ? $mwarning . "%": 0,
            'errors' => $tdocs ? $merror . "%": 0
        ); 
        
        // 2: Process, are quantity of 850 sents by customer
        $sql = "SELECT sum(`data`) pos FROM PartnerStatistics ps 
                WHERE ps.userid = :userid AND xvalue BETWEEN :start AND :end 
                AND ps.`type` = 'po-total' ";
        if ($tp > 0) $sql = $sql . " AND series = :tp ";
        $sql = $sql .";";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('userid', $this->userID);
        $stmt->bindValue('start', $this->start);
        $stmt->bindValue('end', $this->end);
        if ($tp > 0) $stmt->bindValue('tp', $tp);
//        echo $sql.' '.$this->userID.' '.$this->start.' '.$this->end;
        $stmt->execute();
        $rs = $stmt->fetchAll();
        $pos = (int)$rs[0]['pos'];

        $dwarning = 0;
        $derror = 0;
        $dsuccess = 0;
        
        $dtotalAck = 0;
        $dtotalAsn = 0;
        $dtotalInv = 0;
        
        foreach ($resultp as $row) {
            $dtotalAck = $row['doctype'] == 'po-ack-total' ? $row['tdocs'] : $dtotalAck;
            $dtotalAsn = $row['doctype'] == 'po-asn-total' ? $row['tdocs'] : $dtotalAsn;
            $dtotalInv = $row['doctype'] == 'po-inv-total' ? $row['tdocs'] : $dtotalInv;
        }        
        
        $tdprocs = $dtotalAck + $dtotalAsn + $dtotalInv;
        
        if($tdprocs){
            $sql = "SELECT type, sum(`data`) sdata  FROM PartnerStatistics "
                 . "WHERE type IN ('po-asn-duplicate','po-inv-duplicate','po-ack-duplicate',
                                   'po-asn-discr-shipnot','po-inv-missing','po-ack-missing',
                                   'po-ack-pastdue','po-asn-cancel','po-asn-discr-shiplate') "
                 . "AND xvalue BETWEEN :start AND :end AND userid = :userid";
            if ($tp > 0) {
                    $sql = $sql . " AND series = :tp ";
            }

            $sql = $sql . " GROUP BY `type`;";                
            //echo $sql." ".$this->start." ".$this->end." ".$customer ."\n";

            $stmt = $this->em->getConnection()->prepare($sql);
            $stmt->bindValue('userid', $this->userID);
            $stmt->bindValue('end', $this->end);
            $stmt->bindValue('start', $this->start);
            if ($tp > 0) $stmt->bindValue('tp', $tp);
            $stmt->execute();   
            $result = $stmt->fetchAll();
            foreach ($result as $row) {
                if($row['type'] == 'po-ack-pastdue' || $row['type'] == 'po-asn-discr-shiplate'){
                    $dwarning = $dwarning + $row['sdata'];
                } else {
                    $derror = $derror + $row['sdata'];
                }
            }
            $dsuccess = $tdprocs - $dwarning - $derror;
            $psuccess = $tdprocs ? round(($dsuccess * 100)/$tdprocs,2) : 0;
            $pwarning = $tdprocs ? round(($dwarning * 100)/$tdprocs ,2) : 0;
            $perror = $tdprocs ? round(($derror * 100)/$tdprocs,2) : 0;
            
            //Controla que existan mas errores que documentos
            if($psuccess < 0) $psuccess = 0; elseif ($psuccess > 100) $psuccess = 100;
            if($pwarning < 0) $pwarning = 0; elseif ($pwarning > 100) $pwarning = 100;
            if($perror < 0)  $perror = 0; elseif ($perror > 100) $perror = 100;            
        } else
            $pos = 0;
        $processes = array(
            'total' => $pos,
            'success' => $tdprocs ? $psuccess . "%" : 0,
            'warnings' => $tdprocs ? $pwarning . "%": 0,
            'errors' => $tdprocs ? $perror . "%": 0
        );
        $series = array();
        array_push($series, $documents);
        array_push($series, $processes);
        
        return $series;
        
    }
    
    
    public function completeArray ($array,$multiple = null){
        
        $i = 1;
        
        while ($i < 13){
            
            if(!array_key_exists ($i, $array)){
                if($multiple){
                    $array[$i][810] = 0;
                    $array[$i][850] = 0;
                    $array[$i][855] = 0;
                    $array[$i][856] = 0;
                }else{
                    $array[$i] = 0;
                }
            }
            $i++;
        }
        
        ksort($array);
        $serie = array();

        foreach ($array as $key => $val)
            $serie[$key] = $val;

        return (array_values($serie));
    
    }
    //DataTable ListSuplliers
}