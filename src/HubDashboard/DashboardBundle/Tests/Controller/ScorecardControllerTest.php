<?php

namespace HubDashboard\DashboardBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ScorecardControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/index');
    }

    public function testSuppliers()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/suppliers');
    }

    public function testStats()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/stats');
    }

    public function testProcessresults()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/processResults');
    }

    public function testSyntaxcompliance()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/syntaxCompliance');
    }

    public function testProcesscompliance()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/processCompliance');
    }

}
