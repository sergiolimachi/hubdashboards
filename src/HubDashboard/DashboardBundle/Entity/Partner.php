<?php

namespace HubDashboard\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;



/**
 * Partner
 *
 * @ORM\Table(name="Partners")
 * @ORM\Entity
 */
class Partner implements UserInterface, \Serializable
{
    /**
     * @var string
     *
     * @ORM\Column(name="webtpid", type="string", length=50, nullable=false)
     */
    private $webtpid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address1", type="string", length=255, nullable=true)
     */
    private $address1;

    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=255, nullable=true)
     */
    private $address2;

    /**
     * @var string
     *
     * @ORM\Column(name="address3", type="string", length=255, nullable=true)
     */
    private $address3;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=45, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=10, nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=25, nullable=true)
     */
    private $zip;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=50, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="status_note", type="string", length=255, nullable=true)
     */
    private $statusNote;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="joined", type="datetime", nullable=false)
     */
    private $joined;

    /**
     * @var string
     *
     * @ORM\Column(name="salesperson", type="string", length=45, nullable=true)
     */
    private $salesperson;

    /**
     * @var boolean
     *
     * @ORM\Column(name="demo", type="boolean", nullable=true)
     */
    private $demo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="contactdate", type="datetime", nullable=false)
     */
    private $contactdate;

    /**
     * @var string
     *
     * @ORM\Column(name="softwareversion", type="string", length=45, nullable=false)
     */
    private $softwareversion;

    /**
     * @var string
     *
     * @ORM\Column(name="industry", type="string", length=45, nullable=false)
     */
    private $industry;

    /**
     * @var boolean
     *
     * @ORM\Column(name="showonwebsite", type="boolean", nullable=false)
     */
    private $showonwebsite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cancel_date", type="date", nullable=false)
     */
    private $cancelDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="PK_id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pkId;



    /**
     * Set webtpid
     *
     * @param string $webtpid
     * @return Partner
     */
    public function setWebtpid($webtpid)
    {
        $this->webtpid = $webtpid;
    
        return $this;
    }

    /**
     * Get webtpid
     *
     * @return string 
     */
    public function getWebtpid()
    {
        return $this->webtpid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Partner
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address1
     *
     * @param string $address1
     * @return Partner
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
    
        return $this;
    }

    /**
     * Get address1
     *
     * @return string 
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     * @return Partner
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
    
        return $this;
    }

    /**
     * Get address2
     *
     * @return string 
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set address3
     *
     * @param string $address3
     * @return Partner
     */
    public function setAddress3($address3)
    {
        $this->address3 = $address3;
    
        return $this;
    }

    /**
     * Get address3
     *
     * @return string 
     */
    public function getAddress3()
    {
        return $this->address3;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Partner
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Partner
     */
    public function setState($state)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return Partner
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    
        return $this;
    }

    /**
     * Get zip
     *
     * @return string 
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Partner
     */
    public function setCountry($country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Partner
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Partner
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set statusNote
     *
     * @param string $statusNote
     * @return Partner
     */
    public function setStatusNote($statusNote)
    {
        $this->statusNote = $statusNote;
    
        return $this;
    }

    /**
     * Get statusNote
     *
     * @return string 
     */
    public function getStatusNote()
    {
        return $this->statusNote;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Partner
     */
    public function setNote($note)
    {
        $this->note = $note;
    
        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set joined
     *
     * @param \DateTime $joined
     * @return Partner
     */
    public function setJoined($joined)
    {
        $this->joined = $joined;
    
        return $this;
    }

    /**
     * Get joined
     *
     * @return \DateTime 
     */
    public function getJoined()
    {
        return $this->joined;
    }

    /**
     * Set salesperson
     *
     * @param string $salesperson
     * @return Partner
     */
    public function setSalesperson($salesperson)
    {
        $this->salesperson = $salesperson;
    
        return $this;
    }

    /**
     * Get salesperson
     *
     * @return string 
     */
    public function getSalesperson()
    {
        return $this->salesperson;
    }

    /**
     * Set demo
     *
     * @param boolean $demo
     * @return Partner
     */
    public function setDemo($demo)
    {
        $this->demo = $demo;
    
        return $this;
    }

    /**
     * Get demo
     *
     * @return boolean 
     */
    public function getDemo()
    {
        return $this->demo;
    }

    /**
     * Set contactdate
     *
     * @param \DateTime $contactdate
     * @return Partner
     */
    public function setContactdate($contactdate)
    {
        $this->contactdate = $contactdate;
    
        return $this;
    }

    /**
     * Get contactdate
     *
     * @return \DateTime 
     */
    public function getContactdate()
    {
        return $this->contactdate;
    }

    /**
     * Set softwareversion
     *
     * @param string $softwareversion
     * @return Partner
     */
    public function setSoftwareversion($softwareversion)
    {
        $this->softwareversion = $softwareversion;
    
        return $this;
    }

    /**
     * Get softwareversion
     *
     * @return string 
     */
    public function getSoftwareversion()
    {
        return $this->softwareversion;
    }

    /**
     * Set industry
     *
     * @param string $industry
     * @return Partner
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;
    
        return $this;
    }

    /**
     * Get industry
     *
     * @return string 
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * Set showonwebsite
     *
     * @param boolean $showonwebsite
     * @return Partner
     */
    public function setShowonwebsite($showonwebsite)
    {
        $this->showonwebsite = $showonwebsite;
    
        return $this;
    }

    /**
     * Get showonwebsite
     *
     * @return boolean 
     */
    public function getShowonwebsite()
    {
        return $this->showonwebsite;
    }

    /**
     * Set cancelDate
     *
     * @param \DateTime $cancelDate
     * @return Partner
     */
    public function setCancelDate($cancelDate)
    {
        $this->cancelDate = $cancelDate;
    
        return $this;
    }

    /**
     * Get cancelDate
     *
     * @return \DateTime 
     */
    public function getCancelDate()
    {
        return $this->cancelDate;
    }

    /**
     * Get pkId
     *
     * @return integer 
     */
    public function getPkId()
    {
        return $this->pkId;
    }

    public function eraseCredentials() {
        
    }

    public function getPassword() {
        
    }

    public function getRoles() {
        
    }

    public function getSalt() {
        
    }

    public function getUsername() {
        
    }

    public function serialize() {
        
    }

    public function unserialize($serialized) {
        
    }

}