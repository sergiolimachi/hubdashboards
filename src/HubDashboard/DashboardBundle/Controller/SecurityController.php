<?php

namespace HubDashboard\DashboardBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use HubDashboard\DashboardBundle\Service\UserService;

class SecurityController extends Controller
{
    /**
     * @Route("/login/{sid}", name="login")
     * @Template()
     */    
    public function loginAction(Request $request, $sid = null)
    {
        $request = $this->getRequest(); 
        $session = $request->getSession();

        $em = $this->getDoctrine();
        $gm = $em->getManager();
        $hubDashboardUser = UserService::getValidHubDashboardUser($gm, $sid);

        if ($sid) { //If Hub Dashboard app was called from WebEdi app
            if($hubDashboardUser) { //If the logged WebEdi customer has a valid user in Hub Dashboard
                $providerKey = 'secured_area';
                $user  = $em->getRepository("HubDashboardBundle:User")->findOneByUsername($hubDashboardUser); //Entity Repository
                
                $token = new UsernamePasswordToken($user, $user->getPassword(), $providerKey, $user->getRoles());
                $this->get('security.context')->setToken($token);            
                $loginevent = new InteractiveLoginEvent($request, $token);
                
                $this->get("event_dispatcher")->dispatch("security.interactive_login", $loginevent);
                $session->set('_security_secured_area',serialize($token));
                $session->set('sid', $sid);
                
                return $this->redirect($this->generateUrl('homepage'));
            }
        }
        
        if (!$sid) { //If Hub Dashboard app was called out of WebEdi app
            // get the login error if there is one
            if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
                $error = $request->attributes->get(
                    SecurityContext::AUTHENTICATION_ERROR
                );
            } else {
                $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
                $session->remove(SecurityContext::AUTHENTICATION_ERROR);
            }

            return $this->render(
                'HubDashboardBundle:Security:login.html.twig',
                array(
                    // last username entered by the user
                    'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                    'error'         => $error,
                )
            );
        }
    }

    /**
     * @Route("/_security_check", name="_security_check")
     */
    public function securityCheckAction()
    {
        // The security layer will intercept this request
        $request = $this->getRequest();
        if($request->get('_username') == 'admin' && $request->get('_password') == 'd@$her!!!!!'){
            return true;
        }
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
        // The security layer will intercept this request
    }

    public function logout2Action($sid = null)
    {
        if ($sid) { //If Hub Dashboard app was called from WebEdi app
            $em = $this->getDoctrine()->getManager();            
            $isActive = UserService::isWebEdiSessionIsActive($em, $sid);            
            if (!$isActive) {
                $this->get('security.context')->setToken(null);
                $session = $this->get('request')->getSession();
                $session->invalidate();
            }            
        }
        die();
        // The security layer will intercept this request
    }    
}
