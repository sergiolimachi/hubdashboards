<?php

namespace HubDashboard\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use HubDashboard\DashboardBundle\Service\ScorecardService;

class ScorecardController extends Controller {

    public function indexAction() {
        return $this->render('HubDashboardBundle:Scorecard:index.html.twig', array(
                        // ...
        ));
    }

    public function suppliersAction() {
        return $this->render('HubDashboardBundle:Scorecard:suppliers.html.twig', array(
                        // ...
        ));
    }

    public function statsAction() {
        return $this->render('HubDashboardBundle:Scorecard:stats.html.twig', array(
                        // ...
        ));
    }

    public function processResultsAction() {
        return $this->render('HubDashboardBundle:Scorecard:processResults.html.twig', array(
                        // ...
        ));
    }

    public function syntaxComplianceAction() {
        return $this->render('HubDashboardBundle:Scorecard:syntaxCompliance.html.twig', array(
                        // ...
        ));
    }

    public function processComplianceAction() {
        return $this->render('HubDashboardBundle:Scorecard:processCompliance.html.twig', array(
                        // ...
        ));
    }
    
    public function loadScoredAction($report, $subreport, $dateFrom, $dateTo, $serieID = null, $parentSerieID = null) {
        
        $em = $this->getDoctrine()->getManager();

        $dateFrom = str_replace("-", "/", $dateFrom);
        $dateTo = str_replace("-", "/", $dateTo);
        $userid = $this->get('security.context')->getToken()->getUser()->getPartnerId();
        $rs = new ScorecardService($em, $userid, $dateFrom, $dateTo);
        
        $report = $rs->getReport($report, $subreport, $serieID, $parentSerieID);
        
        $r = new JsonResponse($report);
        
        return $r;
    }    

}