<?php

namespace HubDashboard\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use HubDashboard\DashboardBundle\Service\DataService;

class DataController extends Controller {

    public function updateStatsAction($doctype2 = null) {
        $em = $this->getDoctrine()->getManager();
        $status = DataService::updateStats($em, $doctype2);
        $resp = 'update executed [ status: ' . $status
                . ' | for: ' . $doctype2
                . ' ]';
        return new Response($resp, $status, array('Content-Type' => 'text/text'));
    }

    public function generateStatsAction($doctype2, $startDate = '2013-01-01', $endDate = null, $tp = null, $cust = null) {
        $em = $this->getDoctrine()->getManager();
        $cust = isset($cust) ? $cust : $this->get('security.context')->getToken()->getUser()->getPartnerId();        
        $status = DataService::generateStats($em, $doctype2, $startDate, $endDate, $tp, $cust);
        $resp = 'generate executed [ status: ' . $status
                . ' | for: ' . $doctype2
                . ' | from: ' . $startDate
                . ($endDate != null ? ' | to: ' . $endDate : '')
                . ($tp != null ? ' | tp: ' . $tp : '')
                . ($cust != null ? ' | cust: ' . $cust : '')
                . ' ]';
        return new Response($resp, $status, array('Content-Type' => 'text/text'));
    }

    public function deleteStatsAction($doctype2, $tp = null, $cust = null) {
        $em = $this->getDoctrine()->getManager();
        $cust = isset($cust) ? $cust : $this->get('security.context')->getToken()->getUser()->getPartnerId();
        $status = DataService::deleteStats($em, $doctype2, $tp, $cust);
        $resp = 'delete executed [ status: ' . $status
                . ' | for: ' . $doctype2
                . ($tp != null ? ' | tp: ' . $tp : '')
                . ($cust != null ? ' | cust: ' . $cust : '')
                . ' ]';
        return new Response($resp, $status, array('Content-Type' => 'text/text'));
    }


    public function generateScoredAction($doctype2, $startDate = '2013-01-01', $endDate = null, $tp = null, $cust = null) {
        $em = $this->getDoctrine()->getManager();        
        $cust = isset($cust) ? $cust : $this->get('security.context')->getToken()->getUser()->getPartnerId();
        $status = DataService::generateScored($em, $doctype2, $startDate, $endDate, $tp, $cust);

        $resp = 'generate scored executed [ status: ' . $status
                . ' | for: ' . $doctype2
                . ' | from: ' . $startDate
                . ($endDate != null ? ' | to: ' . $endDate : '')
                . ($tp != null ? ' | tp: ' . $tp : '')
                . ($cust != null ? ' | cust: ' . $cust : '')
                . ' ]';
        return new Response($resp, $status, array('Content-Type' => 'text/text'));
    }    

}
