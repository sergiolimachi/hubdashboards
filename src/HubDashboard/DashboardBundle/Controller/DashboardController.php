<?php

namespace HubDashboard\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use HubDashboard\DashboardBundle\Service\ReportService;


class DashboardController extends Controller
{
    public function indexAction()
    {
        $request = $this->getRequest();
        
        $report = $request->get('report');
        $dateFrom = $request->get('dateFrom');
        $dateTo = $request->get('dateTo');
        if (empty($dateFrom)) {
            $dateFrom = new \DateTime();
            $dateFrom->setTime(0, 0, 0);
            $dateFrom->sub(new \DateInterval('P1M'));
            $dateFrom->sub(new \DateInterval('P1D'));
        } else {
            $dateFrom = new \DateTime(str_replace("-", "/", $dateFrom));
        }
        if (empty($dateTo)) {
            $dateTo = new \DateTime();
            $dateTo->setTime(0, 0, 0);
            $dateTo->sub(new \DateInterval('P1D'));
            
        } else {
            $dateTo = new \DateTime(str_replace("-", "/", $dateTo));
        }

        return $this->render(
            "HubDashboardBundle:Dashboard:main.html.twig",
            array(
                'report' => $report,
                'dateFrom' => $dateFrom->format('m/d/Y'),
                'dateTo' => $dateTo->format('m/d/Y'),
            )
        );
    }

    
    public function discrepanciesAction($report = '') {
        $request = $this->getRequest();
        
        $report = $request->get('report');
        $dateFrom = $request->get('dateFrom');
        $dateTo = $request->get('dateTo');
        if (empty($dateFrom)) {
            $dateFrom = new \DateTime();
            $dateFrom->setTime(0, 0, 0);
            $dateFrom->sub(new \DateInterval('P1M'));
            $dateFrom->sub(new \DateInterval('P1D'));
        } else {
            $dateFrom = new \DateTime(str_replace("-", "/", $dateFrom));
        }
        if (empty($dateTo)) {
            $dateTo = new \DateTime();
            $dateTo->setTime(0, 0, 0);
            $dateTo->sub(new \DateInterval('P1D'));
        } else {
            $dateTo = new \DateTime(str_replace("-", "/", $dateTo));
        }

        return $this->render(
            "HubDashboardBundle:Dashboard:index.html.twig",
            array(
                'report' => $report,
                'dateFrom' => $dateFrom->format('m/d/Y'),
                'dateTo' => $dateTo->format('m/d/Y'),
            )
        );
    }
    
    
    public function scorecardAction() {
        $request = $this->getRequest();
        
        $report = $request->get('report');
        $dateFrom = $request->get('dateFrom');
        $dateTo = $request->get('dateTo');
        if (empty($dateFrom)) {
            $dateFrom = new \DateTime();
            $dateFrom->setTime(0, 0, 0);
            $dateFrom->sub(new \DateInterval('P1M'));
        } else {
            $dateFrom = new \DateTime(str_replace("-", "/", $dateFrom));
        }
        if (empty($dateTo)) {
            $dateTo = new \DateTime();
            $dateTo->setTime(0, 0, 0);
        } else {
            $dateTo = new \DateTime(str_replace("-", "/", $dateTo));
        }
        return $this->render(
            "HubDashboardBundle:Dashboard:scorecard.html.twig",
            array(
                'dateFrom' => $dateFrom->format('m/d/Y'),
                'dateTo' => $dateTo->format('m/d/Y'),
            )
        );
    }
    
    
    public function suppliersAction() {
        $request = $this->getRequest();
        
        $report = $request->get('report');
        $dateFrom = $request->get('dateFrom');
        $dateTo = $request->get('dateTo');
        if (empty($dateFrom)) {
            $dateFrom = new \DateTime();
            $dateFrom->setTime(0, 0, 0);
            $dateFrom->sub(new \DateInterval('P1M'));
        } else {
            $dateFrom = new \DateTime(str_replace("-", "/", $dateFrom));
        }
        if (empty($dateTo)) {
            $dateTo = new \DateTime();
            $dateTo->setTime(0, 0, 0);
        } else {
            $dateTo = new \DateTime(str_replace("-", "/", $dateTo));
        }

        return $this->render(
            "HubDashboardBundle:Dashboard:suppliers.html.twig",
            array(
                'dateFrom' => $dateFrom->format('m/d/Y'),
                'dateTo' => $dateTo->format('m/d/Y'),
            )
        );
    }
    
    //funcion boton filtrar
    public function loadReportAction($report, $subreport, $dateFrom, $dateTo, $serieID = null, $parentSerieID = null) {
        $em = $this->getDoctrine()->getManager();

        $dateFrom = str_replace("-", "/", $dateFrom);
        $dateTo = str_replace("-", "/", $dateTo);
        
        //$rs = new ReportService($em, $this->getUser()->getPartnerId(), $dateFrom, $dateTo);
//        $userid=13;
//        if(strpos($report, 'asn') > 0){
//            $userid = 324;
//        }
        $userid = $this->get('security.context')->getToken()->getUser()->getPartnerId();        
        $rs = new ReportService($em, $userid, $dateFrom, $dateTo);
        $rs->setLogger($this->get('logger'));
        $report = $rs->getReport($report, $subreport, $serieID, $parentSerieID);
        
        return new JsonResponse($report);
    }
    
    
    public function getMessagesAction($report, $xdate, $customer, $elementIndex = -1) {
        $em = $this->getDoctrine()->getManager();
        //$rs = new ReportService($em, $this->getUser()->getPartnerId());
//        $userid=13;
//        if(strpos($report, 'asn') > 0){
//            $userid = 324;
//        }
        $userid = $this->get('security.context')->getToken()->getUser()->getPartnerId();        
        $rs = new ReportService($em, $userid);
        $rs->setLogger($this->get('logger'));
        $data = $rs->getDiscrMessages($report, $xdate, $customer, $elementIndex);
        return new JsonResponse($data);
    }
    
    public function detailDiscrAction($orderid, $docid) {
        // TODO: This screen is pretty much hardcoded for 810. 855 & 856.
        // altought I started efforts to make it more generic (originally only supported 855's) 
        // there still a lot of work to do. I would suggest refactoring this code to use our discrepancy_report table 
        // instead of trying to manually compare
        
        // Lookups 
        $status =   array(  'AA' => 'Accepted - Order Forwarded to Alternate Supplier Location',
                'AC' => 'Accepted and Shipped',
                'AR' => 'Accepted and Released for Shipment',
                'BP' => 'Accepted - Partial Shipment, Balance Backordered',
                'DR' => 'Accepted - Date Rescheduled',
                'IA' => 'Accepted',
                'IB' => 'Backordered',
                'IC' => 'Accepted - Changes Made',
                'ID' => 'Deleted',
                'IE' => 'Accepted, Price Pending',
                'IF' => 'On Hold, Incomplete Description',
                'IH' => 'On Hold',
                'IP' => 'Accepted - Price Changed',
                'IQ' => 'Accepted - Quantity Changed',
                'IR' => 'Rejected',
                'IS' => 'Accepted - Substitution Made',
                'IW' => 'On Hold - Waiver Required',
                'R1' => 'Rejected, Not a Contract Item', 
                'R2' => 'Rejected, Invalid Item Product Number',
                'R3' => 'Rejected, Invalid Unit of Issue',
                'R4' => 'Rejected, Contract Item not Available',
                'R5' => 'Rejected, Reorder Item as a Just in Time (JIT) Order',
                'R6' => 'Rejected, Reorder Item as an Extended Delivery Order (EDO)',
                'R7' => 'Rejected, Reorder Item as a Drop Shipment',
                'R8' => 'Rejected, Reorder Item as a Surge Order',
                'SP' => 'Accepted - Schedule Date Pending');
        $em = $this->getDoctrine()->getManager();
        $sql = "select * from Messages where PK_id = :msgid";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('msgid', $orderid);
        $stmt->execute();
        $msgpo = $stmt->fetch();
        $date = $msgpo['shipdate'];
        $poReference=$msgpo['reference'];
        $buyerID=$msgpo['buyername'];
        
        $stmt->bindValue('msgid', $docid);
        $stmt->execute();
        $msgdoc = $stmt->fetch();
        $docType=$msgdoc['doctype'];
        $docReference=$msgdoc['reference'];
        
        $sql = "SELECT n.PK_id, n.title, group_concat(e.`index`,':',e.data ORDER BY e.`index` ASC SEPARATOR ';') as elem 
                FROM Nodes n JOIN Elements e on e.nodepointer=n.PK_id JOIN Messages m ON m.rootnode=e.rootnode 
                WHERE m.PK_id = :msgid
                GROUP BY e.nodepointer;";
        
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('msgid', $msgpo['PK_id']);
        $stmt->execute();
        $poArr = $stmt->fetchAll();
        $msgPoDetail = array();
        $vendor = "";
        $c = 0;
        foreach ($poArr as $po) {
            if($po['title'] == 'PO1' && $po['elem'] != "") {
                $c++;
                $det = explode(";", $po['elem']);
                $detail = array();
                foreach ($det as $de) {
                    $d = explode(":", $de);
                    $detail[$d[0]] = $d[1];
                }
                //$k = ($detail[1] != "")?$detail[1]:$c;
                $k=$c;
                $msgPoDetail[$k] = $detail;
            } elseif($po['title'] == 'N1' && $po['elem'] != "") {
                $det = explode(";", $po['elem']);
                $detail = array();
                foreach ($det as $de) {
                    $d = explode(":", $de);
                    $detail[$d[0]] = $d[1];
                }
                if($detail[1] == 'VN') {
                    $vendor = $detail[2];
                }
            }
        }
        
        
        $sql = "SELECT n.PK_id, n.title, group_concat(e.`index`,':',e.data ORDER BY e.`index` ASC SEPARATOR ';') as elem 
                FROM Nodes n join Elements e on e.nodepointer=n.PK_id JOIN Messages m ON m.rootnode=e.rootnode 
                WHERE m.PK_id = :msgid
                GROUP BY e.nodepointer;";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('msgid', $docid);
        $stmt->execute();
        $docArr = $stmt->fetchAll();
        $msgDocDetail = array();
        $docLocation = "";        
        $c = 0;
        for ($i=0; $i<count($docArr);$i++) {
            $ack = $docArr[$i];
            $newDate = "";
            $newDateQual = "";
            if(($ack['title'] == 'PO1' || $ack['title'] == 'IT1'|| $ack['title'] == 'LIN') && $ack['elem'] != "") {
                $c++;
                $det = explode(";", $ack['elem']);
                $detail = array();
                foreach ($det as $de) {
                    $d = explode(":", $de);
                    $detail[$d[0]] = $d[1];
                }
                //$k = ($detail[1] != "")?$detail[1]:$c;
                $k=$c;
                $msgDocDetail[$k]['PO1'] = $detail;
                $pid = array();
                $ack = array();
                for($j=1;$j<20;$j++) {
                    if(!isset($docArr[$i+$j])) break;
                    if($docArr[$i+$j]['title'] == 'PO1' || $docArr[$i+$j]['title'] == 'IT1'|| $docArr[$i+$j]['LIN'] == 'LIN') {
                        break;
                    }elseif($docArr[$i+$j]['title'] == 'PID') {
                        $det = explode(";", $docArr[$i+$j]['elem']);
                        $detail = array();
                        foreach ($det as $de) {
                            $d = explode(":", $de);
                            if(isset($d[0]) && isset($d[1])) {
                                $detail[$d[0]] = $d[1];
                            }
                        }
                        $pid = $detail;
                    }elseif($docArr[$i+$j]['title'] == 'ACK') {
                        $det = explode(";", $docArr[$i+$j]['elem']);
                        $detail = array();
                        foreach ($det as $de) {
                            $d = explode(":", $de);
                            $detail[$d[0]] = $d[1];
                        }
                        $ack = $detail;
                        if(isset($detail[4]) && isset($detail[5])) {
                            $newDateQual = $detail[4];
                            $newDate = $detail[5];
                        }
                    }elseif ($docArr[$i+$j]['title'] == 'SN1') {
                        $det = explode(";", $docArr[$i+$j]['elem']);
                        $detail = array();
                        foreach ($det as $de) {
                            $d = explode(":", $de);
                            $detail[$d[0]] = $d[1];
                        }
                        
                        $msgDocDetail[$k]['PO1']['2']=$detail['2'];
                        $msgDocDetail[$k]['PO1']['3']=$detail['3'];
                        $msgDocDetail[$k]['PO1']['4']=$msgPoDetail[$k]['4'];
                        $msgDocDetail[$k]['PO1']['5']=$msgPoDetail[$k]['5'];
                    }
                }
                $newDateAck= "";
                $newDateAckQual= "";
                if(strlen($newDate) == 8 && !empty($newDateQual)) {
                    $newDateAck = substr($newDate, 4, 2) . "-".substr($newDate, 6, 2)."-".substr($newDate, 0, 4);
                    $stmt = $em->getConnection()->prepare("SELECT * FROM Codes where elementcode = '374' and code = :code");
                    $stmt->bindValue('code', $newDateQual);
                    $stmt->execute();
                    $dtm04 = $stmt->fetchAll();
                    $newDateAckQual = $dtm04[0]['description'];
                }
                $msgDocDetail[$k]['PID'] = $pid;
                $msgDocDetail[$k]['ACK'] = $ack;
                $msgDocDetail[$k]['ACK']['ACK05'] = $newDateAck;
                $msgDocDetail[$k]['ACK']['ACK04'] = $newDateAckQual;
            } elseif($ack['title'] == 'N1' && $ack['elem'] != "") {
                $det = explode(";", $ack['elem']);
                $detail = array();
                foreach ($det as $de) {
                    $d = explode(":", $de);
                    $detail[$d[0]] = $d[1];
                }
                if($detail[1] == "ST") {
                    $docLocation = $detail[2];
                }
            }
        }
        $result = array();
        foreach($msgPoDetail as $line => $po1) {
            $result[$line]['PO1'] = $po1;
            if(isset($msgDocDetail[$line]['PO1'])) {
                if($msgDocDetail[$line]['PO1'][2] != $po1[2] 
                        || $msgDocDetail[$line]['PO1'][3] != $po1[3] 
                        || $msgDocDetail[$line]['PO1'][4] != $po1[4]) {
                    
                    $result[$line]['PO1ACK'] = $msgDocDetail[$line]['PO1'];
                    if($docType == 855) {
                        $result[$line]['ACK'] = $msgDocDetail[$line]['ACK'];
                        $result[$line]['ACK']['status'] = $status[$msgDocDetail[$line]['ACK'][1]];
                    }
                    $result[$line]['PID'] = $msgDocDetail[$line]['PID'];
                }
            }else{
                $result[$line]['PO1ACK']=array('MISSING','MISSING','MISSING','MISSING','MISSING','MISSING','MISSING','MISSING','MISSING','MISSING');
                $result[$line]['PID'] = 'MISSING LINE';
            }
        }
        
        return $this->render(
            "HubDashboardBundle:Dashboard:discrepancies.html.twig",
            array(
                "poid" => $orderid,
                "docid" => $docid,
                "docType"=> $docType,
                "datemsg" => $date,
                "buyerID" => $buyerID,
                "vendor" => $vendor,
                "poReference" => $poReference,
                "docReference" => $docReference,
                "docLocation" => $docLocation,
                "msgPO" => $result
            )
        );
    }
    
    public function buyersReportAction() {
        $em = $this->getDoctrine()->getManager();
        $userid = $this->get('security.context')->getToken()->getUser()->getPartnerId();        
        $rs = new ReportService($em, $userid);
        $data = $rs->getBuyersReport();
        //dd($data);
        return new JsonResponse($data);
    }
    
    public function suppliersReportAction() {
        $em = $this->getDoctrine()->getManager();
        $userid = $this->get('security.context')->getToken()->getUser()->getPartnerId();        
        $rs = new ReportService($em, $userid);
        $data = $rs->getSuppliersReport();
        return new JsonResponse($data);
    }    
    
    public function exceptionReportAction($report_type,$date, $filter, $buyer, $supplier) {
        switch ($report_type) {
            case 'po-ack-discr':
                $origDoctype='850';
                $changeDoctype='855';
                break;
            case 'po-asn-discr':
                $origDoctype='850';
                $changeDoctype='856';
                break;
            case 'po-inv-discr':
                $origDoctype='850';
                $changeDoctype='810';
                break;
        }
        $em = $this->getDoctrine()->getManager();
        $userid = $this->get('security.context')->getToken()->getUser()->getPartnerId();        
        $rs = new ReportService($em, $userid);
        $data = $rs->getExceptionReport($date, $filter, $buyer, $supplier,$origDoctype,$changeDoctype);

        $today = date('mdY');
        $filename = "ExceptionReport_".$today.".xls";
        
        $response = $this->render("HubDashboardBundle:Dashboard:exceptionReport.xls.twig", 
                    array(
                        'data' => $data,
                        'orig_doctype'=>$origDoctype,
                        'change_doctype'=>$changeDoctype,
                        'report_type'=>$report_type
                        )
                    );
        
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment; filename="'.$filename.'"');

        return $response;        
    }
}
